const gulp = require('gulp'),
	exec = require('child_process').exec,
	concat = require('gulp-concat'),
	order = require("gulp-order"),
	mainBowerFiles = require('main-bower-files'),
	babel = require('gulp-babel'),
	sourcemaps = require('gulp-sourcemaps'),
	jshint = require('gulp-jshint'),
	uglify = require('gulp-uglify'),
	gutil = require('gulp-util'),
	jsdoc = require('gulp-jsdoc3');


const paths = {
	jsLocalSrc : 'mfisheries/static/js/**/*.js',
	cssLocalSrc : 'mfisheries/static/css/**/*.css',
	jsDeployPath : 'mfisheries/static/dist/js/',
	bowerPath: 'mfisheries/static/bower_components',
	vendorPath : 'mfisheries/static/dist/vendor'
};
const babel_ignore =[
	'mfisheries/static/js/caputils/config.js',
	'mfisheries/static/js/caputils/caplib.js',
	'mfisheries/static/js/caputils/OpenLayers.js'
];

/**
 * Development Tasks
 */

gulp.task('lint', function () {
	return gulp.src(paths.jsLocalSrc)
		.pipe(jshint(".jshintrc"))
		.pipe(jshint.reporter('jshint-stylish'));
});

gulp.task('pack-js', function () {
	return gulp.src(paths.jsLocalSrc)
		.pipe(order([
			'mfisheries/static/js/caputils/config.js',
			'mfisheries/static/js/caputils/caplib.js',
			'mfisheries/static/js/caputils/OpenLayers.js',
			'mfisheries/static/js/ga/firebase_config.js',
			'mfisheries/static/js/ga/google_analytics.js',
			'mfisheries/static/js/ga/push_notify.js',
			'mfisheries/static/js/main.js',
			'mfisheries/static/js/controllers/core/libs.js',
			paths.jsLocalSrc
		], {base: './'}))
		.pipe(babel({
			presets: ['es2015'],
			ignore: babel_ignore
		}))
		.pipe(concat('app.js'))
		.pipe(gulp.dest(paths.jsDeployPath));
});

gulp.task('watch-js', function () {
	gulp.watch(paths.jsLocalSrc, ['lint', 'pack-js']);
});

gulp.task('pack-js-deploy', function(){
	return gulp.src(paths.jsLocalSrc)
		.pipe(sourcemaps.init())
		.pipe(babel({ presets: ['es2015'] }))
		.pipe(uglify({mangle: false})) // TODO: Determine why setting Mangle to true crashes the app
		.pipe(concat('app.js'))
		.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest(paths.jsDeployPath));
});

/**
 * Library Packaging Tasks
 */

gulp.task('pack-libs', function () {
	return gulp.src(mainBowerFiles({
		paths: {
			bowerDirectory: paths.bowerPath,
			bowerJson: 'mfisheries/static/bower.json',
			debugging:true
		}
	}))
	.pipe(concat('bundle.js'))
	.pipe(gulp.dest(paths.vendorPath));
});

// gulp.task('pack-libs', function () {
// 	return gulp.src([
// 		'assets/js/vendor/*.js'
// 	])
// 	.pipe(order([
// 		'assets/js/vendor/jquery-2.2.4.min.js',
// 		'assets/js/vendor/*.js'
// 	], {base: './'}))
// 	.pipe(concat('bundle.js'))
// 	.pipe(gulp.dest('public/build/js'));
// });


gulp.task('pack-css', function () {
	return gulp.src(['assets/css/vendor/*.css', 'assets/css/main.css'])
		.pipe(concat('stylesheet.css'))
		.pipe(gulp.dest('public/build/css'));
});

gulp.task('watch-css', function () {
	gulp.watch(paths.cssLocalSrc, ['pack-css']);
});

gulp.task('serve', function (cb) {
	exec('pserve development.ini --reload', function (err, stdout, stderr) {
		console.log(stdout);
		console.log(stderr);
		cb(err);
	});
});

gulp.task('deploy', ['pack-js-deploy']);

gulp.task('watch', ['watch-js', 'watch-css']);

//https://github.com/mlucool/gulp-jsdoc3
gulp.task('docs', function (cb) {
	gulp.src([paths.jsLocalSrc], {read: false})
		.pipe(jsdoc(cb));
});
// create a default task and just log a message
// gulp.task('default', ['pack-js', 'pack-libs', 'pack-css']);
gulp.task('default', ['pack-js', 'serve', 'watch', 'docs']);