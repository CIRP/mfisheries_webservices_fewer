// Build the configuration needed specifically for Push.js
mFish.push_notify_conf = {
	"FCM": mFish.mf_fb_config
};

Push.config(mFish.push_notify_conf);
Push.FCM(); // Initialize FCM within the Push.js service
// Push.create('Hello World!');