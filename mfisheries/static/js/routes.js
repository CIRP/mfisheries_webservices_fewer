// Routes.js
angular.module('mfisheries')
		.config(function ($routeProvider, $locationProvider) {

			$locationProvider.html5Mode({enabled: false, requireBase: true}).hashPrefix('');

			$routeProvider
					.when('/login',
							{
								controller: 'LoginCtrl',
								templateUrl: 'static/partials/login.pt'
							})
					.when('/user/info',
							{
								controller: 'UserInfoCtrl',
								templateUrl: 'static/partials/userinfo.pt'
							})
					.when('/user/info/:modules',
							{
								controller: 'UserInfoCtrl',
								templateUrl: 'static/partials/userinfo.pt'
							})
					.when('/all/tracks',
							{
								controller: 'CoastguardCtrl',
								templateUrl: 'static/partials/coastguard.pt'
							})
					.when('/user/tracks',
							{
								controller: 'CoastguardCtrl',
								templateUrl: 'static/partials/coastguard.pt'
							})
					.when('/image/diary',
							{
								controller: 'imageDiaryCtrl',
								templateUrl: 'static/partials/imagediary.pt'
							})
					.when('/image/gallery',
							{
								controller: 'galleryCtrl',
								templateUrl: 'static/partials/gallery.pt'
							})
					.when('/trip/history',
							{
								controller: 'CoastguardCtrl',
								templateUrl: 'static/partials/coastguard.pt'
							})
					.when('/lek',
							{
								controller: 'LekCtrl',
								controllerAs: 'lekctrl',
								templateUrl: 'static/js/controllers/lek/lek.pt'
							})
					.when('/user/profile',
							{
								controller: 'userProfileCtrl',
								templateUrl: 'static/partials/userprofile.pt'
							})
					.when('/admin/users',
							{
								controller: 'adminUserCtrl',
								templateUrl: 'static/js/controllers/core/user/adminusers.pt'
							})
					.when('/admin/country',
							{
								controller: 'adminCountryCtrl',
								templateUrl: 'static/partials/admincountry.pt'
							})
					.when('/admin/weather',
							{
								controller: 'adminWeatherCtrl',
								templateUrl: 'static/js/controllers/weather/adminweather.pt'
							})
					.when('/admin/email',
							{
								controller: 'adminWeatherEmailCtrl',
								templateUrl: 'static/js/controllers/weather/adminemail.pt'
							})
					.when('/weather/report',
							{
								controller: 'adminReportCtrl',
								templateUrl: 'static/js/controllers/weather/report/adminreport.pt'
							})
					.when('/admin/modules',
							{
								controller: 'adminCountryCtrl',
								templateUrl: 'static/partials/admincountry.pt'
							})
					.when('/admin/occupations',
							{
								controller: 'adminOccupationCtrl',
								templateUrl: 'static/js/controllers/core/occupation/adminoccupation.pt'
							})
					.when('/admin/villages',
							{
								controller: 'adminVillageCtrl',
								templateUrl: 'static/js/controllers/core/village/adminvillage.pt'
							})
					.when('/admin/config',
							{
								controller: 'configCtrl',
								templateUrl: 'static/js/controllers/core/config/config.pt'
							})
					.when('/admin/lek',
							{
								controller: 'adminLEKCtrl',
								templateUrl: 'static/js/controllers/lek/adminlek.pt'
							})
					.when('/admin/miscellaneous',
							{
								controller: 'MiscellCtrl',
								templateUrl: 'static/partials/miscellaneous.pt'
							})
					.when('/alert/groups',
							{
								controller: 'alertGroupCtrl',
								templateUrl: 'static/js/controllers/alerts/alertgroups.pt'
							})
					.when('/alert/groups/:groupid',
							{
								controller: 'alertGroupMembersCtrl',
								templateUrl: 'static/js/controllers/alerts/alertgroupmembers.pt'
							})
					// FEWER Related Routes
					.when('/admin/fewer/emergencycontacts',
							{
								controller: 'adminEmergencyContactCtrl',
								templateUrl: 'static/js/controllers/fewer/emergency/emergencyContact.pt'
							})
					.when('/admin/fewer',
							{
								controller: 'FEWERCtrl',
								controllerAs: 'FEWERCtrl',
								templateUrl: 'static/js/controllers/fewer/adminFewer.pt'
							})
					.when('/admin/fewer/alerts',
							{
								controller: 'adminCAPCtrl',
								templateUrl: 'static/js/controllers/fewer/alerts/admincap.pt'
							})
					.when('/fewer',
							{
								controller: 'FEWERCtrl',
								controllerAs: 'FEWERCtrl',
								templateUrl: 'static/js/controllers/fewer/fewer.pt'
							})
					.when('/admin/fewer/alerts/add',
							{
								controller: 'adminCAPCtrl',
								templateUrl: 'static/js/controllers/fewer/alerts/addcap.pt'
							})
					.when('/admin/fewer/alerts/:operation/:alertid',
							{
								controller: 'adminCAPCtrl',
								templateUrl: 'static/js/controllers/fewer/alerts/addcap.pt'
							})
					.when('/admin/fewer/sources',
							{
								controller: 'adminFEWERSrcCtrl',
								templateUrl: 'static/js/controllers/fewer/sources/sources.pt'
							})
					.when('/admin/fewer/sources/:operation/:src_id',
							{
								controller: 'FEWERSrcMgmtCtrl',
								templateUrl: 'static/js/controllers/fewer/sources/manage.pt'
							})
					.when("/admin/fewer/dmreports",
							{
								controller: 'DMReportCtrl',
								controllerAs: 'dmreports',
								templateUrl: 'static/js/controllers/fewer/damagereport/adminDmReport.pt'
							})

					.when("/admin/fewer/missingpersons",
							{
								controller: 'MSReportCtrl',
								controllerAs: 'missingReports',
								templateUrl: 'static/js/controllers/fewer/missing/adminMissingReport.pt'
							})
					.when('/admin/fewer/notify/settings',
							{
								controller: 'adminFEWERNotifyCtrl',
								templateUrl: 'static/partials/fewer/notify.pt'
							})
					.when('/admin/fewer/delivery',
							{
								controller: 'adminFEWERDeliveryCtrl',
								templateUrl: 'static/js/controllers/fewer/delivery/delivery.pt'
							})
					.when('/admin/fewer/procedures',
						{
							controller: 'ProcedureCtrl',
							controllerAs: 'procedures',
							templateUrl: 'static/js/controllers/fewer/procedures/adminProcedures.pt'
						})
					// Public FEWER URLs
					.when('/fewer/emergencycontacts',
							{
								controller: 'EmergencyContactCtrl',
								controllerAs: 'emgcontacts',
								templateUrl: 'static/js/controllers/fewer/emergency/listEmergencyContact.pt'
							})
					.when("/fewer/procedures",
							{
								controller: 'ProcedureCtrl',
								controllerAs: 'procedures',
								templateUrl: 'static/js/controllers/fewer/procedures/procedures.pt'
							})
					.when("/fewer/dmreports",
							{
								controller: 'DMReportCtrl',
								controllerAs: 'dmreports',
								templateUrl: 'static/js/controllers/fewer/damagereport/dmreport.pt'
							})
					.when("/fewer/missingpersons",
							{
								controller: 'MSReportCtrl',
								controllerAs: 'missingReports',
								templateUrl: 'static/js/controllers/fewer/missing/missingReport.pt'
							})
					.when("/fewer/alerts",
							{
								controller: 'AlertListCtrl',
								controllerAs: 'alertlist',
								templateUrl: 'static/js/controllers/fewer/alerts/listcap.pt'
							})
				
				.when("/dashboard",
					{
						controller: 'DashboardCtrl',
						controllerAs: 'dashctrl',
						templateUrl: 'static/js/controllers/core/dashboard/countryAdminDashboard.pt'
					})
					.otherwise({redirectTo: '/'});
		});
