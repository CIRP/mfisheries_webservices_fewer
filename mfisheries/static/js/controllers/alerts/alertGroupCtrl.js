"use strict";

angular.module('mfisheries.AlertGroupController', [])
	
	.controller('alertGroupCtrl', [
		'$scope', 'LocalStorage', 'Country', 'Group', 'AuthService', 'Alerts',
		function ($scope, LocalStorage, Country, Group, AuthService, Alerts) {
			$(".mFish_menu").removeClass("active");
			$("#menu-group").addClass("active");
			// $("#menu-meta").addClass("active");
			$("#menu-meta").addClass("active");
			

			AuthService.attachCurrUser($scope);
			console.log($scope.role.scope);
			
			$scope.countries = [];
			$scope.groups = [];
			$scope.alerts = [];
			$scope.alert = {};
			$scope.readOnly = false;
			
			function getCountries() {
				Country.get($scope.userCountry).then(function (res) {
					$scope.countries = res.data;
				});
			}
			
			function getGroups() {
				Group.get($scope.userCountry).then(function (res) {
					$scope.groups = res.data;
					console.log("Groups");
					console.log($scope.groups);
				});
			}
			
			function reset() {
				$scope.currGroup = {
					'countryid': $scope.currentUser.countryid,
					'oneWay': false
				};
			}
			
			function resetAlert(){
				$scope.alert = {
					'countryid': $scope.currentUser.countryid,
					'userid': $scope.currentUser.userid,
					'isPublic': 0
				};
				console.log($scope.alert);
			}
			
			getCountries();
			getGroups();
			reset();
			
			$scope.deleteUserGroup = function (id) {
				swal({
					title: "Delete Confirmation",
					text: "Are you sure you want to delete this record. You will not be able to undo this operation",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Delete",
					closeOnConfirm: false
				}).then(res => {
					if (res.value) {
						Group.delete(id).then(function (res) {
								console.log(res);
								swal("Deleted", "Record was successfully deleted", "success");
								// Remove group from array, when operation is successful
								$scope.groups.splice(
									_.indexOf($scope.groups, _.find($scope.groups, function (group) {
										return group.id === id;
									})), 1);
							},
							function (res) {
								swal("Failed", "Unable to delete record", "error");
								console.log(res);
							});
					}
				});
				
				
				
			};
			
			$scope.editGroup = function (group) {
				$scope.currGroup = group;
				$("#groupModal").modal('show');
			};
			
			$scope.updateGroup = function (group) {
				swal({
					title: "Update Confirmation",
					text: "Are you sure you want to update this record. You will not be able to undo this operation",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#3e8f3e",
					confirmButtonText: "Update",
					closeOnConfirm: false
				}).then(res => {
					if (res.value) {
						Group.update(group).then(function (res) {
							console.log(res);
							$("#groupModal").modal('hide');
							swal("Updated", "Record was successfully updated", "success");
						}, function (err) {
							console.log(err);
							swal("Update Confirmation", "Unable to update record. If error persists notify administrator", "error");
						});
						// Reset the model container for next operation
						reset();
					}
				});
			};
			
			$scope.addNewGroup = function () {
				reset();
				$("#groupModal").modal('show');
			};
			
			$scope.submit = function () {
				console.log("Submitting Group:");
				console.log($scope.currGroup);
				
				if ($scope.currGroup.id && $scope.currGroup.id > 0) { // If we have an id then record can be updated
					$scope.updateGroup($scope.currGroup);
				} else { // Record is new and can be saved
					// TODO Ensure that User cannot enter the same group name for a country
					Group.add($scope.currGroup)
						.then(function (res) {
								$("#groupModal").modal('hide');
								console.log(res);
								swal("Create Group", "Group was successfully created", "success");
								getGroups();
							},
							function (res) {
								console.log(res);
								swal("Create Group", "Unable to create group. If problem persists contact administrator.", "error");
							});
				}
			};

			$scope.viewGroupMembers = function(group){
				console.log("Attempting to view members for group: " + group.id);

			};

			$("#groupModal").on('hidden.bs.modal', reset);
			
			// Community alerts based Operations
			
			$scope.addCommAlert = function(){
				console.log("Select add community alert");
				resetAlert();
				$("#msgModal").modal('show');
			};
			
			$scope.loadCommAlertListing = function(){
				console.log("Request for retrieveing community alerts executed");
				$scope.selectedGroup = {};
				$scope.alerts = [];
				$scope.groups.forEach(group => {
					console.log("Requesting information from group: " + group.groupname );
					Alerts.getByGroup(group.id).then(alerts => {
						console.log("Retrieved %s alerts from group %s", alerts.length, group.groupname);
						if (alerts.data.length > 0)
							$scope.alerts = $scope.alerts.concat(alerts.data);
						console.log($scope.alerts);
					});
				});
			};
			
			$scope.refresh = function(){
				console.log("Refresh for Community Alerts requested");
			};
			
			$scope.handleGroupChange = function(group){
				console.log("Attempting to run handle change groups");
				Alerts.getByGroup(group).then(alerts => {
					console.log(alerts.data);
					$scope.alerts = alerts.data;
				}, err => console.error(err));
			};
			
			$scope.saveAlert = function(alert){
				console.log(alert);
				Alerts.add(alert).then(res => {
					if (res.status === 200){
						console.log(res.data);
					}
				});
			};
			
		}]);
