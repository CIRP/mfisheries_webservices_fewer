"use strict";

angular.module('mfisheries.GalleryController',[])

.controller('galleryCtrl', function($scope, $q, $rootScope, getUserImages, Lightbox,
                                    LocalStorage, AUTH_EVENTS, AuthService){

	$(".mFish_menu").removeClass("active");
	$("#menu-gallery").addClass("active");
	
	AuthService.attachCurrUser($scope);

	getUserImages.get($scope.currentUser.userid).then(function(res){
		$scope.images = [];
		$scope.hasgalleryImages = ((res.data.data.length > 0));
		if($scope.hasgalleryImages){			
			const dates = Object.keys(_.indexBy(res.data.data, 'date')),
				imagesByDate = {};

			$.each(dates, function(index, data){				
				imagesByDate[data] = _.filter(res.data.data, function(img){						
					return img.date === data;
				});
				
			});					
			$scope.images = imagesByDate;

			$scope.openLightboxModal = function (date, url) {
				const index = _.findIndex($scope.images[date], function (image) {
					return image.url === url;
				});
				Lightbox.openModal($scope.images[date], index);
			};
		}
	});

});