"use strict";

angular.module('mfisheries.ImageDiaryController',[])

.controller('imageDiaryCtrl', function($scope, $q, $map, $window, $rootScope, LocalStorage,
                                       getImageDates, getLastImage, AUTH_EVENTS, AuthService){

	$(".mFish_menu").removeClass("active");
	$("#menu-diary").addClass("active");
	var map = $("#map"),
	calendar = $('#calendar'),
	dates = [];
	$map.initMap(map);
	$map.setType("image");
	
	AuthService.attachCurrUser($scope);

	getImageDates.get($scope.currentUser.userid).then(function(res){
		calendar.fullCalendar({
			header: {
		        left: 'prev,next, ',
		        center: 'title',
		        right: 'today'
		    },
	        selectable		: false,
	        selectHelper	: false,
	        editable 		: false,
	        events 			: res.data.data,
	        eventColor		: '#378006',
	        eventClick		: eventClick,
	        dayClick		: dayClick
		});
		/**
			The code below fixed a bug, when the calendar renders, the buttons
			display is sent to none, not sure if it is an issue with angular and
			fullcalendar...
		**/
		var btn = $('.fc-toolbar button.fc-button'),
		d = new Date();
		$.each(btn, function(i,d){
			$(d).css('display','block');
		});
	});

	$scope.hideshow = function(){
		console.log("Clicked");

		var sidebar = $('#sidebar'),
			main = $('#main'),
			button = $('#sidebar_button').find('span'),
			map = $("#map"),
			main_class_lg = 'col-md-12 col-lg-12',

			main_class_sm = 'col-md-8 col-lg-8',
			sidebar_class = 'col-md-4 col-lg-4';

		if($(main).hasClass(main_class_lg)){

			$(main).removeClass(main_class_lg);
			$(main).addClass(main_class_sm);

			$(button).removeClass('glyphicon glyphicon-chevron-right');
			$(button).addClass('glyphicon glyphicon-chevron-left');

			$(sidebar).addClass(sidebar_class);
			$(sidebar).show();

		}else{
			$(sidebar).hide();
			$(sidebar).removeClass(sidebar_class);

			$(button).removeClass('glyphicon glyphicon-chevron-left');
			$(button).addClass('glyphicon glyphicon-chevron-right');

			$(main).removeClass(main_class_sm);
			$(main).addClass(main_class_lg);
		}
		resizeMap();
	};


	function resizeMap(){
		var center = $map.map.getCenter();
	 	google.maps.event.trigger($map.map, "resize");
	 	$map.map.setCenter(center);
	 	console.log('Map successfully resized');
	}

	function dayClick(date, jsEvent, view){

	}

	function eventClick(calEvent, jsEvent, view){
		var s= calEvent.start._i.split('-'),
		d = new Date(parseInt(s[0]),parseInt(s[1])-1,parseInt(s[2]));

		getLastImage.get($scope.currentUser.userid,d.getDate(), d.getMonth()+1, d.getFullYear()).then(function(res){
			console.log(res.data.data);
			$map.addImageMarker(res.data.data);
		});

		$map.zoomOut();
	}

});
