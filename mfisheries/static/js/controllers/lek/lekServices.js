class LEKCategory extends BaseAPIService{
	constructor($http){
		super($http, "api/lek/categories");
	}
}
LEKCategory.$inject = [
	"$http"
];

class LEKService extends BaseAPIService{
	constructor($http){
		super($http, "api/lek");
	}
}
LEKService.$inject = [
	"$http"
];

angular.module('mfisheries.LekServices', [])
	.service("LEKCategory", LEKCategory)
	.service("LEKService",LEKService)
	.service("getLek", function ($http) {
		
		var deffered = {};
		
		deffered.get = function (userid, type, offset) {
			console.log('/api/get/lek/' + userid + '/' + type + '/' + offset);
			return $http.get('/api/get/lek/' + userid + '/' + type + '/' + offset);
		};
		
		return deffered;
	})
	
	.service("getLekByCategory", function ($http) {
		
		var deffered = {};
		
		deffered.get = function (userid, type, category) {
			console.log('/api/lek/' + userid + '/' + type + '/' + category);
			return $http.get('/api/lek/' + userid + '/' + type + '/' + category);
		};
		
		return deffered;
	})
	
	.service("getLekByRange", function ($http) {
		
		var deffered = {};
		
		deffered.get = function (userid, type, category, countryid, day1, month1, year1, day2, month2, year2) {
			console.log('/api/lek/' + userid + '/' + type + '/' + category + '/' + countryid + "/" + day1 + '/' + month1 + '/' + year1 + '/' + day2 + '/' + month2 + '/' + year2);
			return $http.get('/api/lek/' + userid + '/' + type + '/' + category + '/' + countryid + '/' + day1 + '/' + month1 + '/' + year1 + '/' + day2 + '/' + month2 + '/' + year2);
		};
		
		return deffered;
	})
	
	.service("getLekByDate", function ($http) {
		
		var deffered = {};
		
		deffered.get = function (userid, type, category, day, month, year) {
			console.log('/api/lek/' + userid + '/' + type + '/' + category + '/' + day + '/' + month + '/' + year);
			return $http.get('/api/lek/' + userid + '/' + type + '/' + category + '/' + day + '/' + month + '/' + year);
		};
		
		return deffered;
	});