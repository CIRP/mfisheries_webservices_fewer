"use strict";

angular.module('mfisheries.AdminLEKController', [])
	.controller('adminLEKCtrl', [
		'$scope', 'LocalStorage', 'Country', 'LEKCategory', 'Module', 'AuthService',
		function ($scope, LocalStorage, Country, LEKCategory, Module, AuthService) {
			
			$(".mFish_menu").removeClass("active");
			$("#menu-lek").addClass("active");
			
			$scope.countries = [];
			$scope.categories = [];
			$scope.country = {};
			$scope.readOnly = false;
			$scope.lek_loading = true;
			
			// Configure operations for ACL
			AuthService.attachCurrUser($scope);
			const currUser = $scope.currentUser;
			
			
			// Implementation copied from admin country controller
			// TODO revisit to have a single implementation
			function loadCountries() {
				console.log("loading countries");
				$scope.country_loading = true;
				Country.get($scope.userCountry).then(function (res) {
					$scope.countries = res.data;
					$scope.country_loading = false;
				});
			}
			
			function loadCategories() {
				console.log("Loading categories");
				$scope.lek_loading = true;
				LEKCategory.get($scope.userCountry).then(function (res) {
					console.log("responded");
					$scope.categories = res.data;
					console.log(res.data);
					$scope.lek_loading = false;
				}, () => console.log("Unable to load LEK"));
			}
			
			function reset() {
				$scope.category = {
					'countryid': currUser.countryid,
					'createdby': currUser.userid
				};
			}
			
			loadCountries();
			loadCategories();
			reset();
			
			// TODO Evaluate if this is needed
			// gets the template to ng-include for a table row / item
			$scope.getTemplate = function (country) {
				if (country.id === $scope.country.id) return 'edit';
				else return 'display';
			};
			
			
			$scope.deleteCategory = function (id) {
				swal({
					title: "Delete Confirmation",
					text: "Are you sure you want to delete this record. You will not be able to undo this operation",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Delete",
					closeOnConfirm: false
				}).then( res => {
					if (res.value) {
						LEKCategory.delete(id).then(function (res) {
							if (res && res.data.status === 200) {
								swal("Deleted", "Record was successfully deleted", "success");
								$scope.categories.splice(
									_.indexOf($scope.categories, _.find($scope.categories, function (country) {
										return country.id === id;
									})), 1);
							} else
								swal("Failed", "Unable to delete record", "error");
						});
					}
				});
			};
			
			$scope.addCategory = function (country) {
				$("#categoryModal").modal('show');
				reset();
			};
			
			$scope.submit = function () {
				$scope.country.path = "";
				console.log($scope.category);
				
				let sim = $scope.categories.filter((el) => {
					return el.countryid === $scope.category.countryid &&
						el.name.toLowerCase() === $scope.category.name.toLowerCase();
				});
				
				if (sim.length < 1) {
					LEKCategory.add($scope.category)
						.then(function (res) {
								swal("Creating Category", "Category Successfully Created", "success");
								loadCategories();
								reset();
							},
							function (res) {
								swal("Creating Category", "Error creating Country!", "error");
							});
				} else {
					swal("Creating Category", "Unable to create category. Category exists", "error");
				}
				
				
				// $("#categoryModal").modal('hide');
			};
			
			$("#categoryModal").on('hidden.bs.modal', $scope.reset);
			
		}]);
