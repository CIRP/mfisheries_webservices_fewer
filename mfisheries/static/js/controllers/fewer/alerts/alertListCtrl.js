/**
 * Class for CAP listing controller.
 * The same class is used for both viewing and administrative functionality
 * Step 1 develop Retrieval for country dropdown list
 * Step 2 develop Retrieval for public group for country
 */
class AlertListCtrl extends BaseController {
	constructor(Country,Locator, AuthService) {
		super(Country, Locator, AuthService);
		this.init();
	}

	init() {
		super.init();
		this.reports = [];

		this.userCountry = 0;
	}

	startView() {
		super.startView().then(res =>{
			console.log(res);
		}, err => console.log(err));
	}

	handleCountryChange(countryid){
		console.log("Country was changed to:" + countryid);
		if (!isNaN(countryid))countryid = parseInt(countryid);
		this.userCountry = countryid;

		this.locator.saveLastLocationID(countryid).then(res => console.log(res));
		// Attempting to add country name to be displayed based on data
		let country = this.countries.filter(country => country.id === countryid)[0];
		if (country)this.countryname = country.name;
		// retrieve posts
	}


}

AlertListCtrl.$inject = [
	"Country",
	"Locator",
	"AuthService"
];

angular.module('mfisheries.Alerts', []).controller("AlertListCtrl", AlertListCtrl);