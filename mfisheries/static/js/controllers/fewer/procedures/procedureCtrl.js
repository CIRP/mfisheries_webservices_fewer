"use strict";

class ProcedureCtrl{
	
	constructor(AuthService, Country){
		this.Country = Country;
		AuthService.attachCurrUser(this);
		this.init();
	}
	
	init(){
		$(".mFish_menu").removeClass("active");
		$("#menu-fewer").addClass("active");
		this.retrieveCountries();
	}
	
	retrieveCountries(){
		const self = this;
		this.Country.get(this.userCountry).then(function (res) {
			self.countries = res.data;
		});
	}
	
	view(){
		console.log("Selected the option for viewing procedures");
	}
}

ProcedureCtrl.$inject = [
	"AuthService",
	"Country"
];

angular.module('mfisheries.Procedures',[])
	.controller("ProcedureCtrl",ProcedureCtrl);