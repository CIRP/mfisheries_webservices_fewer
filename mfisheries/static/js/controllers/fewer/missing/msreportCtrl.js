/**
 * Missing Person Report Controller
 *
 */
class MSReportCtrl extends BaseController{
	
	constructor(MSReportService, MSReportFieldService, AuthService, Country, Locator){
		super(Country, Locator, AuthService);
		console.log("Missing Report Controller");
		// Attach the needed services as attributes of the class
		this.MSReportService = MSReportService;
		this.MSReportFieldService = MSReportFieldService;
		this.init();
	}
	
	/**
	 * Provides operations needed for running initialization operations
	 */
	init(){
		super.init();
		console.log('Running the initialization of Missing Report Controller');
		
		$(".mFish_menu").removeClass("active");
		$("#menu-fewer").addClass("active");
		$("#menu-msgnperson").addClass("active");
		
		
		this.currLocation = ["", ""];
		this.tableId = "msgPersonTbl";
		this.records = [];
		this.is_loading = true;
		this.countryname = "";
	}
	
	/**
	 *
	 */
	startAdmin(){
		console.log("Launching Administrative interface");
		this.retrieveCountries(true).then(res => console.log(res), err=> console.error(err));
		this.retrieveRecords();
		this.reset();
	}
	
	/**
	 *
	 */
	startView(){
		console.log("Launching View/Public interface");
		super.startView().then(res => console.log(res), err=> console.error(err));
	}
	
	retrieveRecords(countryid){
		let country  = (countryid) ? countryid : this.userCountry;
		this.is_loading = true;
		console.log("Attempting to retrieve missing person records for %s" , country);
		this.MSReportService.get(country).then(res => {
			this.is_loading = false;
			this.records = res.data.map(el =>{
				el.timecreated = moment(el.timecreated);
				el.timeupdated = moment(el.timeupdated);
				return el;
			});
			console.log("Received %s records from %s", this.records.length, country);
		});
	}
	
	/**
	 *
	 * @param countryid
	 */
	handleCountryChange(countryid){
		console.log("Country was changed to:" + countryid);
		this.userCountry = countryid;
		this.locator.saveLastLocationID(countryid).then(res => console.log(res));
		// Attempting to add country name to be displayed based on data
		let country = this.countries.filter(country => country.id === countryid)[0];
		if (country)this.countryname = country.name;
		// retrieve posts
		this.retrieveRecords(countryid);
	}
	
	reset(){
		// this.report = {
		// 	title: "John Doe",
		// 	description: "6ft African male, the dreadlocks",
		// 	contact: "4574143",
		// 	latitude: "13.1371855",
		// 	longitude: "-61.1959889",
		// 	additional: "Last seen in Calliaqua, heading to the Grenadines",
		// 	countryid: this.userCountry,
		// 	userid: this.currentUser.userid,
		// 	isFound: 0
		// };
		this.report = {
			title: "",
			description: "",
			contact: "",
			latitude: "",
			longitude: "",
			additional: "",
			countryid: this.userCountry,
			userid: this.currentUser.userid,
			isFound: 0
		};
	}
	
	retrieveLocation(){
	
	}
	
	save(report){
		// if update
		if (report.id && report.id > 0){
			this.MSReportService.update(report).then(res =>{
				swal("Update Report", "Report was successfully Updated", "success");
				$(this.primaryModal).modal('hide');
				this.retrieveRecords();
			}, err => {
				console.log(err);
				swal("Update Report", "Unable to update report. If error persist contact administrator for support.", "error");
			});
		}else{ // we have a new record
			// TODO Implementation to check if record exists requires that it should be loaded. This request should be sent to the server
			let sim = this.records.filter(el => el.title === report.title && el.countryid && report.countryid);
			if (sim.length < 1){
				this.MSReportService.add(report).then(res => {
					if (res.status === 201) {
						swal("Add Report", "Report was successfully Saved", "success");
						$(this.primaryModal).modal('hide');
						this.retrieveRecords();
					}else{
						swal("Add Report", "Unable to save report. If error persist contact administrator for support.", "error");
						console.err(res.status);
					}
				}, (err) => {
					swal("Add Report", "Unable to save report.  If error persist contact administrator for support.", "error");
					console.log(err);
				});
			}else{
				swal("Adding New Report", "Unable to add new Report. Report of a similar name exists.", "error");
			}
		}
	}
	
	deleteRec(report) {
		swal({
			title: "Delete Confirmation",
			text: "Are you sure you want to delete this record. You will not be able to undo this operation",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Delete",
			closeOnConfirm: false
		}).then(res => {
			console.log(res);
			if (res.value) {
				this.MSReportService.delete(report).then(res => {
					if (res && res.status === 200) {
						swal("Deleted", "Record was successfully deleted", "success");
						this.retrieveRecords();
					} else swal("Failed", "Unable to delete record", "error");
				}, err =>{
					swal("Failed", "Unable to delete record", "error");
					console.error(err);
				});
			}
		});
	}
	
	markAsFound(report){
		swal({
			title: "Update Confirmation",
			text: "Confirm that you the person has been found?",
			type: "warning",
			showCancelButton: true,
			confirmButtonText: "Yes",
			closeOnConfirm: false
		}).then(res => {
			if (res.value){
				console.log("Mark person as found");
				console.dir(report);
				swal("Update Report", "Report was successfully Updated", "success");
			}
		});
	}
}

MSReportCtrl.$inject = [
	"MSReportService",
	"MSReportFieldService",
	"AuthService",
	"Country",
	"Locator"
];

angular.module('mfisheries.MissingPersons', [])
		.controller("MSReportCtrl", MSReportCtrl);