"use strict";
// miscellCtrl.js
angular.module('mfisheries.controllers')
.controller('MiscellCtrl', [ '$scope', function($scope){

	$scope.hulls = [];
	$scope.hull = {};

	$(".mFish_menu").removeClass("active");
	$("#menu-miscellaneous").addClass("active");

	$scope.addHull = function(country){
    $("#countryModal").modal('show');
  };

	// gets the template to ng-include for a table row / item
  $scope.getTemplate = function (hull) {
	  if (hull.id === $scope.hull.id) return 'edit';
	  else return 'display';
  };

}]);