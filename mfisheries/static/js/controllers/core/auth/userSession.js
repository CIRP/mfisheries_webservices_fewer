angular.module('mfisheries.services')
	.service("Session", function(LocalStorage){
		// Create will add a new User to the system. Information retrieved from server when logged in
		this.create = function(userid, fname, lname, role, country, countryid){
			this.user = {
				"userid" : userid,
				"fname" : fname,
				"lname" : lname,
				"userRole": role,
				"country": country,
				'countryid': countryid
			};
			LocalStorage.setObject('user',this.user);
			return this.user;
		};
		
		this.destroy = function(){
			LocalStorage.delete('user');
		};
		
		this.getSession = function(){
			return !_.isNull(LocalStorage.getObject('user'));
		};
		
	});