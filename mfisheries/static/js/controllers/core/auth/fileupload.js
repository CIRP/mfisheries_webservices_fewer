angular.module('mfisheries.services',[])
	
	.service("fileupload", function($http){
		const deffered = {};
		
		deffered.uploadFileToUrl  = function(file, data,  uploadUrl, name_file){
			if (!name_file)name_file = "upload";
			const fd = new FormData();
			fd.append(name_file, file);
			for (let d in data){
				if (data.hasOwnProperty(d))
					fd.append(d, data[d]);
			}
			return $http.post(uploadUrl, fd, {
				transformRequest: angular.identity,
				headers: {'Content-Type': undefined }
			});
		};
		
		deffered.post = function(data){
			
			const fd = new FormData();
			for(let key in data){
				if (data.hasOwnProperty(key))
					fd.append(key, data[key]);
			}
			console.log(data);
			return $http.post('api/add/lek', fd,
				{
					transformRequest: angular.indentity,
					headers: {'Content-Type' : undefined}
				});
		};
		return deffered;
	});