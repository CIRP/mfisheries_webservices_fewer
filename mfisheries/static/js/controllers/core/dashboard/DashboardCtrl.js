
class DashboardCtrl{
	constructor(){
		console.log("Dashboard constructor was initialized");
	}
}

DashboardCtrl.$inject = [];

angular.module('mfisheries.controllers')
	.controller("DashboardCtrl", DashboardCtrl);