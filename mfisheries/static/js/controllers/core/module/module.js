class Module extends BaseAPIService{
	constructor($http){
		super($http, "/api/modules");
	}
}

Module.$inject = [
	"$http"
];
angular
	.module('mfisheries.AdminServices')
	.service("Module", Module);