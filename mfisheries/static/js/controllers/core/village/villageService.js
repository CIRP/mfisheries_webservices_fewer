class Villages{
	constructor($http){
		this.$http = $http;
		this.base_url = "/api/villages";
	}
	get(countryid){
		const options = {};
		if (countryid && countryid !== 0)options.countryid = countryid;
		return this.$http.get(this.base_url, { params: options });
	}
	add(data){
		return this.$http.post(this.base_url, data);
	}
	update(source) {
		return this.$http.put(this.base_url + "/" + source.id, source);
	}
	delete(source){
		return this.$http.delete(this.base_url+ "/" + source.id);
	}
}

Villages.$inject = [
	"$http"
];
angular.module('mfisheries.Village').service("Villages", Villages);