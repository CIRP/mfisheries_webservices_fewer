"use strict";

angular.module('mfisheries.Weather')
	.controller('adminReportCtrl', [
		'$scope', 'LocalStorage', 'WeatherSource', 'Entries', 'Country', 'Alerts', 'CAPSource', 'CapService', 'AuthService', 'CountryLoc','Locator',
		function ($scope, LocalStorage, WeatherSource, Entries, Country, Alerts, CAPSource, CapService, AuthService, CountryLoc, Locator) {
			console.log("Report Controller");
			
			$(".mFish_menu").removeClass("active");
			$("#menu-weather").addClass("active");

			// Initialize attributes needed for functionality
			$scope.weatherSources = {};
			$scope.weather = {};
			$scope.alerts = {};
			$scope.countries = [];
			$scope.countryid = 0;
			$scope.countryname = "";
			$scope.image = "sunny";
			$scope.imageforecast = "sunny";
			$scope.temperature = "";
			$scope.openWeather = {
				"sys": {},
				"main": {},
				"weather": {}
			};
			$scope.forecast = [];
			$scope.tides = [];

			// Run the initialization process for the reports
			init();

			/**
			 *
			 * @param lat
			 * @param long
			 */
			function loadCountryDetails(lat, long) {
				const tide_api_key = "1139b6d8-e234-42d0-b93b-a61b647357fc";
				const base_tide = "https://www.worldtides.info/api?key=" + tide_api_key;
				let tide_url = base_tide + "&extremes&lat=" + lat + "&lon=" + long;
				
				const weather_api_key = "3f3b5f31bc28cbcd8c7090057cfdb89f";
				const base_weather = "https://api.openweathermap.org/data/2.5/weather?APPID=" + weather_api_key;
				let open_weather_url = base_weather + "&units=metric&lat=" + lat + "&lon=" + long;
				
				const forecast_api_key = "ee27a1efb7ed4a886da6fd977dc282b2";
				const base_forecast = "https://api.openweathermap.org/data/2.5/forecast/daily?APPID=" + forecast_api_key;
				let open_weather_forecast = base_forecast + "&units=metric&lat=" + lat + "&lon=" + long + "&cnt=5" ;

				// console statement used to debug api calls
				// console.log("Tide: %s , Weather: %s , Forecast: %s ", tide_url, open_weather_url, open_weather_forecast);
				// retrieve current weather
				apiCalls(open_weather_url, "current");
				// retrieve forecast weather
				apiCalls(open_weather_forecast, "forecast");
				// retrieve tide data
				// apiCalls(tide_url, "tides");
			}

			/**
			 * Retrieves latitude and longitude from modules.
			 * Coordinates are passed to request clients via a promise
			 * @param countryid
			 * @returns {Promise}
			 */
			function loadCountryLocation(countryid) {
				if (!isNaN(countryid))countryid = parseInt(countryid);
				return new Promise((resolve, reject) => {
					console.log("Loading Country Location for: " + countryid);
					CountryLoc.get(countryid).then(res => {
						let rec = res.data.filter(el => el.countryid === countryid)[0];
						console.log(rec);
						if (rec)resolve(rec);
						else reject("No record for country " + countryid + " found");
					}, reject);
				});
			}
			
			/**
			 * Loads data from weather extractors
			 * @param countryid
			 */
			function loadWeatherSources(countryid) {
				$scope.loading = true;
				return WeatherSource.get(countryid).then(function (res) {
					if (res.status === 200) {
						$scope.weatherSources = res.data;
						console.log("Retrieved " + $scope.weatherSources.length + " source records");
						$scope.loading = false;
						// var id = getSourceID();
						// getWeather(id);
					}
				});
			}
			
			function getSourceID() {
				for (let i = 0; i < ($scope.weatherSources.length); i++) {
					console.log(i);
					if ($scope.weatherSources[i].infotype === "Weather") {
						return $scope.weatherSources[i].id;
					}
				}
			}
			
			function getWeather(sourceid) {
				Entries.get(sourceid).then(function (res) {
					$scope.readings = res.data.readings;
					delete $scope.readings.sourceid;
					console.log($scope.readings);
				});
			}
			
			function loadAlerts(countryid) {
				$scope.cap_loading = true;
				$scope.alerts = [];
				$scope.communityAlerts = [];
				// Request each country (if countryid not specified, we will load all countries)
				Country.get(countryid).then(res => {
					// Ensure we retrieve country values
					// For each country, we receive the CAP source associated with that country
					res.data.forEach(country => {
						// We retrieve the compatible CAP Source //TODO Implement logic that can accomodate all sources
						CAPSource.get(country.id, true).then(srcRes => {
							// For each source, we extract its URL, Pass it to the CAP service
							srcRes.data.forEach(srcEl => {
								CapService.setBaseURL(srcEl.url);
								// CAP service receives the alert based on information passed
								CapService.getAlerts().then(function (servRes) {
									$scope.alerts = $scope.alerts.concat(servRes.data.map(el => {
										// add the country
										el.country = country.name;
										return el;
									}));

									console.log($scope.alerts);
									$scope.cap_loading = false; // Stop loading as soon as first is retrieved and displayed
								});
							});
						});
					});
				});
				// TODO Restructuring of alerts required
				Alerts.get().then(function (res) {
					// console.log(res);
					$scope.communityAlerts = $scope.communityAlerts.concat(res.data);
				});
			}

			/**
			 * Retrieves data from Country Service.
			 * Will also assign the countries retrieved to the
			 * countries attribute of the controller/scope
			 * @param display
			 * @returns {Promise}
			 */
			function loadCountries(display) {
				return new Promise((resolve, reject) => {
					$scope.country_loading = true;
					Country.get().then(function (res) { // get all countries
						$scope.countries = res.data;
						// Attempting to Add a new Record at the beginning to give useful info for user
						if (display) $scope.countries.unshift({id: 0, name: "Select Country"	});
						$scope.country_loading = false;
						resolve(res.data);
					}, reject);
				});
			}
			
			function getCountry(countryid) {
				Country.get(countryid).then(function (res) {
						let k = res.data;
						$scope.countryname = k[0].name;
						console.log($scope.countryname.name);
						console.log("HELLO");
				});
				
			}

      // API calls for the external sources of information (Openweather and Worldtides)
			function apiCalls(url, state) {
				// console.log("Loading data for %s from %s", state, url);
				fetch(url)
					.then(resp => resp.json())
					.then(data=> {
						// If current data is requested
						if (state === "current") {
							console.log("Current Data Requested.");

							$scope.openWeather.main = data.main;
							$scope.openWeather.sys = data.sys;
							$scope.openWeather.sys.sunrise = timeConverter(data.sys.sunrise, "current");
							$scope.openWeather.sys.sunset = timeConverter(data.sys.sunset, "current");
							$scope.temperature = Math.round(data.main.temp);
							$scope.openWeather.weather = data.weather[0];

							if (data.weather[0].main === "Rain")
								$scope.image = "rainy";
							else if (data.weather[0].main === "Clouds")
								$scope.image = "cloudy";
							else if (data.weather[0].main === "Sunny" || data.weather[0].main === "Clear")
								$scope.image = "sunny";

						}
						else if (state === "forecast") {
							$scope.forecast = data.list;
							console.log(data.list);
							$scope.forecast.pop();
							console.log("start");
							for (let i = 0; i < ($scope.forecast.length); i++) {
								$scope.forecast[i].dt = timeConverter($scope.forecast[i].dt, "forecast");
								$scope.forecast[i].temp.max = Math.round(parseFloat($scope.forecast[i].temp.max));
								$scope.forecast[i].temp.min = Math.round(parseFloat($scope.forecast[i].temp.min));
								$scope.forecast[i].weather = $scope.forecast[i].weather[0];
								
								if ($scope.forecast[i].weather.main === "Rain")
									$scope.forecast[i].image = "rainy";
								else if ($scope.forecast[i].weather.main === "Clouds")
									$scope.forecast[i].image = "cloudy";
								else if ($scope.forecast[i].weather.main === "Sunny" || $scope.forecast[i].weather.main === "Clear")
									$scope.forecast[i].image = "sunny";
							}

						}
						else if (state === "tides") {
							$scope.tides = data.extremes;
							if ($scope.tides) {
								for (let i = 0; i < ($scope.tides.length); i++) {
									$scope.tides[i].dt = timeConverter($scope.tides[i].dt, "tides");
								}
								console.log("Tides");
								console.log($scope.tides);
							}
						}

						$scope.$applyAsync();
					})
					.catch(function (error) {
						console.log(error);
					});
			}

      // Changes UNIX time stamp to readable time
			function timeConverter(UNIX_timestamp, state) {
				const a = new Date(UNIX_timestamp * 1000);
				const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
				const days = ['Sun', 'Mon', 'Tues', 'Wed', 'Thurs', 'Fri', 'Sat'];
				const daysfull = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
				let con = "a.m";
				let day = days[a.getDay()];
				const year = a.getFullYear();
				const month = months[a.getMonth()];
				const date = a.getDate();
				let time;
				let hour = a.getHours();
				const min = a.getMinutes();
				const sec = a.getSeconds();


				if (state === "tides") {
					if (hour > 12) {
						hour = hour - 12;
						con = "p.m";
					}
					time = day + ' ' + date + ' ' + hour + ':' + min + ' ' + con;
					
					return time;
					
				}
				// It uses GMT time zone, restructured to local time
				if (state === "current") {
					hour = hour + 4;
					if (hour > 12) {
						hour = hour - 12;
						con = "p.m";
					}
					time = day + ' ' + date + ' ' + month + ' ' + hour + ':' + min + ' ' + con;
					console.log(UNIX_timestamp);
					return hour + ':' + min + ' ' + con;
					
				}
				day = daysfull[a.getDay()];
				return day + " " + date;
			}
			
			$scope.handleCountryChange = function (countryid, action) {
				if (!isNaN(countryid))countryid = parseInt(countryid);
				// persist country for later use
				Locator.saveLastLocationID(countryid).then(res => console.log(res));
				// Attempting to add country name to be displayed based on data
				let country = $scope.countries.filter(country => country.id === countryid)[0];
				if (country) $scope.countryname = country.name;
				$scope.contryid = countryid;
				loadCountryLocation(countryid).then(countryLoc =>{
					loadCountryDetails(countryLoc.latitude, countryLoc.longitude);
				}, err =>{
					swal("Weather", "Unable to retrieve Country information", "error");
					console.error(err);
				});
				loadWeatherSources(countryid);
				// loadAlerts(countryid);
				// getCountry(countryid);
			};

			/**
			 * Displays the a control-less loading message dialog.
			 * Clients that use the function should use the extent and call close
			 * method on the response to dismiss message
			 * @param message
			 * @returns {Promise}
			 */
			function displayLoading(message){
				swal({
					type: "info",
					title: "Weather",
					text: message,
					allowOutsideClick: false,
					allowEscapeKey: false,
					showConfirmButton: false,
					onOpen: function () {
						swal.showLoading();
					}
				});

				return Promise.resolve(swal);
			}

			/**
			 * Initialize the Report Controller
 			 */
			function init(){
				// display a loading message while we request country data
				displayLoading("Loading Countries").then(loading =>{
					// After Displaying notification
					loadCountries(true).then(countries =>{
						loading.close();
						Locator.getLastLocationID().then(countryid => {
							console.log("Location found from cache as: " + countryid);
							if (countryid && countryid !== "undefined")$scope.handleCountryChange(countryid);
						});
					}, err => loading.close());
				});

				// Check if user logged in:
				// if user logged in, then use the id associated with user

				// Check if Country was previously selected

				// testing loading country details
				// dominica
				// const loc = [15.414999, -61.370976];
				// grenada
				// const loc = [12.262776, -61.604171];
				// skn
				// const loc = [17.357822, -62.782998];
				// slu
				// const loc = [13.909444, -60.978893];
				// svg
				// const loc = [12.984305, -61.287228];
				// tobago
				// const loc = [11.2849228, -60.8219893];
				// trinidad
				// const loc = [10.691803, -61.222503];
				// console.log("Attempting to retrieve details for country:" + loc);
				// loadCountryDetails(loc[0], loc[1]);
			}
		}]);