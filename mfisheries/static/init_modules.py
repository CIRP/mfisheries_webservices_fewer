# Import the os module, for the os.walk function
import ConfigParser
import os

from sqlalchemy import text, func, create_engine
from sqlalchemy.orm import sessionmaker

"""
	The code below allows the importing of the Country and Module classes from
	the models.py files. This is because models.py is apart of the pyramid package and
	init_modules.py is not. 
"""

# TODO Need to Adjust this based on the redesign of the models
from mfisheries.core.country import Country, CountryModule
from mfisheries.core import Module

# from mfisheries.models import DBSession

country_mapping = {
	"bz": "Belize",
	"atb": "Antigua and Barbuda",
	"dom": "Dominica",
	"gre": "Grenada",
	"skn": "St Kitts and Nevis",
	"slu": "St Lucia",
	"svg": "St Vincent and the Grenadines",
	"trinidad": "Trinidad",
	"tobago": "Tobago"
}

if __name__ == '__main__':
	if __package__ is None:
		import sys
		from os import path
		
		sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))

# Set the directory you want to start from
# TODO Determine if system running in production or development mode

config = ConfigParser.ConfigParser()

ini_file_name = "development.ini"
for i in range(0, 3):
	if os.path.isfile(ini_file_name):
		break
	else:
		ini_file_name = "../{0}".format(ini_file_name)

config.read(ini_file_name)
engine_str = config.get('app:main', "sqlalchemy.url")

rootDir = 'country_modules'
engine = create_engine(engine_str, echo=False)
Session = sessionmaker(bind=engine)
DBSession = Session()


def processFileName(filename):
	filename = filename.split(".")[0]
	if filename.lower() != "sos":  # Added Exception for SOS
		caps = [
			'A', 'B', 'C', 'D', 'E', 'F',
			'G', 'H', 'I', 'J', 'K', 'L',
			'M', 'N', 'O', 'P', 'Q', 'R',
			'S', 'T', 'U', 'V', 'W', 'X',
			'Y', 'Z'
		]
		i = 1
		while i < len(filename):
			if filename[i] in caps:
				filename = filename[:i] + " " + filename[i:]
				i += 1
			i += 1
	return filename


def getDirectoriesList(path_loc):
	folders = {}
	modules = []
	countries = []
	data = {}
	# Check each File Within the directory
	for dirName, subdirList, fileList in os.walk(path_loc):
		if dirName != path_loc:
			files = []
			folder_attributes = {}
			# for each file in the list
			for file_name in fileList:
				if file_name != "index.html":
					# ProcessFileName generates the Module Name, based on the naming convention
					file_attributes = {
						'name': processFileName(file_name),
						'hasDownload': "yes"
					}
					# Used to generate the Module name
					modules.append({
						"name": file_attributes['name'],
						"hasDownload": "yes"
					})
					# Add the path where the file is found for the country module
					file_attributes['path'] = 'static/' + os.path.join(dirName, file_name)
					print("found file as path: " + file_attributes['path'])
					files.append(file_attributes)
			
			# After Processing Each File
			folder_attributes['files'] = files
			folder_attributes['path'] = 'static/' + dirName
			folder_attributes['name'] = dirName.split('/')[1]
			folders[dirName.split('/')[1]] = folder_attributes
			
			countries.append({
				"name": dirName.split('/')[1],
				"path": 'static/' + dirName
			})
	data['modules'] = dict((v['name'], v) for v in modules).values()
	data['countries'] = countries
	data['folders'] = folders
	return data


def insertModules(modules):
	_modules = {}
	for module in modules:
		try:
			if 'name' in module and module['name'] != "index":
				modRec = DBSession.query(Module)\
					.filter(func.lower(Module.name) == module['name'])\
					.first()
				
				if modRec is None:  # If we did not find a record, so insert a new module
					modRec = Module("", name=module['name'])
					print("We did not find {0}. Inserting".format(modRec.name))
				modRec.hasDownload = "yes"
				DBSession.add(modRec)
				DBSession.flush()
				_modules[modRec.name] = modRec.id
		except Exception as e:
			print("Failed to retrieve or insert module " + str(e))
		DBSession.commit()
	return _modules


def insertCountries(countries):
	try:
		countries_db = DBSession.query(Country).all()
		_countries = {}
		for country in countries_db:
			_countries[country.name.lower()] = country.id

		# For each country found in the folder
		for country in countries:
			country_str = country['name'].lower()
			# if the country not found by name use mapping
			if country_str not in _countries:
				if country_str in country_mapping:
					mapped = country_mapping[country_str].lower()
					_countries[country_str] = _countries[mapped]
				else:
					print("Unable to find mapping for " + str(country_str))
		return _countries
	except Exception as e:
		print("Unable to get countries {0}".format(e.message))
		return None
	

def doesCountryModuleExist(countryid, moduleid):
	return DBSession.query(CountryModule) \
		.filter(func.lower(CountryModule.moduleid) == moduleid) \
		.filter(func.lower(CountryModule.countryid) == countryid) \
		.first()


def initializeCountryModules(data):
	modules = insertModules(data['modules'])
	countries = insertCountries(data['countries'])
	if modules is not None and countries is not None:
		for country in data['folders']:
			country_id = countries[data['folders'][country]['name']]
			for files in data['folders'][data['folders'][country]['name']]['files']:
				try:
					country_module = CountryModule(country_id, files['path'], modules[files['name']])
					if doesCountryModuleExist(country_id, modules[files['name']]):
						print "Country: ", country_id, " Module: ", modules[files['name']], " already exists"
					else:
						DBSession.add(country_module)
						DBSession.commit()
						print "Country: ", country_id, " Module: ", modules[files['name']], " record created"
				except Exception, e:
					if "Duplicate entry" in e.message:
						print("%s already in table." % e)
						DBSession.rollback()
					else:
						print e.message

	else:
		print "failed to insert country and modules {0}, {1}".format(countries, modules)


# Will run the init_module.py file is called
if __name__ == "__main__":
	files = getDirectoriesList(rootDir)
	initializeCountryModules(files)
	# insertModules([
	# 	{ "name": "Podcast"},
	# 	{"name": "First Aid"},
	# 	{"name": "Test"}
	# ])
	