from sqlalchemy import (
    Column,
    Integer,
    String,
    TIMESTAMP,
    ForeignKey
)

from mfisheries.models import Base
import datetime

class TripHeaders(Base):
    """The SQLAlchemy model class for a TripHeaders object"""
    __tablename__ = 'tripheaders'
    id = Column(Integer, primary_key=True)
    userid = Column(Integer, ForeignKey('user.id'), nullable=False)
    latitude = Column(String(10))
    longitude = Column(String(10))
    name = Column(String(100))
    timestamp = Column(TIMESTAMP,default=datetime.datetime.utcnow)
    deviceTimestamp = Column(TIMESTAMP)
    starttrackid = Column(Integer, ForeignKey('trackpoints.id'), nullable=True)

    def __init__(self, userid, latitude, longitude, name, timestamp, deviceTimestamp, starttrackid):
        self.userid = userid
        self.latitude = latitude
        self.longitude = longitude
        self.name = name
        self.timestamp = timestamp
        self.deviceTimestamp = deviceTimestamp
        self.starttrackid = starttrackid

    def toJSON(self):
        json = {}
        json['id'] = self.id
        json['userid'] = self.userid
        json['latitude'] = self.latitude
        json['longitude'] = self.longitude
        json['name'] = self.name
        json['timestamp'] = str(self.timestamp)
        json['deviceTimestamp'] = str(self.deviceTimestamp)
        json['starttrackid'] = self.starttrackid

        return json
