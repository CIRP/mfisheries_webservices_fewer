from sqlalchemy import (
    Column,
    Integer,
    String,
    Float,
    ForeignKey,
    TIMESTAMP
)
import datetime
from mfisheries.models import Base


class Sale(Base):
    """The SQLAlchemy model class for a Sale object"""
    __tablename__ = 'sale'
    id = Column(Integer, primary_key=True)
    catchid = Column(Integer, ForeignKey('catchdetails.id'), nullable=False)
    weight = Column(Float)
    unit = Column(String(100))
    price = Column(Float)
    timestamp = Column(TIMESTAMP,default=datetime.datetime.utcnow)

    def __init__(self, catchid, weight, unit, price, timestamp):
        self.catchid = catchid
        self.weight = weight
        self.unit = unit
        self.price = price
        self.timestamp = timestamp

    def toJSON(self):
        return {
            'id': self.id,
            'catchid': self.catchid,
            'weight': self.weight,
            'unit': self.unit,
            'price': self.price,
            'timestamp': str(self.timestamp)
        }
