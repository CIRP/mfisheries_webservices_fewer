from sqlalchemy import engine_from_config
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import scoped_session, sessionmaker
from zope.sqlalchemy import ZopeTransactionExtension
from zope.sqlalchemy import register

DBSession = scoped_session(sessionmaker(extension=ZopeTransactionExtension(), expire_on_commit=False))
Base = declarative_base()


def load_local_modules(config):
    # Contains the code to initialize the models and database configuration
    # Points to the util folder (however as of 02/17 does nothing)
    config.scan('mfisheries.util')
    config.scan('mfisheries.events')
    # Core Modules
    config.scan('mfisheries.core.config')
    config.scan('mfisheries.core.country')
    config.scan('mfisheries.core.user')
    config.scan('mfisheries.core')
    # Catch details, trips and hull information
    config.scan('mfisheries.models')
    # Additional Modules
    config.scan('mfisheries.modules.alerts')
    config.scan('mfisheries.modules.photos')
    config.scan('mfisheries.modules.tracks')
    config.scan('mfisheries.modules.lek')
    config.scan('mfisheries.modules.sos')
    config.scan('mfisheries.modules.fewer')
    config.scan('mfisheries.modules.weather')
    config.scan('mfisheries.modules.emergencyContacts')
    config.scan('mfisheries.modules.messaging')
    # UI Specification
    config.scan('mfisheries.views')
    return config


def initialize_sql(engine):
    print("Running the initialize_sql of the models package")
    DBSession.configure(bind=engine)
    Base.metadata.bind = engine
    Base.metadata.create_all(engine)
    return engine


def get_tm_session(session_factory, transaction_manager):
    dbsession = session_factory()
    register(dbsession, transaction_manager=transaction_manager)
    return dbsession


def includeme(config):
    settings = config.get_settings()

    # use pyramid_tm to hook the transaction lifecycle to the request
    config.include('pyramid_tm')

    session_factory = get_session_factory(get_engine(settings))
    config.registry['dbsession_factory'] = session_factory

    # make request.dbsession available for use in Pyramid
    config.add_request_method(
        # r.tm is the transaction manager used by pyramid_tm
        lambda r: get_tm_session(session_factory, r.tm),
        'dbsession',
        reify=True
    )


def get_engine(settings, prefix='sqlalchemy.'):
    return engine_from_config(settings, prefix)


def get_session_factory(engine):
    factory = sessionmaker()
    factory.configure(bind=engine)
    return factory
