from sqlalchemy import (
    Column,
    Integer,
    Float,
    TIMESTAMP,
    String,
    ForeignKey
)
import datetime
from mfisheries.models import Base


class CatchDetails(Base):
    """The SQLAlchemy model class for a CatchDetails object"""
    __tablename__ = 'catchdetails'
    id = Column(Integer, primary_key=True)
    userid = Column(Integer, ForeignKey('user.id'), nullable=False)
    fish = Column(String(100))
    weight = Column(Float)
    unit = Column(String(100))
    timestamp = Column(TIMESTAMP,default=datetime.datetime.utcnow)

    def __init__(self, userid, fish, weight, unit, timestamp):
        self.userid = userid
        self.fish = fish
        self.weight = weight
        self.unit = unit
        self.timestamp = timestamp

    def toJSON(self):
        return {
            'id': self.id,
            'userid': self.userid,
            'fish': self.fish,
            'weight': self.weight,
            'unit': self.unit,
            'timestamp': str(self.timestamp)
        }
