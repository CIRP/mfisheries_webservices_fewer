from sqlalchemy import (
    Column,
    Integer,
    String
)

from mfisheries.models import Base


class HullType(Base):
    """The SQLAlchemy declarative model class for a HullType object"""

    __tablename__ = 'hulltype'
    id = Column(Integer, primary_key=True)
    name = Column(String(100))
    details = Column(String(100))

    def __init__(self, name, details):
        self.name = name
        self.details = details

    def toJSON(self):
        return {'id': self.id, 'name': self.name, 'details': self.details}
