#!/bin/bash

source ../../venv/bin/activate

read -p "Install Bower.json for packages and libraries for front-end development? (y/N)" choice
case "$choice" in 
  y|Y ) bower install ./mfisheries/static/bower.json;;
  n|N ) echo "Skipping Bower installation";;
  * ) echo "Skipping Bower installation";;
esac


read -p "Run setup.py for configuring installation? (y/N)" choice
case "$choice" in 
  y|Y ) python setup.py develop;;
  n|N ) echo "Skipping Setup";;
  * ) echo "Skipping Setup";;
esac


read -p "Initialize Database? (y/N)" choice
case "$choice" in 
  y|Y ) initialize_mfisheries_db development.ini;;
  n|N ) echo "Skipping Database Initialization";;
  * ) echo "Skipping Database Initialization";;
esac

read -p "Initialize Modules? (y/N)" choice
case "$choice" in 
  y|Y ) cd ./mfisheries/static/ && python init_modules.py && cd ../../;;
  n|N ) echo "Skipping Module Initialization";;
  * ) echo "Skipping Module Initialization";;
esac

read -p "Run in development mode? (y/N)" choice
case "$choice" in
  y|Y ) pserve --reload development.ini;;
  n|N ) echo "Skipping Running";;
  * ) echo "Skipping Running";;
esac