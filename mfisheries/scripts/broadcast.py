from mfisheries.modules.fewer.broadcastmanager import run_bcast_mgmtr_default
from mfisheries.util import display_debug, get_ini_path
from mfisheries.events import NotifyError


def broadcast():
	display_debug("Launching Broadcaster", "info", module_name=__name__)
	run_bcast_mgmtr_default(use_bootstrap=True)


def trigger_notification(use_command_line=True):
	display_debug("Displaying no", "info", module_name=__name__)
	from pyramid.config import Configurator
	from pyramid.paster import bootstrap
	config = None
	if use_command_line:
		ini_path = get_ini_path()
		if ini_path:
			with bootstrap(ini_path) as env:
				settings = env['registry'].settings
				config = Configurator(settings=settings)
	else:
		config = Configurator()

	config.registry.notify(NotifyError("Test", "This is a test for notification"))

