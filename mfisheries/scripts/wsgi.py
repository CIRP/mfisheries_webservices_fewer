# Or from an .ini file:
from pyramid.paster import get_app, setup_logging

# ini_path = '/home/mfisheries/public_html/mfisheries/development.ini'
ini_path = './development.ini'

setup_logging(ini_path)

application = get_app(ini_path, 'main')
