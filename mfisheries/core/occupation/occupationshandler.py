from cornice.resource import resource

from mfisheries.core import Occupation, BaseHandler
from mfisheries.models import DBSession


# http://cornice.readthedocs.io/en/latest/resources.html
@resource(collection_path='/api/occupations', path='/api/occupations/{id}', description='Occupations')
class OccupationsHandler(BaseHandler):
    def _get_order_field(self):
        return Occupation.type

    def __init__(self, request, context=None):
        BaseHandler.__init__(self, request, context)

    def _create_empty_model(self):
        return Occupation("","",0)

    def _get_target_class(self):
        return Occupation

    def _get_fields(self):
        return self._create_empty_model().getRequiredFields()

    def _dict_to_model(self, data, rec_id=0):
        # If id is 0, we want to create a new entry
        if rec_id == 0:
            src = self._create_empty_model()
        else:
            src = DBSession.query(self._target_class).get(rec_id)
        # Assign value to the fields of the model
        src.type = data['type']
        src.description = data['description']
        src.countryid = data['countryid']
        if "createdby" in data:
            src.createdby = data['createdby']
        return src