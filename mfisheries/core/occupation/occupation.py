import transaction
import datetime
from sqlalchemy import (
    Column,
    Integer,
    String,
    ForeignKey,
    TIMESTAMP,
    DateTime, func,
    UniqueConstraint
)
from sqlalchemy import event
from sqlalchemy.orm import relationship

from mfisheries.models import Base, DBSession
from mfisheries.core import Country


class Occupation(Base):
    """The SQLAlchemy declarative model class for a Occupation object"""
    __tablename__ = 'occupation'
    id = Column(Integer, primary_key=True)
    type = Column(String(100), nullable=False)
    description = Column(String(100), nullable=False)
    countryid = Column(Integer, ForeignKey('country.id'))
    timecreated = Column(TIMESTAMP, default=datetime.datetime.now)
    timemodified = Column(DateTime, onupdate=func.now())
    createdby = Column(Integer)
    
    country = relationship("Country", foreign_keys=[countryid])
    
    __table_args__ = (UniqueConstraint('type', 'countryid', name='_unique_country_occupation' ),)

    def __init__(self, type, description, countryid=1):
        self.type = type
        self.description = description
        self.countryid = countryid

    def getRequiredFields(self):
        return ['countryid', 'type', 'description']

    def toJSON(self):
        return {
            'id': self.id,
            'type': self.type,
            'description': self.description,
            "countryid": self.countryid,
            "country": self.country.name,
	        "timecreated": str(self.timecreated),
            "timemodified": str(self.timemodified),
            "createdby": str(self.createdby)
        }


def create_default_occupation(target, connection_rec, **kw):
    
    print("Creating Default Occupations for Each Existing Country")
    occupation_list = [
        {"name": "Boat Owner", "description": "Own a boat for fishing or renting to other fishers"},
        {"name": "Fisherman", "description": "Catches Fish"},
        {"name": "Wholesaler", "description": "Collects and sells fishes in bulk"},
        {"name": "Processor", "description": "Cleans and Processes fish for reselling etc."},
        {"name": "Retailer", "description": "Sells fish for in established businesses"},
        {"name": "Vendor", "description": "Sells fish to individual customers"},
        {"name": "Restaurant Owner", "description": "Uses fish for preparing meals"},
        {"name": "Captain", "description": "In charge of the boat but not necessarily the owner"}
    ]
    try:
        countries = DBSession.query(Country).all()
        for country in countries:
            for oc in occupation_list:
                o = Occupation(oc['name'], oc['description'], country.id)
                DBSession.add(o)
        DBSession.flush()
    except Exception, e:
        print("Error creating Occupations {0}".format(e))
        DBSession.rollback()
    transaction.commit()


event.listen(Country.__table__, 'after_create', create_default_occupation)
