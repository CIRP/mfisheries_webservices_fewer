from mfisheries.util import display_debug
from abc import ABCMeta, abstractmethod

from mfisheries.models import DBSession
from sqlalchemy import asc
import transaction


class BaseHandler(object):
	__metaclass__ = ABCMeta
	
	def __init__(self, request, context=None):
		self.request = request
		self.context = context
		self.fields = self._get_fields()
		self._target_class = self._get_target_class()
		self.order_func = asc
	
	def _isvalid(self, values):
		for f in self.fields:
			if f not in values:
				return False
		return True
	
	@abstractmethod
	def _create_empty_model(self):
		pass
	
	@abstractmethod
	def _dict_to_model(self, data, rec_id=0):
		pass
	
	@abstractmethod
	def _get_fields(self):
		return []

	@abstractmethod
	def _get_order_field(self):
		pass
	
	# https://stackoverflow.com/questions/553784/can-you-use-a-string-to-instantiate-a-class-in-python
	@abstractmethod
	def _get_target_class(self):
		return None
	
	def _save(self, data, rec_id=0):
		src = self._dict_to_model(data, rec_id)
		try:
			DBSession.add(src)
			DBSession.flush()
			# transaction.commit()
			return src
		except Exception, e:
			DBSession.rollback()
			# transaction.abort()
			display_debug(e, "error")
			return False

	def _process_results(self, res):
		return map(lambda m: m.toJSON(), res)  # convert each record received to JSON format

	def _handle_conditional_get(self, query):
		if "countryid" in self.request.GET:
			cid = self.request.GET['countryid']
			query = query.filter(self._target_class.countryid == cid)
		return query

	def _order_results(self, query):
		return query.order_by(self.order_func(self._get_order_field()))

	def collection_get(self):
		res = []
		if self._target_class:
			query = DBSession.query(self._target_class)
			query = self._order_results(query)
			query = self._handle_conditional_get(query)
			
			res = query.all()
			if len(res) > 0:
				res = self._process_results(res)

		return res
	
	def get(self):
		# Extract id passed as a parameter within the URL
		ws_id = int(self.request.matchdict['id'])
		if self._target_class:
			res = DBSession.query(self._target_class).get(ws_id)
			if res:
				return res.toJSON()
		self.request.response.status = 404
		return False
	
	def collection_post(self):
		# Extract data to be updated from the body of the post request
		try:
			data = self.request.json_body
		except:
			data = self.request.POST
		
		display_debug(data, "info")
		
		# Ensure required fields are specified
		if self._isvalid(data):
			# Attempt to save the new source
			src = self._save(data)
			# If source was successfully saved
			if src:
				# Notify the client using the appropriate HTTP status code
				self.request.response.status = 201
				# Send the record
				return src.toJSON()
		# If we got to this point the request did not complete so send appropriate status message
		display_debug(data, "error")
		self.request.response.status = 400
		return False
	
	def put(self):
		# Extract data to be updated from the body of the put request
		try:
			data = self.request.json_body
		except:
			data = self.request.POST
		# Extract id passed as a parameter within the URL
		rec_id = int(self.request.matchdict['id'])
		display_debug("Attempting to update the following record with the id: {0}".format(rec_id), "info")
		if self._isvalid(data):
			src = self._save(data, rec_id)
			if src:
				return src.toJSON()
		# If we got to this point the request did not complete so send appropriate status message
		self.request.response.status = 400
		display_debug(data, "error")
		return False
	
	def delete(self):
		# Extract id passed as a parameter within the URL
		rec_id = int(self.request.matchdict['id'])
		try:
			# Delete the record
			DBSession.query(self._target_class).filter(self._target_class.id == rec_id).delete()
			return True
		except Exception, e:
			display_debug(e.message, "error")
			self.request.response.status = 400
			return False
