import datetime

import transaction
from sqlalchemy import (
	Column,
	Integer,
	String,
	TIMESTAMP,
	ForeignKey,
	TEXT
)
from sqlalchemy import event

from mfisheries.models import Base
from mfisheries.models import DBSession


class Config(Base):
	__tablename__ = 'configs'
	id = Column(Integer, primary_key=True)
	key = Column(String(50), nullable=False, unique=True)
	value = Column(String(200), nullable=False)
	description = Column(TEXT, default="")
	timecreated = Column(TIMESTAMP, default=datetime.datetime.utcnow)
	createdby = Column(Integer, ForeignKey('user.id'))
	
	def __init__(self, key, value, createdby):
		self.key = key
		self.value = value
		self.createdby = createdby
	
	def toJSON(self):
		return {
			'id': self.id,
			'key': self.key,
			'value': self.value,
			'description': self.description,
			'createdby': self.createdby,
			'timecreated': str(self.timecreated)
		}


def create_default_configs(target, connection_rec, **kw):
	print("Creating Default Empty Configurations to be updated")
	configs = [{
		"key": "system_base_url",
		"value": "localhost",
		"description": "The url that the components in the system can use to reference the current installation",
		"createdby": 1
	}, {
		"key": "firebase_api_key",
		"value": "",
		"description": "The API key to allow access to Firebase services such as cloud messaging",
		"createdby": 1
	}, {
		"key": "firebase_connection_string",
		"value": "",
		"description": "The full path of the URI to access data stored within the Firebase API for the application",
		"createdby": 1
	}, {
		"key": "system_account",
		"value": 1,
		"description": "The User ID for the system account",
		"createdby": 1
	}, {
		"key": "alert_source_interval",
		"value": 5,
		"description": "The Intervals in minutes to poll the various alert sources for new alerts",
		"createdby": 1
	}]
	try:
		for config in configs:
			c = Config(config['key'], config['value'], config['createdby'])
			c.description = config['description']
			DBSession.add(c)
		DBSession.flush()
	except Exception as e:
		print("Error: Inserting System User Failed: %s" % e.message)
		DBSession.rollback()
	transaction.commit()


event.listen(Config.__table__, 'after_create', create_default_configs)
