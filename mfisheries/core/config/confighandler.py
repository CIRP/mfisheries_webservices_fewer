import logging
log = logging.getLogger(__name__)

from cornice.resource import resource
from ..config.config import Config
from mfisheries.models import DBSession
from mfisheries.core import BaseHandler


# http://cornice.readthedocs.io/en/latest/resources.html
@resource(collection_path='/api/config', path='/api/config/{id}', description='config Sources')
class ConfigsHandler(BaseHandler):

	def __init__(self, request, context=None):
		BaseHandler.__init__(self, request, context)

	def _get_fields(self):
		return ['key', 'value', 'createdby']
	
	def _create_empty_model(self):
		return Config("", "", 0)
	
	def _get_target_class(self):
		return Config

	def _get_order_field(self):
		return Config.key

	def _dict_to_model(self, data, rec_id=0):
		# If id is 0, we want to create a new entry
		if rec_id == 0:
			src = self._create_empty_model()
		else:
			src = DBSession.query(self._target_class).get(rec_id)
		# Assign value to the fields of the model
		src.key = data['key']
		src.value = data['value']
		src.createdby = data['createdby']
		src.description = data['description']
		return src


if __name__ == "__main__":
	print("Running ConfigHandler")
	chandler = ConfigsHandler(None)
	print(chandler._get_fields())