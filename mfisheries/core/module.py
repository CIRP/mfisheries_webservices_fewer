import transaction
from sqlalchemy import (
    Column,
    Integer,
    String
)
from sqlalchemy import event
from sqlalchemy.orm import relationship

from mfisheries.models import Base, DBSession


class Module(Base):
    """The SQLAlchemy declarative model class for a Module object"""
    __tablename__ = 'module'
    id = Column(Integer, primary_key=True)
    description = Column(String(100))
    name = Column(String(100), unique=True, nullable=False)
    hasDownload = Column(String(5), default="no")

    children = relationship("UserModule")

    def __init__(self, description="", name="", hasDownload="no"):
        self.description = description
        self.name = name
        self.hasDownload = hasDownload

    def toJSON(self):
        return {'id': self.id, 'description': self.description, 'name': self.name, "hasDownload": self.hasDownload}


def create_default_modules(target, connection_rec, **kw):
    print("Inserting Default Modules")
    modules = [
        Module(name="First Aid"),
        Module(name="SOS"),
        Module(name="Podcast"),
        Module(name="Navigation"),
        Module(name="Weather"),
        Module(name="Photo Diary"),
        Module(name="Alerts"),
        Module(name="LEK"),
        Module(name="Messaging"),
        Module(name="FEWER")
    ]

    try:
        for m in modules:
            DBSession.add(m)

        DBSession.flush()
        transaction.commit()
    except Exception, e:
        print("Error: Inserting Modules Failed: %s" % e.message)
        DBSession.rollback()


event.listen(Module.__table__, 'after_create', create_default_modules)
