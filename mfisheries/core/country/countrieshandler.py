import json
import logging

import transaction  # transaction manager

log = logging.getLogger(__name__)

from cornice import Service
from sqlalchemy import func, asc

from country import Country
from mfisheries.models import DBSession
from mfisheries.core.user import User
from mfisheries.util.secure import validate

# API Operations for Country
# TODO Delete old API format in this revision of mFisheries

from cornice.resource import resource
from mfisheries.core import BaseHandler


@resource(collection_path='/api/countries',path='/api/countries/{id}',description="Countries")
class CountryHandler(BaseHandler):
    def _get_order_field(self):
        return Country.name  # TODO How to sort by country.name

    def __init__(self, request, context=None):
        BaseHandler.__init__(self, request, context)

    def _create_empty_model(self):
        return Country("", "")

    def _get_fields(self):
        return self._create_empty_model().getRequiredFields()

    def _get_target_class(self):
        return Country

    def _dict_to_model(self, data, rec_id=0):
        # If id is 0, we want to create a new entry
        if rec_id == 0:
            src = self._create_empty_model()
        else:
            src = DBSession.query(self._target_class).get(rec_id)

        # Assign value to the fields of the model
        src.name = data['name']
        if "path" in data['path']:
            src.path = data['path']

        return src

    def collection_get(self):
        res = super(CountryHandler, self).collection_get()
        if "auths" in self.request.GET or "auth" in self.request.GET:
            recs = []
            for row in res:
                user = DBSession.query(User).filter(User.countryid == row['id'], User._class == 1).first()
                if user is not None:
                    row['authority'] = user.toJSON()
                recs.append(row)
            res = recs
        return res

#
# addCountry = Service(name="addCountry", path="/api/add/country", description="adds a new country")
# updateCountry = Service(name="updateCountry", path="/api/update/country", description="updates a country record")
# deleteCountry = Service(name="deleteCountry", path='/api/delete/country/{id}', description="deletes a given country")
# getCountries = Service(name='getCountries', path='/api/get/country/list', description='returns a list of all countries')
#
# # countryService = Service(name="country", path="/api/countries", description="country services")
# specificCountryService = Service(name="countrySpecific", path="/api/countries/{id}", description="access specific country records")
#
# @specificCountryService.delete()
# @deleteCountry.get()
# def delete_country(request):
#     if not validate(request):
#         return {'status': 401, 'data': []}
#     try:
#         id = request.matchdict['id']
#         # http://docs.sqlalchemy.org/en/latest/orm/query.html#sqlalchemy.orm.query.Query.delete
#         DBSession.query(Country).filter(Country.id == id).delete()
#         DBSession.flush()
#         transaction.commit()
#         return {'status': 200}
#     except Exception, e:
#         log.error("Error Occurred while deleting country: " + e.message)
#         DBSession.rollback()
#         return {'status': 500}
#
#
# @specificCountryService.put()
# @updateCountry.post()
# def update_country(request):
#     if not validate(request):
#         return {'status': 401, 'data': []}
#     try:
#         country = json.loads(request.body)
#         log.info(country)
#         if 'id' in country:
#             c = DBSession.query(Country).filter_by(id=country['id']).one()
#             if c is not None:
#                 c.name = country['name']
#                 c.path = country['path']
#             DBSession.add(c)
#             DBSession.flush()
#             transaction.commit()
#             log.info("Updated Country: " + c.name)
#             return {'status': 200, 'data': c.toJSON()}
#
#         log.debug("Invalid data for the request received")
#         return {'status': 400, 'response': 'incorrect request received'}
#     except Exception, e:
#         log.error("Error Occurred while updating country:" + e.message)
#         return {'status': 400, "response": 'unable to process request'}
#
#
# # @countryService.get()
# @getCountries.get()
# def get_country_list(request):
#     if not validate(request):
#         return {'status': 401, 'data': []}
#
#     rec = []
#     query = DBSession.query(Country)
#     query = query.order_by(asc(Country.name))
#     if "countryid" in request.GET:
#         query = query.filter(Country.id == request.GET['countryid'])
#
#     cursor = query.all()
#     res = map(lambda m: m.toJSON(), cursor)
#
#     if "auths" in request.GET:
#         for row in res:
#             user = DBSession.query(User).filter(User.countryid == row['id'], User._class == 1).first()
#             if user is not None:
#                 row['authority'] = user.toJSON()
#             rec.append(row)
#
#     log.info("Retrieved {0} countries from the database".format(len(rec)))
#     return {'status': 200, 'data': res}
#
#
# # @countryService.post()
# @addCountry.post()
# def add_country(request):
#     if not validate(request):
#         return {'status': 401, 'data': []}
#     try:
#         country = json.loads(request.body)
#         print country
#         # http://docs.sqlalchemy.org/en/latest/orm/query.html#sqlalchemy.orm.query.Query.one_or_none
#         c = DBSession.query(Country).filter(func.lower(Country.name) == country['name'].lower()).one_or_none()
#         if c is None:  # No country found
#             c = Country(country['path'], country['name'])
#             DBSession.add(c)
#             DBSession.flush()
#             transaction.commit()
#             log.info("Created country: " + c.name)
#             return {'status': 201, "data": c.toJSON()}
#         # Valid value returned from query
#         return request.errors.add('url', 'create', 'country already exists')
#     except Exception, e:
#         log.error("Unable to create country:" + e.message)
#         return request.errors.add('url', 'create', 'country not saved')
