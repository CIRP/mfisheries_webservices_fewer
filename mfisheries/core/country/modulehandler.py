import json
import logging
import os
import shutil

import transaction  # transaction manager
from cornice import Service
from sqlalchemy import func, text, asc

from country import Country
from countrymodule import CountryModule
from mfisheries import APP_ROOT
from mfisheries.core.module import Module
from mfisheries.models import DBSession
from mfisheries.util.secure import validate

log = logging.getLogger(__name__)

ab_module_path = os.path.join(APP_ROOT, "")
rel_module_path = os.path.join("", "")


# Using the BaseHandler
from mfisheries.core import BaseHandler
from cornice.resource import resource


# http://cornice.readthedocs.io/en/latest/resources.html
@resource(collection_path='/api/modules', path='/api/modules/{id}', description='mFisheries Modules')
class ModuleHandler(BaseHandler):
    
    def __init__(self, request, context=None):
        BaseHandler.__init__(self, request, context)
    
    def _get_fields(self):
        return ['name']
    
    def _create_empty_model(self):
        return Module("", "", "")
    
    def _get_target_class(self):
        return Module
    
    def _get_order_field(self):
        return Module.name
    
    def _dict_to_model(self, data, rec_id=0):
        # If id is 0, we want to create a new entry
        if rec_id == 0:
            src = self._create_empty_model()
        else:
            src = DBSession.query(self._target_class).get(rec_id)
        # Assign value to the fields of the model
        src.name = data['name']
        if 'description' in data:
            src.description = data['description']
        if 'hasDownload' in data:
            src.hasDownload = data['hasDownload']
        return src


@resource(collection_path='/api/country/modules', path='/api/country/modules/{id}', description='mFisheries Country Modules')
class CountryModuleHandler(BaseHandler):
    
    def __init__(self, request, context=None):
        BaseHandler.__init__(self, request, context)
    
    def _get_fields(self):
        return ['countryid', 'moduleid']
    
    def _create_empty_model(self):
        return CountryModule("", "", "")
    
    def _get_target_class(self):
        return CountryModule
    
    def _get_order_field(self):
        return CountryModule.countryid
    
    def _dict_to_model(self, data, rec_id=0):
        # If id is 0, we want to create a new entry
        if rec_id == 0:
            src = self._create_empty_model()
        else:
            src = DBSession.query(self._target_class).get(rec_id)
        # Assign value to the fields of the model
        src.countryid = data['countryid']
        src.moduleid = data['moduleid']
        if "path" in data:
            src.path = data['path']
        
        return src


# API Operations for Modules
# addModule = Service(name="addModule", path="/api/add/module", description="adds a new module")
# getModules = Service(name="getModules", path="/api/get/modules", description="get all modules")
# getModulesByCountry = Service(name='getModulesByCountry', path='/api/get/country/modules/{country_id}',
#                               description='returns all modules associated with a country')
# deleteModule = Service(name="deleteModule", path="/api/delete/module/{id}", description="deletes a given module")
# updateModule = Service(name="updateModule", path="/api/update/module", description="updates a given module")
#
# # API Operations for Country Modules
# addCountryModule = Service(name="addCountryModule", path="/api/add/country/module", description="adds country modules")
# getAllCountryModules = Service(name="getAllCountryModules", path='/api/get/all/country/modules',
#                                description="returns all country modules")
# deleteCountryModule = Service(name="deleteCountryModule", path="/api/delete/country/module/{id}",
#                               description="deletes country module")
#
# # API Operations for Country Module Files
addModuleFile = Service(name="addModuleFile", path="/api/add/modulefile", description="adds a new module file")


# Refactored API
# moduleService = Service(
#     name="module",
#     path="/api/modules",
#     description="modules service"
# )
# specificModuleService = Service(
#     name="specificModule",
#     path="/api/modules/{id}",
#     description="delete modules service"
# )
# countryModuleService = Service(
#     name="countryModule",
#     path="/api/modules/country",
#     description="country modules service"
# )
# specificCountryModuleService = Service(
#     name="deleteCountryModule",
#     path="/api/modules/country/{id}",
#     description="delete country modules service"
# )
# moduleFileService = Service(
#     name="moduleFileService",
#     path="/api/modules/file",
#     description="adds a new module file"
# )
#
#
# @deleteCountryModule.get()
# @specificCountryModuleService.delete()
# def delete_country_module(request):
#     if not validate(request):
#         return {'status': 401, 'data': []}
#     result = deletecountrymodule(request.matchdict['id'])
#     if result:
#         return {'status': 200, 'response': 'Country Module Record successfully deleted'}
#     else:
#         return {'status': 500, 'response': 'Country Module Record was not deleted'}
#
#
# @getAllCountryModules.get()
# @countryModuleService.get()
# def get_country_modules(request):
#     if not validate(request):
#         return {'status': 401, 'data': []}
#     result = getcountrymodules()
#     if not result:
#         return {'status': 204, 'data': 'No modules exist'}
#     else:
#         return {'status': 200, 'data': result}
#
#
# @getModules.get()
# # @moduleService.get()
# def get_modules(request):
#     if not validate(request):
#         return {'status': 401, 'data': []}
#     result = getmodules()
#     if not result:
#         return {'status': 204, 'data': 'No modules exist'}
#     else:
#         return {'status': 200, 'data': result}
#
#
# @deleteModule.get()
# # @specificModuleService.delete()
# def delete_module(request):
#     if not validate(request):
#         return {'status': 401, 'data': []}
#     result = deletemodule(request.matchdict['id'])
#     if result:
#         return {'status': 200}
#     else:
#         return {'status': 500}
#
#
# @updateModule.post()
# # @moduleService.put()
# @specificCountryModuleService.put()
# def update_module(request):
#     if not validate(request):
#         return {'status': 401, 'data': []}
#     module = json.loads(request.body)
#     print module
#     try:
#         if 'id' in module:
#             m = DBSession.query(Module).filter_by(id=module['id']).one()
#             print m
#             if m is not None:
#                 m.hasDownload = module['hasDownload']
#                 m.name = module['name']
#                 DBSession.add(m)
#                 DBSession.flush()
#                 transaction.commit()
#                 return {'status': 200, 'data': 'Updated Module record'}
#
#         return {'status': 400, 'response': 'Incorrect Request Received'}
#     except Exception, e:
#         print e.message
#         return {'status': 400, "response": 'Unable to Update Module'}
#
#
# @getModulesByCountry.get()
# @countryModuleService.get()
# def get_module_by_country(request):
#     if not validate(request):
#         return {'status': 401, 'data': []}
#     if 'country_id' in request.matchdict:
#         result = getmodulebycountry(request.matchdict['country_id'])
#         return {'status': 200, 'data': result}
#     else:
#         return {'status': 204, 'data': "No Country Specified"}
#
#
# @addModule.post()
# # @moduleService.post()
# def add_module(request):
#     if not validate(request):
#         return {'status': 401, 'data': []}
#     module = json.loads(request.body)
#
#     if findModuleByName(module['name']) is None:
#         new_module = Module(module['description'], module['path'], module['hasDownload'])
#         if insertModule(new_module):
#             return {'status': 201}
#         else:
#             return request.errors.add('url', 'create', 'user not registered')
#     else:
#         return request.errors.add('url', 'create', 'module already exists')
#
#
# Followed from: http://docs.pylonsproject.org/projects/pyramid-cookbook/en/latest/forms/file_uploads.html
@addModuleFile.post()
# @moduleFileService.post()
def upload_modulefile(request):
    if not validate(request):
        return {'status': 401, 'data': []}
    try:
        # check parameters
        filename = request.POST['upload'].filename
        upfile = request.POST['upload'].file
        countryid = request.POST['countryid']
        moduleid = request.POST['moduleid']
    except Exception, e:
        return {'status': 400, "response": "Invalid request"}

    # All parameters received
    extension = filename.split(".")
    print extension[-1]
    if len(extension) > 1:
        if extension[-1] == "zip":
            try:
                country = DBSession.query(Country).get(countryid)
                module = DBSession.query(Module).get(moduleid)
                if country is not None and module is not None:

                    country_path = os.path.join(ab_module_path, country.path)
                    local_path = os.path.join(rel_module_path, country.path)
                    # Ensure country path exists
                    if not os.path.exists(country_path):
                        os.makedirs(country_path)
                    # Set full path for file
                    file_path = os.path.join(country_path, filename)
                    local_path = os.path.join(local_path, filename)
                    temp_file_path = file_path + '~'
                    upfile.seek(0)
                    # Upload file to temp location
                    with open(temp_file_path, 'wb') as output_file:
                        shutil.copyfileobj(upfile, output_file)
                    # Once upload complete then copy to desired location
                    os.rename(temp_file_path, file_path)

                    log.info("Saved data at: " + file_path)
                    cm = save_module_file(countryid, moduleid, local_path)
                    if cm is not None:
                        return {'status': 200, "response": "correct file type"}
                    else:
                        return {'status': 500, "response": "Error occurred while saving file"}
                else:
                    return {'status': 500, "response": "Country or Module specified was invalid"}
            except Exception, e:
                print(e.message)
                log.error("Error while uploading module file: " + str(e))
                return {'status': 500, "response": "Error occurred while saving file"}
    return {'status': 400, "response": "Incorrect file type"}

def save_module_file(countryid, moduleid, file_path):
    try:
        cm = DBSession.query(CountryModule).filter(CountryModule.countryid == countryid,
                                                   CountryModule.moduleid == moduleid).first()
        if cm is None:
            cm = CountryModule(countryid, file_path, moduleid)
        else:
            cm.path = file_path
        # Will perform update if exists and create otherwise
        DBSession.add(cm)
        # Update Module
        module = DBSession.query(Module).filter(Module.id == moduleid).one()
        module.hasDownload = "yes"
        DBSession.add(module)

        # Persist Changes
        DBSession.flush()
        transaction.commit()
        return cm
    except Exception, e:
        print e.message
        DBSession.rollback()
        return None

#
# @addCountryModule.post()
# @countryModuleService.post()
# def add_country_modules(request):
#     if not validate(request):
#         return {'status': 401, 'data': []}
#     countrymodules = json.loads(request.body)
#     response = []
#     for cm in countrymodules:
#         new_cm = DBSession.query(CountryModule).filter(CountryModule.countryid == cm['countryid']).filter(
#             CountryModule.moduleid == cm['moduleid']).first()
#         if new_cm is None:
#             new_cm = CountryModule(cm['countryid'], cm['path'], cm['moduleid'])
#             res = insert_country_module(new_cm)
#             if res:
#                 response.append({"cid": cm['countryid'], "mid": cm['moduleid'], "added": res})
#             else:
#                 return {'status': 400, "response": "Unable to create country module"}
#         else:
#             return {'status': 400, "response": "Country Module Already Exist"}
#
#     return {'status': 201, "response": response}
#
#
# def deletecountrymodule(id):
#     try:
#         nu = DBSession.query(CountryModule).filter(CountryModule.id == id).delete()
#         # cm = DBSession.query(CountryModule).filter(CountryModule.id == id).one()
#         # print "Path is : " + cm.path
#         # print len(cm.path)
#         # if cm is not None:
#         # 	if cm.path is not None and len(cm.path) > 1:
#         # 		return delete_module_file(id, cm)
#         # 	else:
#         # 		DBSession.delete(cm)
#         # 		DBSession.flush()
#         # 		transaction.commit()
#         return True
#     # else:
#     # 	return False
#     except Exception, e:
#         print e.message
#         return None
#
#
# def insert_country_module(countrymodule):
#     try:
#         DBSession.add(countrymodule)
#         DBSession.flush()
#         transaction.commit()
#         return True
#     except Exception, e:
#         print e.message
#         DBSession.rollback()
#         return False
#
#
# def delete_module_file(countrymoduleid, cm=None):
#     try:
#         if cm is None:
#             cm = DBSession.query(CountryModule).filter(CountryModule.id == countrymoduleid).one()
#         file_path = ""
#         if cm is not None:
#             print "Path detected is: " + cm.path
#             if cm.path is not None or len(cm.path) > 1:
#                 file_path = os.path.join(APP_ROOT, cm.path)
#
#             moduleid = cm.moduleid
#             DBSession.delete(cm)
#             DBSession.flush()
#             transaction.commit()
#
#             # Check to see if any other resources exist for this module
#             hasDownload = False
#             mods = DBSession.query(CountryModule).filter(CountryModule.moduleid == moduleid).all()
#             for m in mods:
#                 if m.path is not None or len(m.path) > 1:
#                     hasDownload = True
#
#             if not hasDownload:
#                 module = DBSession.query(Module).filter(Module.id == moduleid).one()
#                 if module is not None:
#                     module.hasDownload = "no"
#                     DBSession.add(module)
#                     DBSession.flush()
#                     transaction.commit()
#
#             if os.path.exists(file_path):
#                 os.remove(file_path)
#
#             return True
#         else:
#             return False
#     except Exception as e:
#         print e.message
#         DBSession.rollback()
#         return False
#
#

#
#
# def insertModule(module):
#     try:
#         DBSession.add(module)
#         DBSession.flush()
#         transaction.commit()
#         return True
#     except Exception, e:
#         reason = e.message
#         print reason
#         if "Duplicate entry" in reason:
#             print "%s already in table." % e.params[1]
#             DBSession.rollback()
#         return False
#
#
# def findModuleByName(name):
#     try:
#         module = DBSession.query(Module).filter(func.lower(Module.name) == name.lower()).one()
#         print module.toJSON()
#         return module.toJSON()
#
#     except Exception, e:
#         reason = e.message
#         print reason
#         return None
#
#
# def getmodules():
#     arr = []
#     try:
#         result = DBSession.query(Module).order_by(asc(Module.name)).all()
#         for model in result:
#             arr.append(model.toJSON())
#         return arr
#     except Exception, e:
#         print e
#         return {}
#
#
# def getcountrymodules():
#     try:
#         stmt = text(
#             "SELECT cm.id, c.name as countryname, c.name as country, m.name as modulename, m.name, cm.path, cm.countryid, cm.moduleid from countrymodule cm, country c, module m where cm.countryid = c.id and cm.moduleid = m.id order by countryname, modulename ASC")
#         result = DBSession.execute(stmt)
#         arr = []
#         for model in result:
#             arr.append(dict(model))
#         return arr
#     except Exception, e:
#         print e
#         return {}
#
#
# def getmodulebycountry(countryid):
#     try:
#         stmt = text(
#             "SELECT cm.id, c.name as countryname, c.name as country, m.name as modulename, m.name as name, cm.path, cm.countryid, cm.moduleid, m.hasDownload from module m, countrymodule cm, country c where c.id = cm.countryid and m.id = cm.moduleid and c.id = :id order by countryname, modulename ASC")
#         result = DBSession.execute(stmt, {'id': countryid})
#         arr = []
#         for model in result:
#             arr.append(dict(model))
#         return arr
#     except Exception, e:
#         print e
#         return 0
#
#
# def deletemodule(id):
#     try:
#         nu = DBSession.query(Module).filter(Module.id == id).delete()
#
#         return True
#     except Exception, e:
#         print e.message
#         return None
