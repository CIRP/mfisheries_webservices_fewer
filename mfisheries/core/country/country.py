import transaction
from sqlalchemy import (
	Column,
	Integer,
	String
)
from sqlalchemy import event
from sqlalchemy.orm import relationship

from mfisheries.models import Base, DBSession


class Country(Base):
	"""The SQLAlchemy declarative model class for a Country object"""
	__tablename__ = 'country'
	id = Column(Integer, primary_key=True)
	path = Column(String(100))
	name = Column(String(100), unique=True, nullable=False)

	countryid = 0

	# Relationships
	occupations = relationship("Occupation")

	def __init__(self, path, name):
		self.path = path
		self.name = name

	def toJSON(self):
		return {
			'id': self.id,
			'path': self.path,
			'name': self.name,
			'code': self.getCode()
		}

	def getRequiredFields(self):
		return ["name"]

	def getCode(self):
		codes = Country.from_name_to_code()
		code = codes[self.name] if self.name in codes else ""
		return code

	@staticmethod
	def from_name_to_code():
		return {
			"Antigua and Barbuda": "atb",
			"Bahamas": "bhs",
			"Barbados": "brb",
			"Belize": "blz",
			"Dominica": "dom",
			"Grenada": "gre",
			"Guyana": "guy",
			"St Kitts and Nevis": "skn",
			"St Lucia": "slu",
			"St Vincent and the Grenadines": "svg",
			"Tobago": "tob",
			"Trinidad": "tri"
		}

	@staticmethod
	def from_code_to_name():
		"""
		Function to represent country based on 3 letter codes derived from:
		http://www.nationsonline.org/oneworld/country_code_list.htm
		:return: dictionary of country codes
		"""
		return {  # TODO Consider placing in a database and defined through user interface
			"atb": "Antigua and Barbuda",
			"bhs": "Bahamas",
			"brb": "Barbados",
			"blz": "Belize",
			"dom": "Dominica",
			"gre": "Grenada",
			"guy": "Guyana",
			"skn": "St Kitts and Nevis",
			"slu": "St Lucia",
			"svg": "St Vincent and the Grenadines",
			"tob": "Tobago",
			"tri": "Trinidad"
		}


def create_default_countries(target, connection_rec, **kw):
	print("Creating Default Countries")
	countries = [
		["static/country_modules/trinidad", "Trinidad"],
		["static/country_modules/tobago", "Tobago"],
		["static/country_modules/grenada", "Grenada"],
		["static/country_modules/dominica", "Dominica"],
		["static/country_modules/slu", "St Lucia"],
		["static/country_modules/skn", "St Kitts and Nevis"],
		["static/country_modules/svg", "St Vincent and the Grenadines"],
		["static/country_modules/atb", "Antigua and Barbuda"],
		["static/country_modules/belize", "Belize"],
		["static/country_modules/brb", "Barbados"],
		["static/country_modules/bhs","Bahamas"],
		["static/country_modules/guy","Guyana"]
	]
	try:
		for c in countries:
			country = Country(c[0], c[1])
			DBSession.add(country)
		DBSession.flush()
	except Exception as e:
		print("Error: Inserting Countries Failed: %s" % e.message)
		DBSession.rollback()
	transaction.commit()


event.listen(Country.__table__, 'after_create', create_default_countries)
