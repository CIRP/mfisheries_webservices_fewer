import datetime
import hashlib

import transaction
from sqlalchemy import (
	Column,
	Integer,
	String,
	TIMESTAMP,
	ForeignKey,
	UniqueConstraint
)
from sqlalchemy import event
from sqlalchemy.orm import relationship

from mfisheries.core.country import Country
from mfisheries.models import Base
from mfisheries.models import DBSession


class User(Base):
	""" SQLAlchemy declarative model class for a User object"""
	__tablename__ = "user"
	id = Column(Integer, primary_key=True)
	villageid = Column(Integer, ForeignKey('village.id'))
	username = Column(String(100), nullable=False)
	password = Column(String(100), nullable=False)
	type = Column(Integer)
	fname = Column(String(100))
	lname = Column(String(100))
	hstreet = Column(String(100))
	countryid = Column(Integer, ForeignKey('country.id'))
	mobileNum = Column(String(15))
	homeNum = Column(String(15))
	age = Column(Integer)
	enable = Column(Integer)
	_class = Column(Integer, default=3, nullable=False)
	ext = Column(String(100))
	fisherFolkId = Column(Integer)
	aquacultureId = Column(Integer)
	email = Column(String(100))
	regTime = Column(TIMESTAMP, default=datetime.datetime.utcnow)
	gcmId = Column(String(200))
	computer_at_home = Column(String(100))
	disabilities = Column(String(100))
	nat_id = Column(String(200))
	nat_dp = Column(String(100))
	passport = Column(String(100))
	pic = Column(String(100))
	
	__table_args__ = (UniqueConstraint('username', 'password', 'email', name='unique_user'),)
	
	country = relationship("Country", foreign_keys=[countryid], lazy='subquery')
	village = relationship("Village", foreign_keys=[villageid], lazy='subquery')
	
	def __init__(self, username, password, fname, lname, email, userclass=3, country=1):
		self.username = username
		self.password = password
		self.fname = fname
		self.lname = lname
		self.email = email
		self._class = userclass
		self.countryid = country
	
	def toJSON(self):
		json = {}
		json['id'] = self.id
		json['villageid'] = self.villageid
		json['username'] = self.username
		# json['password'] = self.password
		json['type'] = self.type
		json['fname'] = self.fname
		json['lname'] = self.lname
		json['hstreet'] = self.hstreet
		json['homeNum'] = self.homeNum
		json['mobileNum'] = self.mobileNum
		json['age'] = self.age
		json['enable'] = self.enable
		json['_class'] = self._class
		json['ext'] = self.ext
		json['fisherFolkId'] = self.fisherFolkId
		json['aquacultureId'] = self.aquacultureId
		json['email'] = self.email
		json['regTime'] = str(self.regTime)
		json['gcmId'] = self.gcmId
		json['computer_at_home'] = self.computer_at_home
		json['disabilities'] = self.disabilities
		json['nat_id'] = self.nat_id
		json['nat_dp'] = self.nat_dp
		json['countryid'] = self.countryid
		if self.country:
			json['country'] = self.country.name
		json['passport'] = self.passport
		json['pic'] = self.pic
		return json
	
	@staticmethod
	def get_roles():
		return {
			"admin": {
				"name": 'Administrator',
				"code": 2,
				"description": "The technical administrator for the mFisheries installation. Has oversight over the entire system.",
				"alias": "Tech Admin",
				"scope": {
					"visibility": 'global',
					"privilege": 'admin'
				},
				"default_path": '/admin/users'
			},
			"coastguard": {
				"name": 'Coast Guard',
				"code": 1,
				"description": "Account for Safety at Sea organization. Displays information critical to responding to Fishers in distress at sea.",
				"scope": {
					"visibility": 'country',
					"privilege": 'rescue'
				},
				"default_path": '/all/tracks'
			},
			"user": {
				"name": 'User',
				"code": 3,
				"description": "The end-user for system. Fishers and mobile app users belong to this group",
				"alias": "Fisherfolk",
				"scope": {
					"visibility": 'country',
					"privilege": 'basic'
				},
				"default_path": '/image/diary'
			},
			"reviewer": {
				"name": 'Reviewer',
				"code": 4,
				"description": "A global read only user.",
				"scope": {
					"visibility": 'global',
					"privilege": 'read-only'
				},
				"default_path": '/admin/users'
			},
			"organization": {
				"name": "Country Reviewer",
				"code": 5,
				"description": "A read only user for information of a specific",
				"scope": {
					"visibility": 'country',
					"privilege": 'read-only'
				},
				"default_path": '/admin/users'
			},
			"country_admin": {
				"name": "Country Administrator",
				"code": 6,
				"description": "The administrative user that configures modules and manage data for specific countries.",
				"alias": "Country Admin",
				"scope": {
					"visibility": 'country',
					"privilege": 'admin'
				},
				"default_path": '/admin/users'
			},
			"fewer_admin": {
				"name": "Agency Administrator",
				"code": 7,
				"description": "The administrator that will manage fishers within an agency.",
				"alias": "Agency Admin",
				"scope": {
					"visibility": 'agency',
					"privilege": 'admin'
				},
				"default_path": '/admin/users'
			},
			"fewer_kiosk": {
				"name": "Data Consumer",
				"code": 8,
				"description": "The user that will simply consume basic information.",
				"alias": "Kiosk",
				"scope": {
					"visibility": 'country',
					"privilege": 'read-only'
				},
				"default_path": '/admin/users'
			},
			"fisher_organization": {
				"name": "Organization User",
				"code": 9,
				"description": "The user that will be able to view agency related information",
				"alias": "Member",
				"scope": {
					"visibility": 'agency',
					"privilege": 'basic'
				},
				"default_path": '/image/diary'
			}
		}
	
	@staticmethod
	def get_code_from_role(role):
		roles = User.get_roles()
		return roles[role]['code']
	
	@staticmethod
	def get_role_from_code(code):
		roles = User.get_roles()
		for role in roles:
			if roles[role]['code'] == code:
				return role


def create_default_admins():
	print("Creating Overall system users")
	
	user_details = [{
		"username": "system",
		"password": "Y!5ewrAtrun&puf3arabetreK",
		"first": "system",
		"last": "",
		"email": "",
		"user_class": 0
	}, {
		"username": "mfishAdmin",
		"password": "mFisher@PassC0mpl3x",
		"first": "mFisheries",
		"last": "Administrator",
		"email": "mfisheries2011@gmail.com",
		"user_class": 2
	}]
	
	for usr in user_details:
		password = hashlib.sha256(usr['password']).hexdigest()
		u = User(
			usr['username'],
			password,
			usr['first'],
			usr['last'],
			usr['email'],
			usr['user_class']
		)
		DBSession.add(u)


def create_default_country_users(target, connection_rec, **kw):
	print("Creating Default country users")
	user_classes = [{
		"name": "adminuser",
		"role": User.get_code_from_role("country_admin")
	}, {
		"name": "coastguard",
		"role": User.get_code_from_role("coastguard")
	}, {
		"name": "fisherorg",
		"role": User.get_code_from_role("fisher_organization")
	}]
	
	countries = DBSession.query(Country).all()
	for country in countries:
		for u_class in user_classes:
			username = "{0}{1}".format(country.getCode(), u_class['name'])
			role = int(u_class['role'])
			user = User(
				username,
				"SiazLEbRoawo",  # Random password
				u_class['name'],  # First Name
				country.name,  # Last Name
				"noemail@email.com",  # Email
				role,
				country.id
			)
			DBSession.add(user)
			

def create_default_users(target, connection_rec, **kw):
	print("Creating Default Users")
	try:
		create_default_admins()
		# create_default_country_users()
		DBSession.flush()
	except Exception, e:
		print("Error: Inserting System User Failed: %s" % e.message)
		DBSession.rollback()
	transaction.commit()


event.listen(User.__table__, 'after_create', create_default_users)
event.listen(Country.__table__, 'after_create', create_default_country_users)
