from sqlalchemy import (
    Column,
    Integer,
    String,
    ForeignKey
)

from mfisheries.models import Base


class PhoneDetails(Base):
    """The SQLAlchemy delcarative model class for a PhoneDetails object"""

    __tablename__ = 'phonedetails'
    id = Column(Integer, primary_key=True)
    userid = Column(Integer, ForeignKey('user.id'), nullable=False)
    imei = Column(Integer)
    phonenumber = Column(String(15))
    puk1 = Column(String(100))
    puk2 = Column(String(100))
    homephone = Column(String(15))

    def __init__(self, userid, imei, phonenumber, puk1, puk2, homephone):
        self.userid = userid
        self.imei = imei
        self.phonenumber = phonenumber
        self.puk1 = puk1
        self.puk2 = puk2
        self.homephone = homephone
