from sqlalchemy import (
    Column,
    Integer,
    String,
    ForeignKey
)

from mfisheries.models import Base


class Vessels(Base):
    """The SQLAlchemy declarative model class for a Vessel object"""
    __tablename__ = 'vessels'
    id = Column(Integer, primary_key=True)
    userid = Column(Integer, ForeignKey('user.id'), nullable=False)
    regnumber = Column(Integer)
    description = Column(String(100))
    length = Column(Integer)
    hullcolor = Column(String(100))
    insidecolor = Column(String(100))
    hulltype = Column(String(100))

    def __init__(self, userid, regnumber, description, length, hullcolor, insidecolor, hulltype):
        self.userid = userid
        self.regnumber = regnumber
        self.description = description
        self.length = length
        self.hullcolor = hullcolor
        self.insidecolor = insidecolor
        self.hulltype = hulltype

    def toJSON(self):
        return {
            'id': self.id,
            'userid': self.userid,
            'regnumber': self.regnumber,
            'length': self.length,
            'hullcolor': self.hullcolor,
            'insidecolor': self.insidecolor,
            'hulltype': self.hulltype
        }
