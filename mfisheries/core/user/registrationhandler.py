import hashlib
import json
import logging
import urllib

import transaction
from sqlalchemy import text

from mfisheries.core.user.alternativecontact import AlternativeContact
from mfisheries.core.user.occupations import Occupations
from mfisheries.core.user.userhandler import get_user_by_id
from mfisheries.core.user.users import User
from mfisheries.core.user.vessels import Vessels
from mfisheries.models import DBSession
from mfisheries.util.secure import validate

log = logging.getLogger(__name__)

from cornice import Service

googleregister = Service(name='googleregister', path='/api/add/google/user',
                         description='returns a list of all countries')
userlogin = Service(name="userlogin", path='/api/user/login',
                    description="authenticates coastguard or administrator users")
updateUserProfile = Service("updateUserProfile", path="/api/update/user/profile", description="updates user's profile")
getFullProfileInfo = Service('getFullProfileInfo', path='/api/get/user/full/profile/{userid}',
                             description='returns full user profile')


userService = Service(name='userService', path='/api/user', description='user service')
loginService = Service(name='loginService', path='/api/user/login', description='login service')

userByName = Service(name='userServiceUsername', path='/api/user/username/{username}', description='user service by username')

@getFullProfileInfo.get()
@userService.get()
def get_full_profile(request):
	if not validate(request):
		return {'status': 401, 'data': []}
	profile = getfullprofileinfo(request.matchdict['userid'])
	if profile is not None:
		return {"status": 200, "data": profile}
	else:
		return {"status": 404, "data": []}


@updateUserProfile.post()
@userService.put()
def update_user_profile(request):
	if not validate(request):
		return {'status': 401, 'data': []}
	profiledetails = json.loads(request.body)
	userdata = {}
	log.info("Received from the server:")
	log.info(profiledetails)
	if profiledetails is not None and profiledetails is not False:
		userdata = updateuserprofile(profiledetails)
		DBSession.flush()
		log.info("Completed attempting update user profile function and produced: {0}".format(userdata))
		if userdata is not None or userdata is not False:
			return {"status": 200, "data": userdata}
	
	return {"status": 400, "data": "invalid operation requested"}


@userlogin.post()
@loginService.post()
def login_user(request):
	log.info("Attempting to log-in user")
	if not validate(request):
		log.info("Unauthorized request received")
		return {'status': 401, 'data': []}
		
		# user_data = json.loads(request.body)
	user_data = request.json_body
	log.info("Received from the server: {0} ".format(user_data))
	
	if len(user_data) > 0:
		password = hashlib.sha256(user_data['password']).hexdigest()
		data = check_user(user_data['username'], password)
		
		if data is None:
			return request.errors.add('url', 'create', 'not found')
		else:
			return {"status": 200, "data": data.toJSON()}
	else:
		log.error("Received no data from the client")
		return request.errors.add('url', 'create', 'received empty user data')


@googleregister.post()
@userService.post()
def add_user(request):
	log.info("Adding User through Google log-in")
	if not validate(request):
		log.info("Unauthorized request received")
		return {'status': 401, 'data': []}

	user_data = json.loads(request.body)['data']

	# if there are no additional modules then set to empty string
	mod = json.loads(request.body)['modules'] if len(json.loads(request.body)['modules']) > 0 else [""]
	
	# modulesREquireAddInfo = ["photo diary", "alerts", "sos", "navigation", "lek"]
	m = {'modules[]': mod}
	modules = urllib.urlencode(m, True)
	
	if "username" in user_data and "password" in user_data:
		password = hashlib.sha256(user_data['password']).hexdigest()
		data = check_user(user_data['username'], password)
		if data:
			log.info("User already exists with an id of: " + str(data.id))
			#  add userid to module list
			mod.append("userid_" + str(data.id))
			#  convert list of modules and userid to url string
			m = {'modules[]': mod}
			log.info("URL encoding modules")
			modules = urllib.urlencode(m, True)
			return {'status': 200, "data": data.toJSON(), "message": "user already exists"}
		else:
			user = User("", "", "", "", "")
			user.username = user_data['username']
			user.password = user_data['password']
			user.fname = user_data['fname']
			user.lname = user_data['lname']
			user.email = user_data['email']
			user.countryid = user_data['countryid']

			user = register_user(user)  # get newly created user from database
			if user:
				mod.append("country")
				mod.append("userid_" + str(user.id))
				m = {'modules[]': mod}
				modules = urllib.urlencode(m, True)
				log.debug(modules)
				return {
					"status": 201,
					"modules": modules,
					"userid": str(user.id),
					"data": user.toJSON()
				}
			else:
				return request.errors.add('url', 'create', 'user not registered')
	else:
		return request.errors.add('url', 'create', 'received empty user data')


@userByName.get()
def get_user_by_username(request):
	user_name = request.matchdict['username']
	user = DBSession.query(User).filter(User.username == user_name).one_or_none()
	if user:
		return user.toJSON()
	else:
		request.response.status = 401
		return None

def check_user(username, password):
	log.info("Attempting to Check credentials of user {0}".format(username))
	# res = DBSession.query(User).filter(User.username == username, User.password == password).one_or_none()
	res = DBSession.query(User).filter(User.username == username).one_or_none()
	return res


def register_user(data):
	try:
		DBSession.add(data)
		DBSession.flush()
		data.toJSON()
		transaction.commit()
		log.info("Completed registration of student: {0}".format(data))
		return data
	except Exception, e:
		log.error("Unable to register user: {0}".format(e.message))
		DBSession.rollback()
		return None


def updateuserprofile(data):
	user_id = ""
	res = True
	if 'initial' in data and res:
		log.info("Inside of initial section")
		res = updateinitialprofile(data['initial'])
		log.debug("Update Initial Profile returned: " + str(res))
		user_id = data['initial']['userid']
	if 'basic' in data and res:
		log.info("Inside of basic section")
		res = updatebasicprofile(data['basic'])
		log.debug("Update Basic Profile returned: " + str(res))
		user_id = data['basic']['userid']
	if 'standard' in data and res:
		log.info("inside of standard section")
		res = updateStandardProfile(data['standard'])
		log.debug("Update Standard Profile returned: " + str(res))
		user_id = data['standard']['userid']
	if 'full' in data and res:
		log.info("Inside of full section")
		res = updateFullProfile(data['full'])
		log.debug("Update Full Profile returned: " + str(res))
		user_id = data['full']['userid']
	
	userdata = get_user_by_id(user_id)
	log.info("Extracting User information From db: {0}".format(userdata))
	
	if res is False:  # Error Occurred when trying to save the data
		return False
	elif userdata is not None:
		return userdata
	else:
		return {}


def updateinitialprofile(data):
	try:
		log.info("Data in the update initial profile function: {0}".format(data))
		user = DBSession.query(User).filter(User.id == data['userid']).one()
		if user is not None:
			user.countryid = data['countryid']
			user.mobileNum = data['mobileNum']
			DBSession.add(user)
			DBSession.flush()
			transaction.commit()
			return True
	except Exception, e:
		log.error("Error Occurred: " + e.message)
	return False


def updatebasicprofile(data):
	try:
		user = DBSession.query(User).filter(User.id == data['userid']).one()
		user.age = data['age']
		user._class = 3
		log.info("Attempting to update: {0}".format(user))
		# Persist to database
		DBSession.add(user)
		DBSession.flush()
		transaction.commit()
		# Add the occupation of the user to db
		adduseroccupations(data['userid'], data['occupations'])
		
		return True
	except Exception, e:
		log.error("Failed Inserting user profile:" + e.message)
		return False


def adduseroccupations(userid, data):
	try:
		# Create an user-occupation object for each
		for occupation in data:
			o = Occupations(userid, occupation)
			DBSession.add(o)
		# After Saving Items Attempt to Commit to the database
		DBSession.flush()
		transaction.commit()
	except Exception, e:
		reason = e.message
		log.error("Failed Inserting user occupations: " + reason)
		DBSession.rollback()


def updateStandardProfile(data):
	try:
		log.debug(data)
		user = DBSession.query(User).filter(User.id == data['userid']).one()
		user.hstreet = data['hstreet']
		user.villageid = data['village']
		DBSession.add(user)
		if data['hasvessel'] == "yes":
			log.info("Has Vessel was passed as true")
			vessel = Vessels(data['userid'],
			                 data['vessel']['regnumber'],
			                 data['vessel']['description'],
			                 data['vessel']['length'],
			                 data['vessel']['hullcolor'],
			                 data['vessel']['insidecolor'],
			                 data['vessel']['hulltype'])
			
			DBSession.add(vessel)
		# DBSession.flush()
		transaction.commit()
		log.info("Added Standard Profile information for User")
		return True
	except Exception, e:
		log.error("Unable to update standard profile: " + e.message)
		DBSession.rollback()
	
	return False


def updateFullProfile(data):
	try:
		user = DBSession.query(User).filter(User.id == data['userid']).one()
		user.nat_id = data['nat_id']
		user.nat_dp = data['nat_dp']
		user.passport = data['passport']
		DBSession.add(user)
		if "emg" in data:
			altcontact = AlternativeContact(
				data['userid'],
				data['emg']['village'],
				data['emg']['fname'],
				data['emg']['lname'],
				data['emg']['street'],
				data['emg']['cellnum'],
				data['emg']['homenum'],
				data['emg']['email']
			)
			DBSession.add(altcontact)
		
		# DBSession.flush()
		transaction.commit()
		log.info("Saved full Profile successful")
		return True
	except Exception, e:
		log.error("Unable to save full profile: " + e.message)
		DBSession.rollback()
	return False


def getfullprofileinfo(userid):
	res = []
	try:
		stmt = text(
			"SELECT u.id, u.fname, u.lname, u.email, u.username,u.nat_dp, u.nat_id, u.passport, u.hstreet, v.name as village, u.age, c.name as country, u.mobileNum, u.countryid, u.villageid as villageid from user u, village v, country c where (u.villageid = v.id or u.villageid is null) and u.countryid = c.id and u.id = :userid")
		result = DBSession.execute(stmt, {'userid': userid})
		arr = []
		for model in result:
			arr.append(dict(model))
		
		if len(arr) > 0:
			res = arr[0]
			# get emergency contact
			stmt = text("select a.id as emergencyid, a.villageid, a.fname, a.lname, a.street, a.cellnum, a.homenum, a.email, v.name as village\
			 from alternativecontact a, village v where a.villageid = v.id and a.userid = :userid")
			result = DBSession.execute(stmt, {'userid': userid})
			for model in result:
				if not res.has_key("emg"):
					res['emg'] = []
				res["emg"].append(dict(model))
			
			# get vessel information
			stmt = text("select v.id as vesselid, v.regnumber, v.description, v.length, v.hullcolor, v.insidecolor, v.hulltype\
						from vessels v where v.userid = :userid")
			result = DBSession.execute(stmt, {'userid': userid})
			for model in result:
				if not res.has_key("vessel"):
					res['vessel'] = []
				res["vessel"].append(dict(model))
			
			# get occupation
			stmt = text(
				"SELECT ocs.userid, ocs.occupationid, o.type FROM `occupations` ocs, occupation o where o.id = ocs.occupationid and ocs.userid = :userid")
			result = DBSession.execute(stmt, {'userid': userid})
			for model in result:
				if not res.has_key("occupations"):
					res['occupations'] = []
				res["occupations"].append(dict(model))
		return res
	except Exception, e:
		log.error("Unable to get profile information: {0}".format(e.message))
		return None
		
		
		# used this section to test internal functions written before testing connection through rest
		# print getoccupations()
		# print deleteVillageRec(3)
		# print deleteOccupationRec(3)
