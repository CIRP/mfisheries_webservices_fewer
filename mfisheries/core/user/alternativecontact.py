from sqlalchemy import (
    Column,
    Integer,
    String,
    ForeignKey
)

from mfisheries.models import Base


class AlternativeContact(Base):
    """The SQLAlchemy delcarative model class for an AlternativeContact object"""

    __tablename__ = 'alternativecontact'
    id = Column(Integer, primary_key=True)
    userid = Column(Integer, ForeignKey('user.id'), nullable=False)
    villageid = Column(Integer, ForeignKey('village.id'), nullable=False)
    fname = Column(String(100))
    lname = Column(String(100))
    street = Column(String(100))
    cellnum = Column(String(15))
    homenum = Column(String(15))
    email = Column(String(100))

    def __init__(self, userid, villageid, fname, lname, street, cellnum, homenum, email):
        self.userid = userid
        self.villageid = villageid
        self.fname = fname
        self.lname = lname
        self.street = street
        self.cellnum = cellnum
        self.homenum = homenum
        self.email = email

    def toJSON(self):
        json = {}
        json['id'] = self.id
        json['userid'] = self.userid
        json['villageid'] = self.villageid
        json['fname'] = self.fname
        json['lname'] = self.lname
        json['street'] = self.street
        json['cellnum'] = self.cellnum
        json['homenum'] = self.homenum
        json['email'] = self.email

        return json
