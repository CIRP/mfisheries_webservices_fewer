import hashlib
import json
import logging

import transaction  # transaction manager
from cornice import Service  # allows the services to be exposed via a RESTFUL service
from sqlalchemy import asc

from mfisheries.core.user.users import User
from mfisheries.models import DBSession
from mfisheries.util.secure import validate

log = logging.getLogger(__name__)

# getusers = Service(name='getusers', path='/api/get/all/users', description='returns mfisheries users')
# addnewuser = Service(name='addnewuser', path='/api/add/new/user/', description='add new users')
# updateuser = Service(name='updateuser', path='/api/update/user/', description='updates user')

# Refactored API Service
userService = Service(name='userService', path='/api/users', description='users service')
userCountryService = Service(name='userCountryService', path='/api/users/country/{cid}',description='users country service')
specificUserService = Service(name='specificUserService', path='/api/users/{userid}', description='specific users service')


@specificUserService.delete()
def delete_user(request):
	if not validate(request):
		request.response.status = 401
		return {'status': 401, 'data': []}
	user_id = request.matchdict['userid']
	value = delete_user_helper(user_id)
	if value:
		return {'status': 200}
	else:
		request.response.status = 500
		return {'status': 500}


# @updateuser.post()
@userService.put()
@specificUserService.put()
def update_user(request):
	if not validate(request):
		request.response.status = 401
		return {'status': 401, 'data': []}
	userdata = json.loads(request.body)
	value = update_user_profile(userdata)
	log.debug(value)
	if value is not None:
		return {'status': 200, 'data': value.toJSON()}
	else:
		request.response.status = 500
		return {'status': 500}


# @getusers.get()
@userService.get()
def get_userinfo_by_userid(request):  # .get() indicates to the framework that this method is of type GET
	if not validate(request):
		request.response.status = 401
		return {'status': 401, 'data': []}
	if 'countryid' in request.params:
		result = get_users_by_country_id(request.params['countryid'])
	else:
		result = get_users()
	if not result:
		return {'status': 204, 'data': []}
	else:
		log.debug("Retrieved {0} users from the database ".format(len(result)))
		return {'status': 200, 'data': result}


# @addnewuser.post()
@userService.post()
def add_new_info(request):
	if not validate(request):
		request.response.status = 401
		return {'status': 401, 'data': []}
	
	user_info = json.loads(request.body)
	log.info("Adding a New User")
	log.info(str(user_info))
	value = add_user(user_info)
	log.debug(value)
	if value:
		return {'status': 201}
	else:
		return {'status': 501, "message": "error creating user "}


@userCountryService.get()
def get_users_by_country(request):
	if not validate(request):
		request.response.status = 401
		return {'status': 401, 'data': []}
	cid = request.matchdict['cid']
	res = DBSession.query(User).filter(User.countryid == cid).all()
	res = map(lambda m: m.toJSON(), res)
	return {'status': 200, 'data': res}


def add_user(data):  # the data model parameter is passed
	try:
		log.info("Adding a new user to the database")
		password = hashlib.sha256(data['password']).hexdigest()
		
		log.info("Creating a new User Model")
		user = User(
			data['username'],
			password,
			data['fname'],
			data['lname'],
			data['email'],
			country=int(data['countryid']),
			userclass=int(data['_class'])
		)
		
		if "homeNum" in data:
			user.homeNum = data['homeNum']
		if 'mobileNum' in data:
			user.mobileNum = data['mobileNum']
		
		DBSession.add(user)  # the database session adds the new data model to the database
		DBSession.flush()
		transaction.commit()
		log.info("User Added to the database")
		
		# TODO Can the id be retrieved without additional query
		nu = DBSession.query(User).filter(User.username == data['username'], User.password == password).one()
		log.info(str(nu.toJSON()))
		
		return nu
	except Exception, e:
		reason = e.message
		log.error("Unable to add User: " + reason)
		if "Duplicate entry" in reason:
			print "%s already in table." % e.params[0]
			DBSession.rollback()
		return None


def get_users_by_country_id(cid):
	res = DBSession.query(User).filter(User.countryid == cid).all()
	res = map(lambda m: m.toJSON(), res)
	return res

def get_users():
	res = DBSession.query(User).order_by(asc(User.username)).all()
	res = map(lambda m: m.toJSON(), res)
	return res


def get_user_by_id(u_id):
	res = DBSession.query(User).get(u_id)
	return res


def update_user_profile(data):
	try:
		# We attempt to find user based on data submitted
		user = DBSession.query(User).filter(User.id == data['id']).one_or_none()
		if user:  # if we find user then update
			# If we are attempting to update password, then hash data
			if 'passChanged' in data and data['passChanged'] == True:
				log.debug("Attempting to changed Password")
				password = hashlib.sha256(data['password']).hexdigest()
				user.password = password
			# else we ensure that data is passed and re-assign hashed password
			elif "password" in data:
				log.debug("Password not changed keeping the same")
				password = data['password']
				user.password = password
			
			# Re-assign attributes received from the client to the model
			user.username = data['username']
			user.fname = data['fname']
			user.lname = data['lname']
			user.email = data['email']
			user.countryid = data['countryid']
			user.homeNum = data['homeNum']
			user._class = data['_class']
			user.mobileNum = data['mobileNum']
			
			# We update the model
			DBSession.add(user)
			log.info("Successfully Updated the User")
		return user
	except Exception, e:
		print(e.message)
		log.error("Failed to Update user: " + e.message)
		return None


def delete_user_helper(userid):
	try:
		log.info("Deleting user with id: " + userid)
		DBSession.query(User).filter(User.id == userid).delete()
		log.info("Successfully deleted User")
		return True
	except Exception, e:
		log.error("Unable to deleted User: {0}".format(e))
		return None
