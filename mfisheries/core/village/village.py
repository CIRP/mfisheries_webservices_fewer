import transaction
import logging

from sqlalchemy import (
    Column,
    Integer,
    String,
    ForeignKey,
    UniqueConstraint
)
from sqlalchemy import event
from sqlalchemy import func
from sqlalchemy.orm import relationship

from mfisheries.core.country import Country
from mfisheries.models import Base
from mfisheries.models import DBSession

log = logging.getLogger(__name__)


class Village(Base):
    """SQLAlchemy declarative model class for a Village object"""
    __tablename__ = "village"
    id = Column(Integer, primary_key=True)
    name = Column(String(100), nullable=False)
    latitude = Column(String(25))
    longitude = Column(String(25))
    countryid = Column(Integer, ForeignKey('country.id'))
    country = relationship("Country", foreign_keys=[countryid])
    createdby = Column(Integer, ForeignKey('user.id'))

    # http://docs.sqlalchemy.org/en/latest/core/constraints.html#sqlalchemy.schema.UniqueConstraint
    __table_args__ = (UniqueConstraint('countryid', 'name', name='uixcvill_1'),)

    def __init__(self, name, latitude, longitude, countryid=1):
        self.name = name
        self.latitude = latitude
        self.longitude = longitude
        self.countryid = countryid

    def toJSON(self):
        return {
            'id': self.id,
            'name': self.name,
            'latitude': self.latitude,
            'longitude': self.longitude,
            "countryid": self.countryid,
            "country": self.country.name,
            "createdby": self.createdby,
        }
    
    def getRequiredFields(self):
        return ['name', 'countryid', 'latitude', 'longitude']


def create_default_villages(target, connection_rec, **kw):
    s = []
    try:
        country = DBSession.query(Country).filter(func.lower(Country.name) == "tobago").first()
        s.append(Village('Bon Accord', '11.1577716', '-60.8245611', country.id))
        s.append(Village('Canaan', '11.1568453', '-60.8123731999999', country.id))
        s.append(Village('Crown Point', '11.1536453', '-60.8340025', country.id))
        s.append(Village('Buccoo', '11.1821068', '-60.8051634', country.id))
        s.append(Village('Mt. Irvine', '11.1857274', '-60.7989835999999', country.id))
        s.append(Village('Black Rock', '11.202146', '-60.7811307999999', country.id))
        s.append(Village('Plymouth', '11.2171324', '-60.7651663', country.id))
        s.append(Village('Les Coteaux', '11.2296766', '-60.7414340999999', country.id))
        s.append(Village('Moriah', '11.232118', '-60.7245684', country.id))
        s.append(Village('Castara', '11.2800583', '-60.6923175', country.id))
        for v in s:
            DBSession.add(v)
        DBSession.flush()
    except Exception, e:
        log.error("Error inserting default villages: " + e.message)
        DBSession.rollback()
    transaction.commit()

event.listen(Country.__table__, 'after_create', create_default_villages)
