import csv
import json
import logging
import os
import shutil

import transaction
from cornice import Service
from fastkml import kml
from sqlalchemy import text

from mfisheries import APP_ROOT
from mfisheries.core import Country
from mfisheries.core import Village
from mfisheries.models import DBSession
from mfisheries.util.secure import validate

log = logging.getLogger(__name__)

addVillage = Service("addVillage", path="/api/add/village", description="Add a new village to the database")
getVillages = Service('getVillages', path="/api/get/villages", description="a list of all villages")
getVillagesCountry = Service('getVillagesCountry', path="/api/get/villages/country/{cid}",
                             description="a list of country villages")
updateVillage = Service("updateVillage", path="/api/update/village", description="updates a specific village")
deleteVillage = Service("deleteVillage", path="/api/delete/village/{id}", description="delete a specific village")
uploadVillages = Service("uploadVillages", path="/api/upload/villages", description="Upload Bulk of Villages")

# villageService = Service("villageService", path="/api/villages", description="Village Service")
# specificVillageService = Service("specificVillageService", path="/api/villages/{id}",
#                                  description="Specific Village Service")

village_path = APP_ROOT + "/res/villages/uploads/"


@addVillage.post()
# @villageService.post()
def add_village(request):
    if not validate(request):
        return {'status': 401, 'data': []}
    village_data = json.loads(request.body)
    res = addVillageRec(village_data)
    if res is not None:
        return {'status': 200, 'data': True}
    else:
        return {'status': 404, 'data': "Unable to complete request"}


@getVillages.get()
# @villageService.get()
def get_villages(request):
    if not validate(request):
        return {'status': 401, 'data': []}
    result = getvillages()
    if len(result) > 0:
        return {"status": 200, "data": result}
    else:
        return {"status": 404, "data": []}


@getVillagesCountry.get()
def get_villages_by_country(request):
    if not validate(request):
        return {'status': 401, 'data': []}
    cid = request.matchdict['cid']
    result = getvillages(cid)
    if len(result) > 0:
        return {"status": 200, "data": result}
    else:
        return {"status": 404, "data": []}


@updateVillage.post()
# @villageService.put()
def update_village(request):
    if not validate(request):
        return {'status': 401, 'data': []}
    village_details = json.loads(request.body)
    village_data = {}
    if village_details is not None:
        village_data = updatevillageRec(village_details)
    return {"status": 200, "data": village_data}


@deleteVillage.get()
# @specificVillageService.delete()
def delete_village(request):
    if not validate(request):
        return {'status': 401, 'data': []}
    vid = request.matchdict['id']
    res = deleteVillageRec(vid)
    if res:
        return {"status": 200, "data": "record successfully deleted"}
    else:
        return {"status": 500, "response": "unable to delete record"}


@uploadVillages.post()
def upload_villages(request):
    if not validate(request):
        return {'status': 401, 'data': []}
    try:
        log.info("Upload Villages Process Started")
        countryid = request.POST['countryid']
        filename = request.POST['upload'].filename
        upfile = request.POST['upload'].file
    except Exception, e:
        log.error("Unable to process upload images: " + e.message)
        return {'status': 400, "response": "Invalid request data received"}

    msg = "Error occurred while processing file " + filename
    # All parameters received
    extension = filename.split(".")
    country = DBSession.query(Country).get(countryid)

    if len(extension) > 1:
        log.info("Retrieved extension: " + extension[-1])
        if extension[-1] == "csv" or extension[-1] == "kml":
            log.info("Correct file type was uploaded")
            try:
                if country is not None:
                    filename = filename.replace(" ", "")
                    file_path = os.path.join(village_path, filename)
                    log.info("File path is: " + file_path)
                    with open(file_path, 'wb') as output_file:
                        shutil.copyfileobj(upfile, output_file)
                        log.info("File copied successfully")
                        output_file.close()

                    if extension[-1] == "csv":
                        log.info("CSV File was uploaded")
                        with open(file_path, 'r') as csvfile:
                            reader = csv.DictReader(csvfile, delimiter=',')
                            if 'latitude' in reader.fieldnames and 'longitude' in reader.fieldnames and 'village' in reader.fieldnames:
                                log.info("Correct Columns detected")
                                for row in reader:
                                    DBSession.add(
                                        Village(row['village'], row['latitude'], row['longitude'], country.id))
                            else:
                                msg = "Invalid Column names in CSV"
                                log.error(msg)
                                raise Exception(msg)
                    elif extension[-1] == "kml":
                        log.info("KML File was uploaded")
                        with open(file_path, 'r') as kmlfile:
                            log.debug("File uploaded and opened")
                            dataStr = kmlfile.read()
                            log.debug("Read the data from the kmlFile: " + dataStr)
                            k = kml.KML()
                            k.from_string(dataStr)
                            document = list(k.features())
                            placemarks = list(document[0].features())
                            log.info("Found {0} places".format(len(placemarks)))
                            if len(placemarks) > 0:
                                for placemark in placemarks:
                                    log.info("Location called {0} is located at ({1}, {2}) ".format(placemark.name,
                                                                                                    placemark.geometry.x,
                                                                                                    placemark.geometry.y))
                                    DBSession.add(
                                        Village(placemark.name, placemark.geometry.y, placemark.geometry.x, country.id))
                            else:
                                msg = "Insufficient village locations specified"
                                log.error("Raising error:" + msg)
                                raise Exception(msg)
                    else:
                        log.error("Raising error:" + msg)
                        raise Exception(msg)

                    DBSession.flush()
                    transaction.commit()
                    log.debug("Correctly uploaded information from the Village File")
                    # remove files after upload
                    os.remove(file_path)
                    os.remove(upfile)
                    log.debug("Removed file from after completed uploading information to database: " + file_path)
                    return {'status': 200, 'data': 'import completed successfully'}
                else:
                    # remove files after upload
                    # os.remove(file_path)
                    os.remove(upfile)
                    return {'status': 400, "response": "Invalid Country specified"}
            except Exception, e:
                log.error("Unable to upload Villages: " + e.message)
                DBSession.rollback()
                return {'status': 400, "response": "Unable to import data - " + msg}
        else:
            # File extension was something other than accepted type
            log.info("Unsupported file type that is uploaded")
            os.remove(upfile)
            return {'status': 400, "response": "Invalid file type uploaded."}
    else:
        # File uploaded did not have an extension therefore we cannot process the file
        log.info("Unsupported file type that is uploaded")
        os.remove(upfile)
        return {'status': 400, "response": "Invalid file type uploaded."}


def addVillageRec(data):
    v = Village(data['name'], data['latitude'], data['longitude'], data['countryid'])
    try:
        DBSession.add(v)
        DBSession.flush()
        transaction.commit()
        return v
    except Exception, e:
        print e.message
        return None


def getvillages(cid=None):
    try:
        if cid is None:
            stmt = text(
                "SELECT v.id, v.name, v.latitude, v.longitude, v.countryid, c.name as countryname from village v, country c where v.countryid = c.id")
        else:
            stmt = text(
                "SELECT v.id, v.name, v.latitude, v.longitude, v.countryid, c.name as countryname from village v, country c where v.countryid = c.id AND c.id = " + cid)
        result = DBSession.execute(stmt)
        arr = []
        for model in result:
            arr.append(dict(model))
        return arr
    except Exception, e:
        print e.message
        return []


def updatevillageRec(data):
    try:
        village = DBSession.query(Village).filter(Village.id == data['id']).one()
        village.countryid = data['countryid']
        village.name = data['name']
        village.latitude = data['latitude']
        village.longitude = data['longitude']
        print village
        return True
    except Exception, e:
        print e.message
    return None


def deleteVillageRec(vid):
    try:
        res = DBSession.query(Village).filter(Village.id == vid).delete()
        DBSession.flush()
        transaction.commit()
        return True
    except Exception, e:
        print e.message
        return None

from cornice.resource import resource
from mfisheries.core import BaseHandler


# http://cornice.readthedocs.io/en/latest/resources.html
@resource(collection_path='/api/villages',path='/api/villages/{id}',description="Country villages")
class VillageHandler(BaseHandler):
    def _get_order_field(self):
        return Village.name

    def __init__(self, request, context=None):
        BaseHandler.__init__(self, request,context)

    def _create_empty_model(self):
        return Village("", "", "")

    def _get_fields(self):
        return self._create_empty_model().getRequiredFields()

    def _get_target_class(self):
        return Village

    def _dict_to_model(self, data, rec_id=0):
        # If id is 0, we want to create a new entry
        if rec_id == 0:
            src = self._create_empty_model()
        else:
            src = DBSession.query(self._target_class).get(rec_id)
        src.name = data['name']
        src.countryid = data['countryid']
        src.latitude = data['latitude']
        src.longitude = data['longitude']
        src.countryid = data['countryid']
        if "createdby" in data:
            src.createdby = data['createdby']
        
        return src