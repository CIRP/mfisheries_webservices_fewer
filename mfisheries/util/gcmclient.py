import json

import requests
from pyfcm import FCMNotification

push_service = FCMNotification(
    api_key="AAAAy23oC3E:APA91bHG8Qm1q9t31IJeHowvP-HnfV39WuCRwBibj7tum-kh0oFvkBrUQPrtKH_FbAJ0soh-EbtqUewN654iu49g617PnfL50VyraDbvnGZdaTsR2lvKzLGaopQnNGvpKtvRz3kW-rd9M9w6Ub6tH_06XFQELU2TcA")

gcm = "key=AIzaSyAcnj3kIrypDTXN1wjIMT-vBHpKDpeDzCA"


def notifyTopic(topic, data):
    result = push_service.notify_topic_subscribers(topic_name=topic, data_message=data)
    print result
    return result


def sendMsg(message, subscribers):
    """New data Packet"""
    msg = {
        "data": {"message": message['msg']},
        "registration_ids": subscribers
    }

    """Old data packet"""
    # msg = {
    # 	"message":message['msg'],
    # 	"registration_ids": subscribers
    # }
    print subscribers
    print json.dumps(msg)

    res = requests.post('https://gcm-http.googleapis.com/gcm/send',
                        data=json.dumps(msg),  # this is the original line
                        headers={'Authorization': gcm, 'Content-Type': 'application/json'})

    if res.status_code == 200:
        print res.text
        resp = json.loads(res.text)
        if resp['success'] == 0:
            return False
        else:
            return True
    else:
        print res.status_code
        print res.text
        return False
