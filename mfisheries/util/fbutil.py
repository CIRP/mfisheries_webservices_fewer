from pyramid.events import subscriber
from mfisheries.events import NotifyError
from mfisheries.util import display_debug


@subscriber(NotifyError)
def executeOnError(event):
	display_debug("Received - {0} {1}".format(event.module_name, event.error_msg))