import ConfigParser
import logging
import smtplib

from mfisheries import APP_ROOT

log = logging.getLogger(__name__)

config = ConfigParser.ConfigParser()
# If script is executed via command line
if __name__ == '__main__':
    config.readfp(open("/../development.ini"))
# The script run within the pyramid app
else:
    curdir = APP_ROOT
    # print(curdir)
    config.readfp(open(curdir + "/../development.ini"))

gmail_user = config.get('app:main', "mfisheries.email_user")
gmail_pwd = config.get('app:main', "mfisheries.email_app_password")


def send_email(body, recipient=None, subject="SOS Alert"):
    if recipient is None:
        recipient = []
    # Ensure the mfisheries email is added to all sos messages
    recipient.append('mfisheries2011@gmail.com')

    fromEmail = gmail_user
    toEmails = recipient if type(recipient) is list else [recipient]
    messageBody = body

    # Prepare actual message
    message = """\From: %s\nTo: %s\nSubject: %s\n\n%s
    #""" % (fromEmail, ", ".join(toEmails), subject, messageBody)
    log.info("Attempting to send: " + message)

    try:
        server = smtplib.SMTP("smtp.gmail.com", 587)
        server.ehlo()
        server.starttls()
        server.ehlo()
        server.login(gmail_user, gmail_pwd)
        server.sendmail(fromEmail, toEmails, message)
        server.close()

        # print('successfully sent the mail')
        log.info("Email {0} was successfully sent to {1} recipients".format(subject, len(recipient)))
    except Exception, e:
        log.error("Unable to send email: " + str(e))
    # print("failed to send mail")
