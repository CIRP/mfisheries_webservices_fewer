from mfisheries.util import display_debug


class NotifyError(object):
	def __init__(self, module_name, error_msg):
		self.module_name = module_name
		self.error_msg = error_msg
		display_debug("NotifyError event successfully created", "info", module_name=__name__)
		display_debug("received: {0}:{1}".format(module_name, error_msg))
