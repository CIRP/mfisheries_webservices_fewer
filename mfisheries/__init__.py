import os
import threading

from apscheduler.schedulers.background import BackgroundScheduler
from pyramid.config import Configurator
from pyramid.settings import asbool
from sqlalchemy import engine_from_config

from mfisheries.util import display_debug
from mfisheries.models import initialize_sql, DBSession, Base, load_local_modules
from mfisheries.modules.weather.parsers import scheduler

APP_ROOT = os.path.dirname(os.path.realpath(__file__))
from scripts.broadcast import trigger_notification # Need to come after APP_ROOT


def load_system_modules(config):
	config.include('pyramid_chameleon')
	config.include('cornice')
	return config


def setup_background_scheduler():
	from mfisheries.modules.fewer.broadcastmanager import run_bcast_mgmtr_default
	from mfisheries.modules.weather.broadcastweather import run_weather_broadcast
	from mfisheries.core.config.config import Config
	background_scheduler = BackgroundScheduler()
	weather_notify = 60
	try:
		c = DBSession.query(Config).filter(Config.key == "alert_source_interval").one()
		alert_interval = int(c.value)
		# Background tasks for Broadcasting the Alerts
		broadcast_job = background_scheduler.add_job(run_bcast_mgmtr_default, 'interval', minutes=alert_interval)
		# The Threading task for Weather source retrieval
		broadcast_weather= background_scheduler.add_job(run_weather_broadcast, 'interval', minutes=weather_notify)
		threading.Timer(10, scheduler.run_weather_scheduled_task).start()
		background_scheduler.start()
		# Printing for debugging purposes
		display_debug(broadcast_job)
		display_debug(broadcast_weather)		
	except Exception, e:
		display_debug("Error occurred when starting scheduler: {0}".format(e), "error", module_name=__name__)


def setup_views(config):
	config.add_static_view('static', 'static', cache_max_age=3600)
	config.add_route('home', '/')

	# Load Service workers
	config.add_route('push.js', '/serviceWorker.min.js')
	config.add_route('push.fcm.js', '/firebase-messaging-sw.min.js')

	return config


def main(global_config, **settings):
	""" This function returns a Pyramid WSGI application."""
	print("Running the __init__ of the mFisheries project")
	engine = engine_from_config(settings, 'sqlalchemy.')
	
	DBSession.configure(bind=engine)
	Base.metadata.bind = engine
	
	config = Configurator(settings=settings)
	config = load_system_modules(config)
	config = load_local_modules(config)
	config = setup_views(config)
	
	run_scheduler = asbool(settings.get('mfisheries.run_scheduler', 'true'))
	if run_scheduler:
		setup_background_scheduler()
	
	return config.make_wsgi_app()
