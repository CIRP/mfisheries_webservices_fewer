import datetime
import logging

log = logging.getLogger(__name__)

from cornice.resource import resource  # allows the services to be exposed via a RESTFUL service
from mfisheries.models import DBSession

from mfisheries.modules.weather.weatherreading import WeatherReadings
from mfisheries.modules.weather.weathersource import WeatherSource
from mfisheries.modules.weather.weatherextractorhandler import delete_extractor_by_path


@resource(collection_path='/api/entry/weather', path='/api/entry/weather/{id}', description="Weather entry data")
class WeatherDetailsHandler(object):
    def __init__(self, request, context=None):
        self.request = request
        self.context = context

    def __save(self, data, ws_id=0):
        try:
            # If id is 0, we want to create a new entry
            if ws_id == 0:
                src = WeatherReadings("")  # Create an empty WS
            # If id is not 0, then we want to update existing record
            else:
                src = DBSession.query(WeatherReadings).filter(WeatherReadings.sourceid == data['sourceid']).first()

            source = DBSession.query(WeatherSource).filter(WeatherSource.id == data['sourceid']).first()
            source.lastexecuted = datetime.datetime.utcnow()
            src.timecreated = datetime.datetime.utcnow()
    
            # Add fields to the Weather Source object
            src.sourceid = data['sourceid']
            src.readings = str(data)

            # Save the operation and return the object
            DBSession.add(src)
            DBSession.add(source)
            DBSession.flush()
            return src
        except Exception, e:
            
            log.error("Unable to save Source: " + e.message)
            return False

    def collection_get(self):
        # retrieve all weather data
        res = DBSession.query(WeatherReadings).all()
        if len(res) > 0:
            res = map(lambda m: m.toJSON(), res)
        return res

    def get(self):
        # Extract id passed as a parameter within the URL
        ws_id = int(self.request.matchdict['id'])
        # Attempt to Retrieve record from database
        res = DBSession.query(WeatherReadings).filter(WeatherReadings.sourceid == ws_id).first()
        # If we successfully receive a record with the id specified
        if res:
            return res.toJSON()
        # Send a HTTP Status code to tell client id was not found
        self.request.response.status = 404
        return False

    def delete(self):
        # Extract id passed as a parameter within the URL
        ws_id = int(self.request.matchdict['id'])
        try:
            # Delete the weather
            DBSession.query(WeatherReadings).filter(WeatherReadings.sourceid == ws_id).delete()
            # Retrieve the record so we can delete the corresponding extractor file
            ws = DBSession.query(WeatherSource).filter(WeatherSource.id == ws_id).one()
            # Delete the file
            delete_extractor_by_path(ws.codepath)

            return True
        except Exception, e:
            print "ERROR: Unable to delete Weather readings"
            print e.message
            log.error(e.message)
            self.request.response.status = 400
            return False

    def put(self):
        # Extract data to be updated from the body of the put request
        data = self.request.json_body
        print " Not here"
        # Extract id passed as a parameter within the URL
        ws_id = int(self.request.matchdict['id'])
        if self.__isvalid(data):
            src = self.__save(data, ws_id)
            if src:
                return src.toJSON()

        try:
            source = DBSession.query(WeatherSource).filter(WeatherSource.id == data['sourceid']).first()
            source.status = "Fail"
            DBSession.add(source)
            DBSession.flush()
        except Exception, e:
            log.error(e.message)            

        self.request.response.status = 400
        return False

    def collection_post(self):
        # Extract data to be updated from the body of the post request
        data = self.request.json_body
        print "workis"
        # Ensure required fields are specified
        # if self.__isvalid(data):
        # Attempt to save the new source
        if DBSession.query(WeatherReadings).filter(WeatherReadings.sourceid == data['sourceid']).count():
            src = self.__save(data, data['sourceid'])
        else:
            src = self.__save(data)
        # If source was successfully saved
        if src:
            # Notify the client using the appropriate HTTP status code
            self.request.response.status = 201
            # Send the record
            return src.toJSON()
        try:
            source = DBSession.query(WeatherSource).filter(WeatherSource.id == data['sourceid']).first()
            source.status = "Fail"
            DBSession.add(source)
            DBSession.flush()
        except Exception, e:
            log.error(e.message)

        self.request.response.status = 400
        return False
