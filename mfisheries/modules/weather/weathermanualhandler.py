from cornice.resource import resource  # allows the services to be exposed via a RESTFUL service

from mfisheries.modules.weather.parsers import factory
from mfisheries.util import display_debug
from pyramid.config import Configurator


@resource(collection_path='/api/entry/manualrun', path='/api/entry/manualrun/{object}', description="Weather entry data")
class WeathermanualHandler(object):
    def __init__(self, request, context=None):
        self.request = request
        self.context = context

    def collection_post(self):
        # Extract data to be updated from the body of the post request
        data = self.request.json_body
        try:
            config = Configurator()
            config.scan('mfisheries.modules.weather.parsers.extractors')
            # TODO Should probably not do this, but use record id instead
            filename = data['codepath'].split('/').pop().split(".py")[0]
            my_class = factory.extractor_factory(filename)

            if my_class is None:
                raise Exception("Invalid factory specified: " + str(filename))

            if my_class.save(data['id']):
                return True
            return False
        except Exception, e:
            display_debug(e, "error")
            self.request.response.status = 400
        return False

