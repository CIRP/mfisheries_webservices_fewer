import datetime
import logging

from sqlalchemy import (
    Column,
    Integer,
    String,
    TIMESTAMP,
    ForeignKey
)
from sqlalchemy.orm import relationship

from mfisheries.models import Base

log = logging.getLogger(__name__)


class WeatherSource(Base):
    __tablename__ = "weathersources"
    id = Column(Integer, primary_key=True)
    countryid = Column(Integer, ForeignKey('country.id'))
    name = Column(String(30), nullable=False)
    infotype = Column(String(30))
    sourcetype = Column(String(30), default="Website")
    url = Column(String(100), nullable=False)
    codepath = Column(String(1000))
    frequency = Column(String(100), nullable=False, default="Every Hour")
    duration = Column(String(25), nullable=False, default="Single Day")
    lastexecuted = Column(TIMESTAMP)
    created = Column(TIMESTAMP, default=datetime.datetime.utcnow)
    createdby = Column(Integer, ForeignKey('user.id'))

    country = relationship("Country", foreign_keys=[countryid], lazy='subquery')

    def __init__(self, cid, name, cat, url, code, frequency):
        self.countryid = cid
        self.name = name
        self.infotype = cat
        self.url = url
        self.codepath = code
        self.frequency = frequency

    def toJSON(self):
        return {
            'id': self.id,
            'countryid': self.countryid,
            'country': self.country.name,
            'name': self.name,
            'infotype': self.infotype,
            'sourcetype': self.sourcetype,
            'url': self.url,
            'codepath': self.codepath,
            'frequency': self.frequency,
            'duration': self.duration,
            'lastexecuted': str(self.lastexecuted),
            'created': str(self.created),
            'createdby': self.createdby
        }
