try:
	import json
	import re
	
	import requests
	from bs4 import BeautifulSoup
	
	from mfisheries.modules.weather.parsers.WeatherSourceExtractor import WeatherSourceExtractor
except ImportError, e:
	raise ImportError(e)


def findword(word, array):
	for data in array:
		if word in data.lower():
			return data


def findwordList(word, array):
	save = ""
	for data in array:
		if word in data.lower():
			save = data
	return save


class Extractor(WeatherSourceExtractor):
	def __init__(self):
		WeatherSourceExtractor.__init__(
			self,
			"http://slumet.gov.lc/category/uncategorized/"
		
		)
	
	def get_poster_url(self):
		return self.post_url
	
	def get_extractor_url(self):
		return self.extractor_url
	
	def get_reading_types(self):
		return {
			"temperature": {
				"type": "numerical",
				"unit": "C"
			}
		}
	
	def extract(self):
		items = []
		forecast = []
		self.readings = {}
		
		r = requests.get(self.extractor_url)
		soup = BeautifulSoup(r.content, "html.parser")
		data = soup.find_all("div", {"class": "content-container"})
		for row in data:
			for list in row.find_all('p'):
				items.append(list.text)
		
		temperature = findword('temperature', items).split("C OR")[0]
		self.readings['temperature'] = re.sub("[^0-9]", "", temperature)
		self.readings['wind'] = findwordList('wind', items).lower().split("\n")
		for i in range(len(self.readings["wind"])):
			if "wind" in self.readings['wind'][i]:
				self.readings['wind'] = self.readings['wind'][i].capitalize()
		self.readings['waves'] = findword("seas:", items).split("SEAS: ")[1].lower().capitalize()
		
		sun = findword('sunset', items).split(" ")
		count = 0
		for i in sun:
			if "AM" in i:
				self.readings['sunrise'] = sun[count - 1].strip() + " AM"
			elif "PM" in i:
				self.readings['sunset'] = sun[count - 1].strip() + " PM"
			count = count + 1
		print "Hi"
		return self.readings
	
	def toJSON(self):
		return json.dumps(self.readings)


# Can be executed directly using the command python -m mfisheries.modules.weather.parsers.GrenadaCurrentExtractor
if __name__ == "__main__":
	country = Extractor()
	print country.extract()
	print country.toJSON()
	# print country.get_reading_types()
	# print country.save(3)
