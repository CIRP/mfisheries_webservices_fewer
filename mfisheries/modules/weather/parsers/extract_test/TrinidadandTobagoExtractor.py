try:
	import json
	import re
	
	import requests
	from bs4 import BeautifulSoup
	
	from mfisheries.modules.weather.parsers.WeatherSourceExtractor import WeatherSourceExtractor
except ImportError, e:
	raise ImportError(e)


def wordsize(stuff, word):
	string = ""
	pos = stuff.find(word) + len(word)
	while not (stuff[pos].isupper() and stuff[pos - 1].islower()):
		string += stuff[pos]
		pos += 1
	return string


def findword(word, array):
	for data in array:
		if word in data.lower():
			return data


class Extractor(WeatherSourceExtractor):
	def __init__(self):
		WeatherSourceExtractor.__init__(
			self,
			"http://metservice.gov.tt/forecast_api2.php"
		)
	
	def get_poster_url(self):
		return self.post_url
	
	def get_extractor_url(self):
		return self.extractor_url
	
	def get_reading_types(self):
		return {
			"temperature": {
				"type": "numerical",
				"unit": "C"
			}
			
		}
	
	def extract(self):
		headings = []
		details = []
		self.readings = {}
		count = 0
		print("Extracting current info from Trinidad and Tobago MET")
		r = requests.get(self.extractor_url)
		print("Retrieved home page")
		soup = BeautifulSoup(r.content, "html.parser")
		stuff = filter(None, soup.text.split("\n"))
		data = " ".join(stuff)
		self.readings['waves'] = wordsize(data, "Waves: ")
		self.readings['seas'] = wordsize(data, "Seas: ")
		sun = findword('sunset', stuff).split(" ")
		count = 0
		for i in sun:
			if "AM" in i or "am" in i:
				self.readings['sunrise'] = sun[count - 1].strip() + " am"
			elif "PM" in i or "pm" in i:
				self.readings['sunset'] = sun[count - 1].strip() + " pm"
			count = count + 1
		
		return self.readings
	
	def toJSON(self):
		return json.dumps(self.readings)


# Can be executed directly using the command python -m mfisheries.modules.weather.parsers.GrenadaCurrentExtractor

if __name__ == "__main__":
	country = Extractor()
	country.get_extractor_url()
	country.get_poster_url()
	print country.extract()
# print country.save(1)
# country.toDatabase()
