try:
	import json
	import re
	
	import requests
	from bs4 import BeautifulSoup
	
	from mfisheries.modules.weather.parsers.WeatherSourceExtractor import WeatherSourceExtractor
except ImportError, e:
	raise ImportError(e)


def findword(word, array):
	items = []
	for data in array:
		# print data
		if word in data.lower():
			items.append(data)
	
	return items


# St.VincentCurrentForecastExtractor
class Extractor(WeatherSourceExtractor):
	def __init__(self):
		WeatherSourceExtractor.__init__(
			self,
			
			"http://www.barbadosweather.org/Overseers/Wx_Data/getWxForecastData.php?country=Saint%20Vincent"
		)
	
	def get_poster_url(self):
		return self.post_url
	
	def get_extractor_url(self):
		return self.extractor_url
	
	def get_reading_types(self):
		return {
			"wind": {
				"type": "text",
				"unit": "km/h"
			},
			"sea": {
				"type": "text",
				"unit": "m"
			}
		}
	
	def extract(self):
		weather = []
		self.readings = {}
		# print("Extracting current info from Barbados MET")
		items = []
		r = requests.get(self.extractor_url)
		soup = BeautifulSoup(r.content, "html.parser")
		forecast = []
		for row in soup.find_all("tr"):
			for i in row.find_all('td'):
				items.append(i.text)
		
		self.readings['synopsis'] = findword("synopsis", items)[0].split(": ")[1]
		self.readings['wind'] = findword("wind", items)[0].split(": ")[1]
		self.readings['seas'] = findword("sea", items)[0].split(": ")[1]
		
		for row in soup.find_all("div", {"style": "text-align:left;"}):
			weather.append(row.text.strip().split(": ")[1])
		
		self.readings['weather'] = weather[0]
		
		return self.readings
	
	def toJSON(self):
		return json.dumps(self.readings)


if __name__ == "__main__":
	country = Extractor()
	print country.extract()
	print country.toJSON()
	# print country.get_reading_types()
	# print country.save(4)
