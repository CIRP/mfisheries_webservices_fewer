import json

import requests
from bs4 import BeautifulSoup

from mfisheries.modules.weather.parsers.WeatherSourceExtractor import WeatherSourceExtractor


class Extractor(WeatherSourceExtractor):
	def __init__(self):
		WeatherSourceExtractor.__init__(
			self,
			"http://weather.mbiagrenada.com/?q=forecast"
		)
	
	def get_poster_url(self):
		return self.post_url
	
	def get_extractor_url(self):
		return self.extractor_url
	
	def get_reading_types(self):
		return {
			"temperature": {
				"type": "numerical",
				"unit": "C"
			},  # TODO Need a cross platform way to represent degree symbol
			"wind": {
				"type": "text",
				"unit": "km/h"
			}
			
		}
	
	def extract(self):
		dates = []
		winds = []
		waves = []
		sun = []
		tides = []
		maxmintemp = []
		condition = []
		
		print("Extracting current info from grenada MET")
		r = requests.get(self.extractor_url)
		print("Retrieved home page")
		soup = BeautifulSoup(r.content, "html.parser")
		data = soup.find_all("tr")
		
		# Date
		for i in data[0].text.split("\n"):
			if len(i) > 0:
				dates.append(i)
		
		# Sunset sunrise
		for j in data[2].text.split("\n"):
			if len(j) > 0:
				sun.append(j.split(":", 1)[-1])
		
		# Temperature
		for k in data[4].text.split("\n"):
			if len(k) > 0:
				maxmintemp.append(k.split(":", 1)[-1])
		
		# Waves
		for l in data[6].text.split("\n"):
			if len(l) > 0:
				waves.append(l.split(":", 1)[-1].strip())
		
		for n in data[5].text.split("\n"):
			if len(n) > 0:
				winds.append(n.split(":", 1)[-1])
		
		for m in data[8].text.split("\n"):
			if len(m) > 0:
				tides.append(m.split("Tides:", 1)[-1])
		
		self.readings = {"dates": dates,
		                 "wind": winds,
		                 "waves": waves,
		                 "sun": sun,
		
		                 "tides": tides,
		                 "maxmintemp": maxmintemp
		                 }
	
	def toJSON(self):
		return json.dumps(self.readings)


if __name__ == "__main__":
	country = Extractor()
	print country.extract()
	print country.toJSON()
	# print country.get_reading_types()
	print country.save(2)
