# -*- coding: utf-8 -*-
# This file is a template of how to create an extractor for a country
try:
	import json
	import re
	
	import requests
	from bs4 import BeautifulSoup
	
	from mfisheries.modules.weather.parsers.WeatherSourceExtractor import WeatherSourceExtractor
except ImportError, e:
	raise ImportError(e)


# Class must be named Extractor in order to have functionality that factory.py provides
class Extractor(WeatherSourceExtractor):
	def __init__(self):
		WeatherSourceExtractor.__init__(
			self,
			""  # URL for website
		)
	
	def get_poster_url(self):
		return self.post_url
	
	def get_extractor_url(self):
		return self.extractor_url
	
	# User enters what values can have threshold in format given
	def get_reading_types(self):
		return {
			"temperature": {
				"type": "numerical",
				"unit": "C"
			},
			"windspeed": {
				"type": "numerical",
				"unit": "km/h"
			},
			"pressure": {
				"type": "numerical",
				"unit": "mb"
			},
			"humidity": {
				"type": "numerical",
				"unit": "%"
			}
		}
	
	# Programmer can implement extract suitable to website scraping
	# But it must return a dictionary with readings e.g self.readings['temperature'] =10
	# return self.readings
	def extract(self):
		self.readings = {}
		r = requests.get(get_extractor_url)
		# it is advised to use BeautifulSoup package for scraping
		soup = BeautifulSoup(r.content, "html.parser")
		# Textual data cannot have threshold current therefore do not set threshold for them
		temperature = 10
		humidity = 10
		pressure = 10
		winds = 10
		self.readings = {'temperature': temperature,
		                 'humidity': humidity,
		                 'pressure': pressure,
		                 'wind': winds}
		
		return self.readings
	
	def toJSON(self):
		return json.dumps(self.readings)


# To test the single extractor with the WeatherSource class use python -m mfisheries.modules.weather.parsers.extract_test."filename"
if __name__ == "__main__":
	country = Extractor()
	print country.extract()
	print country.toJSON()
