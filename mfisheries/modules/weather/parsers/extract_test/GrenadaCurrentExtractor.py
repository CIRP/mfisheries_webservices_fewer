try:
	import json
	import re
	
	import requests
	from bs4 import BeautifulSoup
	
	from mfisheries.modules.weather.parsers.WeatherSourceExtractor import WeatherSourceExtractor
except ImportError, e:
	raise ImportError(e)


class Extractor(WeatherSourceExtractor):
	def __init__(self):
		WeatherSourceExtractor.__init__(
			self,
			"http://weather.mbiagrenada.com/",
		
		)
	
	def get_poster_url(self):
		return self.post_url
	
	def get_extractor_url(self):
		return self.extractor_url
	
	def get_reading_types(self):
		return {
			"temperature": {
				"type": "numerical",
				"unit": "F"
			},  # TODO Need a cross platform way to represent degree symbol
			"wind": {
				"type": "numerical",
				"unit": "km/h"
			},
			"pressure": {
				"type": "numerical",
				"unit": "hPa"
			},
			"rel. humidity": {
				"type": "numerical",
				"unit": "%"
			},
			"visibility": {
				"type": "numerical",
				"unit": "km"
			}
			
		}
	
	def extract(self):
		headings = []
		self.readings = {}
		details = []
		count = 0
		print("Extracting current info from grenada MET")
		r = requests.get(self.extractor_url)
		print("Retrieved home page")
		soup = BeautifulSoup(r.content, "html.parser")
		data = soup.find_all("div", {"class": "weather"})
		for row in data:
			for list_item in row.find_all('li'):
				rec = list_item.text.split(": ")
				if len(rec) > 1:
					self.readings[rec[0].lower()] = rec[1]
		
		soup = BeautifulSoup(r.content.decode('utf-8', 'ignore'), "html.parser")
		for row in soup.find_all("tr"):
			headings.append(filter(None, row.text.split('\n'))[0])
			details.append(filter(None, row.text.split('\n'))[1])
		
		for key in headings:
			if "warning" in key.lower():
				print key.lower().replace(":", "").replace(" ", "")
				self.readings[key.lower().replace(":", "")] = details[count]
			count += 1
		
		return self.readings
	
	def toJSON(self):
		return json.dumps(self.readings)
	
	# Can be executed directly using the command python -m mfisheries.modules.weather.parsers.GrenadaCurrentExtractor
	# if __name__ == "__main__":
	# 	country = Extractor()
	# 	country.get_extractor_url()
	# 	country.get_poster_url()
	# 	print country.extract()
	# print country.save(1)
	# country.toDatabase()
