try:
	import json
	import requests
	from mfisheries.modules.weather.parsers.WeatherSourceExtractor import WeatherSourceExtractor
except ImportError, e:
	raise ImportError(e)


class Extractor(WeatherSourceExtractor):
	def __init__(self):
		latitude = 15.4150
		longitude = 61.3710
		tide_api = "https://www.worldtides.info/api?extremes&lat={0}&lon={1}&key={2}".format(latitude, longitude, self.api_key)
		WeatherSourceExtractor.__init__(self, tide_api)
	
	def get_poster_url(self):
		return self.post_url
	
	def get_extractor_url(self):
		return self.extractor_url
	
	def get_reading_types(self):
		return {
			
		}
	
	def extract(self):
		self.readings = {}
		items = []
		forecast = []
		r = requests.get(self.extractor_url)
		self.readings['readings'] = r.json()['extremes']
		return self.readings
	
	def toJSON(self):
		return json.dumps(self.readings)


# Can be executed directly using the command python -m mfisheries.modules.weather.parsers.GrenadaCurrentExtractor
if __name__ == "__main__":
	country = Extractor()
	print country.extract()
	print country.toJSON()
# print country.get_reading_types()
# print country.save(3)
