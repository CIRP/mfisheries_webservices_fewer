import json

import requests
from bs4 import BeautifulSoup

from mfisheries.modules.weather.parsers.WeatherSourceExtractor import WeatherSourceExtractor


class Extractor(WeatherSourceExtractor):
	def __init__(self):
		WeatherSourceExtractor.__init__(
			self,
			"http://www.weather.gov.dm/forecast/extended-forecast"
		)
	
	def get_poster_url(self):
		return self.post_url
	
	def get_extractor_url(self):
		return self.extractor_url
	
	def get_reading_types(self):
		return {
			"temperature": {
				"type": "numerical",
				"unit": "C"
			},  # TODO Need a cross platform way to represent degree symbol
			"wind": {
				"type": "text",
				"unit": "km/h"
			},
			"waves": {
				"type": "text",
				"unit": "ft"
			}
			
		}
	
	def extract(self):
		dates = []
		maxtemp = []
		mintemp = []
		wind = []
		seas = []
		waves = []
		condition = []
		r = requests.get(self.extractor_url)
		soup = BeautifulSoup(r.content, "html.parser")
		extended = soup.find_all("div", {"id": "ext_forecast"})
		for i in extended:
			for date in i.find_all('h3'):
				dates.append(date.text.replace("\n", " "))
		
		data = soup.find_all("div", {"class": "entry"})
		for entry in data:
			maxtemp.append(entry.text.split("\n")[1].split(": ")[1].split("/")[0])
			mintemp.append(entry.text.split("\n")[2].split(": ")[1].split("/")[0])
			wind.append(entry.text.split("\n")[5].split(": ")[1])
			seas.append(entry.text.split("\n")[6].split(": ")[1])
			waves.append(entry.text.split("\n")[7].split(": ")[1])
			condition.append(entry.text.split("\n")[4].split(": ")[1])
		
		self.readings = {"dates": dates,
		                 "wind": wind,
		                 "waves": waves,
		                 "sea": seas,
		                 "condtions": condition,
		                 "mintemp": mintemp,
		                 "maxtemp": maxtemp}
		
		return self.readings
	
	def toJSON(self):
		return json.dumps(self.readings)


if __name__ == "__main__":
	country = Extractor()
	country.get_extractor_url()
	country.get_poster_url()
	print country.extract()
	print country.toJSON()
