try:
	import json
	import requests
	from mfisheries.modules.weather.parsers.WeatherSourceExtractor import WeatherSourceExtractor
except ImportError, e:
	raise ImportError(e)


class Extractor(WeatherSourceExtractor):
	def __init__(self):
		WeatherSourceExtractor.__init__(
			self,
			# For each new country change lat and long in url. Everything else remains the same
			"https://www.worldtides.info/api?extremes&lat=12.9843&lon=61.2872&key=" + self.api_key
		)
	
	def get_poster_url(self):
		return self.post_url
	
	def get_extractor_url(self):
		return self.extractor_url
	
	def get_reading_types(self):
		return {
			
		}
	
	def extract(self):
		self.readings = {}
		items = []
		forecast = []
		r = requests.get(self.extractor_url)
		self.readings['readings'] = r.json()['extremes']
		return self.readings
	
	def toJSON(self):
		return json.dumps(self.readings)


# Can be executed directly using the command python -m mfisheries.modules.weather.parsers.TemplateTides
if __name__ == "__main__":
	country = Extractor()
	print country.extract()
	print country.toJSON()
