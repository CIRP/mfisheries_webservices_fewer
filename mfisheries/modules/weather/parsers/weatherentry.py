import datetime

from sqlalchemy import (
    Column,
    Integer,
    String,
    TIMESTAMP
)

from mfisheries.models import Base


class WeatherEntry(Base):
    __tablename__ = "weatherentry"
    id = Column(Integer, primary_key=True)
    temperature =Column(String(30))
    country = Column(String(30))
    pressure = Column(String(30))
    humidity = Column(String(100))
    winds = Column(String(1000))
    waves = Column(String(100))
    created = Column(TIMESTAMP, default=datetime.datetime.utcnow)

    def __init__(self, country, temperature,humidity,pressure, waves,winds):
        self.country = country
        self.pressure = pressure
        self.temperature = temperature
        self.humidity = humidity
        self.waves = waves
        self.winds = winds

    def toJSON(self):
        return {
            'id':self.id,
            'country': self.country,
            'temperature': self.temperature,
            'pressure':self.pressure,
            'humidity': self.humidity,
            'waves': self.waves,
            'winds': self.winds
            }

