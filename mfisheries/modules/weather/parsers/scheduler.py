from apscheduler.schedulers.background import BackgroundScheduler

from mfisheries.models import DBSession
from mfisheries.modules.weather import WeatherSource
from mfisheries.modules.weather.parsers import factory
from mfisheries.util import display_debug

FREQUENCY = {
	"Every 6 Hours": 6,
	"Every Hour": 1,
	"Once a day": 24
}


def run_weather_scheduled_task():
	sources = DBSession.query(WeatherSource).all()
	sched = BackgroundScheduler()
	for entry in sources:
		try:
			filename = entry.codepath.split('/').pop().split(".py")[0]
			loop = FREQUENCY[entry.frequency]
			src_id = entry.id
			display_debug("{0}-{1}-{2}".format(filename, loop, src_id))
			my_class = factory.extractor_factory(filename)
			display_debug(my_class.extract())
			job = sched.add_job(my_class.save, 'interval', hours=loop, kwargs={'sourceid': src_id})
			display_debug(job, "info")
		except Exception, e:
			display_debug(e, "error")

	sched.start()
	display_debug("Extractors Scheduled Successfully", "info")