import os
from mfisheries.util import display_debug
from .emailsender import send_email
from mfisheries.modules.weather.parsers.extractors import *


def get_all_package_names(path="extractors"):
	#  this is a list of Extractors available
	packages = []
	base_path = os.path.dirname(os.path.realpath(__file__))
	extractor_path = "{0}/{1}/".format(base_path, path)
	
	for i in os.listdir(extractor_path):
		if len(i.split(".")) == 2:
			if i.split(".")[1] == "py":
				packages.append(i.split(".")[0])
	return packages


# Gets a list of extractors in folder and runs the one which is called from the scheduler
def extractor_factory(extractor_type, path="extractors"):
	packages = get_all_package_names(path)
	country = str(extractor_type).split("_")[0]
	try:
		display_debug("Attempting to process: " + str(extractor_type), type="info", module_name=__name__)
		if extractor_type in packages:
			display_debug("Attempting to run: " + str(extractor_type + "." + "Extractor()"), type="info", module_name=__name__)
			call = eval(extractor_type + "." + "Extractor()")
			call.extract()
			return call
	except Exception, e:
		display_debug(e, type="error", module_name=__name__)
		send_email(str(e), country, str(extractor_type))
	return None
