import datetime
import json
import logging
import re
from abc import ABCMeta, abstractmethod

from mfisheries.models import DBSession
from mfisheries.modules.weather.threshold.threshold import Threshold
from mfisheries.modules.weather.threshold.weatheralert import WeatherAlert
from mfisheries.modules.weather.weatherreading import WeatherReadings
from mfisheries.modules.weather.weathersource import WeatherSource

log = logging.getLogger(__name__)


# https://www.toptal.com/python/python-class-attributes-an-overly-thorough-guide
class WeatherSourceExtractor(object):
	__metaclass__ = ABCMeta
	# Not needed anymore but have to remove method from all extractors
	post_url = ' '
	extractor_url = ""
	readings = {}
	# TODO change api_key from https://www.worldtides.info with account made for mfisheries
	# To manage api calls better for tides api
	api_key = "1139b6d8-e234-42d0-b93b-a61b647357fc"
	
	def __init__(self, extract=""):
		self.extractor_url = extract
	
	@abstractmethod
	def get_extractor_url(self):
		pass
	
	@abstractmethod
	def toJSON(self):
		pass
	
	@abstractmethod
	def get_reading_types(self):
		pass
	
	@abstractmethod
	def extract(self):
		return {}
	
	def save(self, sourceid=0):
		data = self.extract()
		# If extension developer did not assign extracted values to class property
		if data and len(self.readings.keys()) < 1:
			self.readings = data

		self.readings.update({"sourceid": sourceid}) # Update method of dict - https://www.tutorialspoint.com/python/dictionary_update.htm

		# If extension developer did not return readings
		if not data and len(self.readings.keys()) > 1:
			data = self.readings

		try:
			log.debug(self.readings)
			alert_res = self.weather_alert(sourceid) # Adds data to table to determine if notification to be sent
			log.debug(alert_res)
			log.debug(data)
			status = self.__save(data, sourceid)
			log.debug(status)
			return status
		except Exception, e:
			log.error("Unable to save weather alert: {0}".format(e))
			return False

	def __save(self, data, w_src_id=0):
		try:
			src = DBSession.query(WeatherReadings).filter(WeatherReadings.sourceid == data['sourceid']).first()
			if src is None:
				log.debug("No sourceid specified. Creating a new Weather Reading")
				src = WeatherReadings("")  # Create an empty WS
			
			# Find the source corresponding to the record received
			qry_src = DBSession.query(WeatherSource)
			qry_src = qry_src.filter(WeatherSource.id == data['sourceid'])
			source = qry_src.first()
			
			if source:
				source.lastexecuted = datetime.datetime.utcnow()
				DBSession.add(source)
			else:
				raise Exception("Invalid source id supplied. No record found for: " + str(data['sourceid']))
			if src:
				src.timecreated = datetime.datetime.utcnow()
				# Add fields to the Weather Source object
				src.sourceid = data['sourceid']
				src.readings = str(data)
				DBSession.add(src)
			else:
				raise Exception("No Weather readings for source: " + str(data['sourceid']))

			DBSession.flush()
			return src
		except Exception, e:
			DBSession.rollback()
			log.error("Unable to save Source: " + e.message)
			return False

	def clean_text(self, data):
		return data.replace('\n', ' ').replace('\r', ' ').replace("\u00a0", " ")

	def findword(self, word,array):
		"""
		Accepts array of paragraphs and returns which paragraph text is found in
		"""
		items = []
		for data in array:
			if word in data.lower():
				items.append(data)
		return items

	def findwordList(word, array):
		save = ""
		for data in array:
			if word in data.lower():
				save = data
		return save


	def comparison(self, newalert, oldalert):
		return set(newalert) == set(json.loads(oldalert))


	def weather_alert(self, sourceid):
		# saving for the weather alerts
		query = DBSession.query(Threshold)
		res = query.filter(Threshold.weathersourceid == sourceid).first()
		if res:
			log.debug("Found record for sourceid: " + str(sourceid))
			thresholds = res.getThresholds()
			if thresholds or len(thresholds.keys()) < 1:
				log.debug("Threshold has values for emergency and warning")
				emergency = thresholds['emergency']
				warning = thresholds['warnings']

				# If alert for source id exist
				count_alert = DBSession.query(WeatherAlert).filter(WeatherAlert.weathersourceid == sourceid).count()
				if count_alert:
					alert = DBSession.query(WeatherAlert).filter(WeatherAlert.weathersourceid == sourceid).first()
					log.debug("Alert for sourceid {0} previous existed with an id {1}".format(sourceid, alert.id))
				else:
					alert = WeatherAlert(sourceid, [])
					log.debug("Alter created for sourceid {0} with a record if of {1}".format(sourceid, alert.id))

				values = []
				try:
					# Finding all values triggered
					for key in emergency:
						if emergency[key] < int(re.sub("[^0-9]", "", self.readings[key])):
							if key not in values:
								values.append(key)

					for key in warning:
						if warning[key] < int(re.sub("[^0-9]", "", self.readings[key])):
							if key not in values:
								values.append(key)

					equals = self.comparison(values, alert.readings)
					if len(values) > 0 and (alert.created.date() != datetime.date.today() or not equals):
						alert.readings = json.dumps(values)
						alert.sent = 0
						alert.created = datetime.datetime.utcnow()
						DBSession.add(alert)
						DBSession.flush()

					return alert
				except Exception, e:
					log.error("Error occurred while attempting to determine if emergency or warning exists: " + str(e))
			else:
				log.debug("Record exist, but invalid data for threshold with sourceid: " + str(sourceid))
		else:
			log.debug("Unable to find record for source with id: " + str(sourceid))

		return False