import logging

from cornice.resource import resource

from mfisheries.models import DBSession
from weathersource import WeatherSource

log = logging.getLogger(__name__)


# http://cornice.readthedocs.io/en/latest/resources.html
@resource(collection_path='/api/weathersources', path='/api/weathersources/{id}', description='Weather Sources')
class WeatherSourceHandler(object):
    def __init__(self, request, context=None):
        self.context = context
        self.request = request
        self.fields = ['countryid', 'name', 'infotype', 'sourcetype', 'url', 'codepath', 'frequency', 'duration']

    #  Private Helper Methods
    def __isvalid(self, values):
        for f in self.fields:
            if f not in values:
                return False
        return True

    def __save(self, data, ws_id=0):
        try:
            # If id is 0, we want to create a new entry
            if ws_id == 0:
                src = WeatherSource("", "", "", "", "", "")  # Create an empty WS
            # If id is not 0, then we want to update existing record
            else:
                src = DBSession.query(WeatherSource).get(ws_id)

            # Add fields to the Weather Source object
            src.countryid = data['countryid']
            src.name = data['name']
            src.infotype = data['infotype']
            src.sourcetype = data['sourcetype']
            src.url = data['url']
            src.codepath = data['codepath']
            src.frequency = data['frequency']
            src.duration = data['duration']

            # if 'lastexecuted' in data:
            #     src.lastexecuted = data['lastexecuted']
            if 'createdby' in data:
                src.createdby = data['createdby']

            # Save the operation and return the object
            DBSession.add(src)
            DBSession.flush()
            return src
        except Exception, e:
            log.error("Unable to save Source: " + e.message)
            return False

    # Methods exposing API calls
    def collection_get(self):
        # retrieve all weather sources from table
        query = DBSession.query(WeatherSource)
        if len(self.request.GET) > 0 and "countryid" in self.request.GET:
            log.debug("Received additional data in get country request: " + self.request.GET['countryid'])
            cid = self.request.GET['countryid']
            query = query.filter(WeatherSource.countryid == cid)

        res = query.all()

        if len(res) > 0:  # convert each record received to JSON format
            res = map(lambda m: m.toJSON(), res)

        return res  # send data as json

    def get(self):
        # Extract id passed as a parameter within the URL
        ws_id = int(self.request.matchdict['id'])
        # Attempt to Retrieve record from database
        res = DBSession.query(WeatherSource).get(ws_id)
        # If we successfully receive a record with the id specified
        if res:
            return res.toJSON()
        # Send a HTTP Status code to tell client id was not found
        self.request.response.status = 404
        return False

    def delete(self):
        # Extract id passed as a parameter within the URL
        ws_id = int(self.request.matchdict['id'])
        try:
            # Delete the record
            DBSession.query(WeatherSource).filter(WeatherSource.id == ws_id).delete()
            return True
        except Exception, e:
            log.error(e.message)
            self.request.response.status = 400
            return False

    def put(self):
        # Extract data to be updated from the body of the put request
        data = self.request.json_body
        # User should not be able to update this 
        del data["lastexecuted"]
        # Extract id passed as a parameter within the URL
        ws_id = int(self.request.matchdict['id'])
        if self.__isvalid(data):
            src = self.__save(data, ws_id)
            if src:
                return src.toJSON()

        self.request.response.status = 400
        return False

    def collection_post(self):
        # Extract data to be updated from the body of the post request
        data = self.request.json_body
        # Ensure required fields are specified
        if self.__isvalid(data):
            # Attempt to save the new source
            src = self.__save(data)
            # If source was successfully saved
            if src:
                # Notify the client using the appropriate HTTP status code
                self.request.response.status = 201
                # Send the record
                return src.toJSON()

        self.request.response.status = 400
        return False
