import ast
import datetime
import logging

from sqlalchemy import (
    Column,
    Integer,
    String,
    TIMESTAMP,
    ForeignKey
)
from sqlalchemy.orm import relationship

from mfisheries.models import Base

log = logging.getLogger(__name__)


class WeatherReadings(Base):
    __tablename__ = "weatherreadings"
    id = Column(Integer, primary_key=True)
    readings = Column(String(1000), nullable=False)
    is_active = Column(Integer, nullable=False, default=1)
    sourceid = Column(Integer, ForeignKey('weathersources.id'))
    timecreated = Column(TIMESTAMP, default=datetime.datetime.utcnow)

    source = relationship("WeatherSource", foreign_keys=[sourceid], lazy='subquery')

    def __init__(self, readings={}):
        self.readings = str(readings)

    def toJSON(self):
        rec = {
            'readings': ast.literal_eval(self.readings),
            'timecreated': str(self.timecreated)
        }

        if self.sourceid is not None:
            rec['sourceid'] = self.sourceid
            rec['source'] = self.source.name
            rec['country'] = self.source.country.name
            rec['countryid'] = self.source.countryid

        return rec
