import time

# Firebase related imports
from firebase import firebase
from pyfcm import FCMNotification
from pyramid.config import Configurator
from pyramid.paster import bootstrap

from mfisheries.core.config import Config
from mfisheries.models import DBSession
from mfisheries.modules.weather.threshold.weatheralert import WeatherAlert
from mfisheries.modules.weather.weathersource import WeatherSource
from mfisheries.util import display_debug

# Expected to be executed either from the main or source script
ini_full_path = "./development.ini"


class BroadcastManager(object):
	def __init__(self):
		self.fb = None
		self.api_key = None
		self.enable_debug = True
		self.config = Configurator()
	
	def getFBApp(self):
		"""
		This method will utilize the system configured firebase connection string to setup the connection
		between the mFisheries system and Firebase
		"""
		if not self.fb:  # If Firebase not previously initialized
			c = DBSession.query(Config).filter(Config.key == "firebase_connection_string").one()
			fb_conn_str = c.value
			self.fb = firebase.FirebaseApplication(fb_conn_str, None)
			DBSession.rollback()
		return self.fb
	
	def getPushService(self):
		"""
		This method will return an instantiated FCM Notification Object.
		It will fetch the api key from the database and perform the necessary setup for enabling FB push notification services
		"""
		if not self.api_key:
			c = DBSession.query(Config).filter(Config.key == "firebase_api_key").one()
			self.api_key = c.value
			DBSession.rollback()
		
		return FCMNotification(self.api_key)
	
	def notify(self, country, message):
		push_service = self.getPushService()
		mes = ""
		for msg in message:
			mes += msg
		# TODO I need to find a better message
		mes += " notification"
		data = {
			"title": "Weather Notification",
			"message": mes,
			"type": "cap",  # TODO I have to check why only type: cap works
			"alertId": "weather"
		}
		topic = "weather-" + country
		self.debug("Sending to " + topic)
		result = push_service.notify_topic_subscribers(topic_name=topic, data_message=data)
		self.debug(str(result))
	
	def debug(self, message, type="debug"):
		if self.enable_debug:
			display_debug(message, type, module_name=__name__)
	
	def checkAllSourcesForAlerts(self):
		# Retrieve the notifications made
		fb = self.getFBApp()
		alerts = self.retrieveWeatherAlerts()
		# self.notify("dominica",["pressure"])
		for alert in alerts:
			try:
				# source = src.toJSON()
				message = alert.readings
				weather_source = self.getWeatherSource(alert.weathersourceid)
				
				# Getting country to notify
				country = weather_source.country
				country = country.strip().lower()  # strip text for firebase
				self.notify(country, message)
				self.setAsSent(alert.weathersourceid)
			except Exception, e:
				self.debug("Unable to retrieve data: "+e.message, "error")
	
	def setAsSent(self, src_id):
		try:
			query = DBSession.query(WeatherAlert)
			query = query.filter(WeatherAlert.weathersourceid == src_id)
			res = query.first()
			res.sent = 1
		
			DBSession.add(res)
			DBSession.flush()
			return res
		except Exception, e:
			err_msg = "Unable to set {0} as send: Error {1}".format(src_id, e.message)
			self.debug(err_msg, type="error")
			DBSession.rollback()
			return None
		
	def getWeatherSource(self, id):
		query = DBSession.query(WeatherSource).filter(WeatherSource.id == id).first()
		DBSession.rollback()
		return query
	
	def retrieveWeatherAlerts(self):
		res = DBSession.query(WeatherAlert).filter(WeatherAlert.sent == 0).all()
		DBSession.rollback()
		return res
	
	def saveStatus(self, alert_fb_id, message):
		fb = self.getFBApp()
		fb_notify_base = '/capalerts/notif-status/' + alert_fb_id
		self.debug("Saving notification status")
		users = fb.get('/users', None)
		for user in users:
			if 'name' in users[user]:
				result = fb.post(fb_notify_base + '/users/' + users[user]['name'], False)
				self.debug(result)
				millis = int(round(time.time() * 1000))
				fb.post(fb_notify_base + '/sent', millis)
				fb.post(fb_notify_base + '/message', message)
			else:
				self.debug("Unable to retrieve name of account")


def run_weather_broadcast(path=None, use_bootstrap=False):
	display_debug("Executing Broadcast Manager")
	# Ensures that System Modules are loaded even if executed from the command line or a service
	if use_bootstrap:
		if path:
			bootstrap(path)
		else:
			bootstrap(ini_full_path)
	bc = BroadcastManager()
	bc.checkAllSourcesForAlerts()


if __name__ == "__main__":
	run_weather_broadcast(use_bootstrap=True)
