
import imp
import os
import shutil

from cornice import Service  # allows the services to be exposed via a RESTFUL service
from pyramid.config import Configurator

from mfisheries import APP_ROOT
from mfisheries.modules.weather.parsers.WeatherSourceExtractor import WeatherSourceExtractor
from mfisheries.util import display_debug
from mfisheries.util.secure import validate

weatherExtractor = Service(name='post', path='/api/weather/extractor', description='Adds a new extractor file')

ab_module_path = os.path.join(APP_ROOT, "")
rel_module_path = os.path.join("", "")


@weatherExtractor.post()
def upload_extractor(request):
    if not validate(request):
        return {'status': 401, 'data': []}
    try:
        # check parameters
        filename = request.POST['upload'].filename
        upfile = request.POST['upload'].file
        country = request.POST['country']

    except Exception, e:
        return {'status': 400, "response": "Invalid request. Ensure all required fields are submitted"}
    config = Configurator()
    
    # All parameters received
    extension = filename.split(".")
    display_debug(extension[-1])
    if len(extension) > 1:
        if extension[-1] == "py":

            code_path = os.path.join(ab_module_path, "modules/weather/parsers/extractors")
            local_path = os.path.join(rel_module_path, "modules/weather/parsers/extractors")

            # Ensure country path exists
            if not os.path.exists(code_path):
                os.makedirs(code_path)

            # Add country to the file name to prevent conflicts
            filename = "{0}_{1}".format(country.lower().replace(" ", "_"), filename.lower())

            # Set full path for file
            file_path = os.path.join(code_path, filename)
            local_path = os.path.join(local_path, filename)
            display_debug("Local path " + str(local_path))
            display_debug("Absolute path" + str(file_path))

            # Create a temporary location for uploading the file
            temp_file_path = file_path + '~'
            upfile.seek(0)

            # Upload file to temp location
            with open(temp_file_path, 'wb') as output_file:
                shutil.copyfileobj(upfile, output_file)
            # Once upload complete then copy to desired location
            os.rename(temp_file_path, file_path)

            display_debug("Extractor saved at : " + str(file_path))
            # Dynamically importing a file using path -> https://stackoverflow.com/questions/67631/how-to-import-a-module-given-the-full-path
            config.scan('mfisheries.modules.weather.parsers.extractors')
            extractor = imp.load_source('mfisheries.modules.weather.parsers.{0}'.format(filename), file_path)
            extractor_instance = extractor.Extractor()
            if isinstance(extractor_instance, WeatherSourceExtractor):
                fields = extractor_instance.get_reading_types()
                extractor_instance.extract()
                readings = extractor_instance.toJSON()
                config.scan('mfisheries.modules.weather.parsers.extractors')
                return {
                    'status': 200,
                    "response": "File Upload Success",
                    "extractor_path": file_path,
                    "fields": fields,
                    "readings": readings
                }
            else:
                # Invalid file submitted. Remove file and send appropriate message to user
                os.remove(file_path)
                config.scan('mfisheries.modules.weather.parsers.extractors')                
                return {
                    'status': 400,
                    "response": "Incorrect Python Class upload in extractor file. Ensure file meets the required specifications and try again"
                }
    config.scan('mfisheries.modules.weather.parsers.extractors')
    return {'status': 400, "response": "Incorrect file type"}


def delete_extractor_by_path(full_path):
    if os.path.exists(full_path):
        os.remove(full_path)
