import json
import logging

from cornice.resource import resource

from mfisheries.models import DBSession
from threshold import Threshold

log = logging.getLogger(__name__)


# http://cornice.readthedocs.io/en/latest/resources.html
@resource(collection_path='/api/weather/thresholds', path='/api/weather/thresholds/{id}',
          description='Weather Thresholds')
class WeatherThresholdHandler(object):
	def __init__(self, request, context=None):
		self.request = request
		self.context = context
		self.fields = ['weathersourceid', 'thresholds']
	
	#  Private Helper Methods
	def __isvalid(self, values):
		for f in self.fields:
			if f not in values:
				return False
		return True
	
	def __save_src(self, data, rec_id=0):
		try:
			if rec_id == 0:  # Dealing with saving a new record
				src = Threshold("", "")
			else:  # Attempting to update an existing record
				src = DBSession.query(Threshold).get(rec_id)
			print data
			src.setThresholds(data['thresholds'])
			src.weathersourceid = data['weathersourceid']
			
			# Save the operation and return the object
			DBSession.add(src)
			DBSession.flush()
			return src
		except Exception, e:
			log.error("Unable to save Source: " + e.message)
			return False

	
	def __custom_save_src(self, data, rec_id=0):
		try:
			if rec_id == 0:  # Dealing with saving a new record
				src = Threshold("", "")
			else:  # Attempting to update an existing record
				src = DBSession.query(Threshold).filter(Threshold.weathersourceid == rec_id).first()
			print data
			src.setThresholds(data['thresholds'])
			src.weathersourceid = data['weathersourceid']
			
			# Save the operation and return the object
			DBSession.add(src)
			DBSession.flush()
			return src
		except Exception, e:
			log.error("Unable to save Source: " + e.message)
			return False
	
	# Methods exposing API calls
	def collection_get(self):
		# retrieve all weather sources from table
		res = DBSession.query(Threshold).all()
		print res
		if len(res) > 0:
			# convert each record received to JSON format
			res = map(lambda m: m.toJSON(), res)
		# send data as json
		return res
	
	def get(self):
		# Extract id passed as a parameter within the URL
		wt_id = int(self.request.matchdict['id'])
		# Attempt to Retrieve record from database
		res = DBSession.query(Threshold).filter(Threshold.weathersourceid ==wt_id).first()
		# If we successfully receive a record with the id specified
		if res:
			return res.toJSON()
		# Send a HTTP Status code to tell client id was not found
		self.request.response.status = 404
		return False
	
	def delete(self):
		# Extract id passed as a parameter within the URL
		ws_id = int(self.request.matchdict['id'])
		try:
			# Retrieve the record so we can delete the corresponding extractor file
			DBSession.query(Threshold).filter(Threshold.weathersourceid == ws_id).delete()
			
			return True
		except Exception, e:
			print "ERROR: Unable to delete Weather Source"
			log.error(e.message)
			self.request.response.status = 400
			return False
	
	def put(self):
		# Extract data to be updated from the body of the put request
		data = self.request.json_body
		# Extract id passed as a parameter within the URL
		ws_id = int(self.request.matchdict['id'])
		if self.__isvalid(data):
			src = self.__custom_save_src(data, ws_id)
			if src:
				return src.toJSON()
		
		self.request.response.status = 400
		return False
	
	def collection_post(self):
		# Extract data to be updated from the body of the post request
		data = self.request.json_body
		print data
		# Ensure required fields are specified
		if self.__isvalid(data):
			# Attempt to save the new source
			src = self.__save_src(data)
			# If source was successfully saved
			if src:
				# Notify the client using the appropriate HTTP status code
				self.request.response.status = 201
				# Send the record
				return src.toJSON()
		
		self.request.response.status = 400
		return False
