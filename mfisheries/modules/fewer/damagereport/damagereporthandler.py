from cornice.resource import resource
from mfisheries.core import BaseHandler
from mfisheries.models import DBSession
from sqlalchemy import or_, desc

from mfisheries.util.fileutil import store_media
from .damagereport import DamageReport
from .damagereportcategory import DamageReportCategory

import logging
from datetime import datetime
log = logging.getLogger(__name__)


# http://cornice.readthedocs.io/en/latest/resources.html
@resource(collection_path='/api/damagereport', path='/api/damagereport/{id}', description="Damage Reporting")
class DamageReportHandler(BaseHandler):
	def __init__(self, request, context=None):
		BaseHandler.__init__(self, request, context)
		self.order_func = desc
	
	def _get_order_field(self):
		return DamageReport.timecreated
	
	def _create_empty_model(self):
		return DamageReport(0, 0, "")

	def _get_fields(self):
		return self._create_empty_model().getRequiredFields()

	def _get_target_class(self):
		return DamageReport

	def _dict_to_model(self, data, rec_id=0):
		# If id is 0, we want to create a new entry
		if rec_id == 0:
			src = self._create_empty_model()
		else:
			src = DBSession.query(self._target_class).get(rec_id)
		# Assign value to the fields of the model
		src.name = data["name"]
		src.description = data["description"]
		src.userid = data["userid"]
		src.countryid = data["countryid"]
		src.filetype = data["filetype"]
		src.category = data['category']
		src.latitude = data["latitude"]
		src.longitude = data["longitude"]
		
		# Ensure we can accommodate for both numbers and words else default to private
		try:
			src.isPublic = int(data['isPublic'])
		except:
			try:
				if data['isPublic'].lower() is "public":
					src.isPublic = 1
				elif data['isPublic'].lower() is "private":
					src.isPublic = 0
			except Exception:
				src.isPublic = 0
		
		# Try to process date
		try:
			src.aDate = datetime.fromtimestamp(int(data["aDate"]))
		except:
			try:
				# print("Attempting alternative format for " + str(data['aDate']))
				src.aDate = datetime.fromtimestamp(int(data["aDate"])/1000)
			except Exception, e:
				log.error("Invalid date: {0}".format(e))
				# print("Failed to convert date from:" + str(data['aDate']))
		
		if "cost" in data:
			try:
				src.cost = float(data['cost'])
			except Exception, e:
				src.cost = 0
				log.error("Invalid date: {0}".format(e))
				
		# If user report is about is different than person submitting
		if "createdby" in data:
			src.createdby = data["createdby"]
		else:
			src.createdby = data['userid']
			
		if "filepath" in data:
			src.filepath = data["filepath"]
	
		return src
	

	
	def _handle_conditional_get(self, query):
		query = super(DamageReportHandler, self)._handle_conditional_get(query)
		
		# Handle individual user reports
		if 'userid' in self.request.params:
			userid = self.request.params['userid']
			query = query.filter(DamageReport.userid == userid)
		
		# Handle visibility
		if self.request.params.has_key('type'):
			is_public = self.request.params['type']
			if is_public == 'public':
				query = query.filter(DamageReport.isPublic == 1)
			elif is_public == 'all':
				query = query.filter(or_(DamageReport.isPublic == 1, DamageReport.isPublic == 0))
			elif is_public == 'private':
				query = query.filter(DamageReport.isPublic == 0)
		else:
			query = query.filter(DamageReport.isPublic == 1)
			
		return query
	
	def collection_post(self):
		res = super(DamageReportHandler, self).collection_post()
		log.debug(res)
		if res:
			if "file" in self.request.POST:
				file_data = {
					"filename": self.request.POST['file'].filename,
					"file": self.request.POST['file'].value,
					"type": self.request.POST['filetype'],
					"userid": self.request.POST['userid']
				}
				location = store_media(file_data)
				# If file successfully
				if location is not "":
					location_str = "static" + location.split('static')[1]
					res['filepath'] = location_str
					src = self._save(res, res['id'])
					if src:
						return src.toJSON()
				self.request.response.status = 400
				res['message'] = "Unable to save file resource"
				# TODO Should the record be rollback or deleted at this point
		return res
		



@resource(collection_path='/api/damagereport/category', path='/api/damagereport/category/{id}', description="Damage Reporting")
class DamageReportCategoryHandler(BaseHandler):
	def __init__(self, request, context=None):
		BaseHandler.__init__(self, request, context)
	
	def _get_order_field(self):
		return DamageReportCategory.name

	def _create_empty_model(self):
		return DamageReportCategory(0, "")

	def _get_fields(self):
		return self._create_empty_model().getRequiredFields()

	def _get_target_class(self):
		return DamageReportCategory

	def _dict_to_model(self, data, rec_id=0):
		# If id is 0, we want to create a new entry
		if rec_id == 0:
			src = self._create_empty_model()
		else:
			src = DBSession.query(self._target_class).get(rec_id)
		# Assign value to the fields of the model
		src.countryid = data["countryid"]
		src.name = data['name']
		
		if "createdby" in data:
			src.createdby = data['createdby']

		return src