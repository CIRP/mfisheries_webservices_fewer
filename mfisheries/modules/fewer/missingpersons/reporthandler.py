from cornice.resource import resource
from mfisheries.core import BaseHandler
from mfisheries.models import DBSession
from sqlalchemy import or_, desc
import logging
log = logging.getLogger(__name__)

from mfisheries.util.fileutil import store_media
from .report import Report
from .reportfields import MSReportField


# http://cornice.readthedocs.io/en/latest/resources.html
@resource(collection_path='/api/missingpersons', path='/api/missingpersons/{id}', description="Missing Persons")
class MSReportHandler(BaseHandler):
    def __init__(self, request, context=None):
        BaseHandler.__init__(self, request, context)
        self.order_func = desc

    def _get_order_field(self):
        return Report.timecreated

    def _create_empty_model(self):
        return Report(0,"")

    def _dict_to_model(self, data, rec_id=0):
        # If id is 0, we want to create a new entry
        if rec_id == 0:
            src = self._create_empty_model()
        else:
            src = DBSession.query(self._target_class).get(rec_id)
        # Assign value to the fields of the model
        src.title = data["title"]
        src.userid = data['userid']
        src.countryid = data['countryid']
        src.contact = data['contact']

        if "filepath" in data:
            src.filepath = data['filepath']

        if "description" in data:
            src.description = data['description']
        if "additional" in data:
            src.additional = data['additional']
        if "isFound" in data:
            src.isFound = data['isFound']

        if "latitude" in data:
            src.latitude = data["latitude"]
        if "longitude" in data:
            src.longitude = data["longitude"]

        # # TODO Implement receiving multiple fields
        # if "fields" in data:
        #     pass

        return src

    def _get_fields(self):
        return self._create_empty_model().getRequiredFields()

    def _get_target_class(self):
        return Report

    def collection_post(self):
        res = super(MSReportHandler, self).collection_post()
        log.debug(res)
        if res:
            if "file" in self.request.POST:
                file_data = {
                    "filename": self.request.POST['file'].filename,
                    "file": self.request.POST['file'].value,
                    "type": 'image',
                    "userid": self.request.POST['userid']
                }
                location = store_media(file_data)
                # If file successfully
                if location is not "":
                    location_str = "static" + location.split('static')[1]
                    res['filepath'] = location_str
                    src = self._save(res, res['id'])
                    if src:
                        return src.toJSON()
                self.request.response.status = 400
                res['message'] = "Unable to save file resource"
        return res

# http://cornice.readthedocs.io/en/latest/resources.html
@resource(collection_path='/api/missingpersons/fields', path='/api/missingpersons/fields/{id}', description="Damage Reporting")
class MSReportFieldHandler(BaseHandler):
    def __init__(self, request, context=None):
        BaseHandler.__init__(self, request, context)
    
    def _get_fields(self):
        return self._create_empty_model().getRequiredFields()

    def _dict_to_model(self, data, rec_id=0):
        # If id is 0, we want to create a new entry
        if rec_id == 0:
            src = self._create_empty_model()
        else:
            src = DBSession.query(self._target_class).get(rec_id)
        src.name = data['name']
        return src

    def _create_empty_model(self):
        return MSReportField("")

    def _get_order_field(self):
        return MSReportField.name

    def _get_target_class(self):
        return MSReportField