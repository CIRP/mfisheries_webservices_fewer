from datetime import datetime

from sqlalchemy import (
    Column,
    Integer,
    String,
    TIMESTAMP,
    ForeignKey,
    TEXT,
    func,
    UniqueConstraint)
from sqlalchemy.orm import relationship

from mfisheries.models import Base
from .reportfields_bridge import MSReportFieldsBridge


class Report(Base):
    """The SQLAlchemy model class for Making reports"""
    __tablename__ = 'msreports'
    id = Column(Integer, primary_key=True)
    title = Column(String(100), nullable=False)
    description = Column(TEXT, default="")
    additional = Column(TEXT)
    contact = Column(String(10))
    filepath = Column(String(200))
    latitude = Column(String(20))
    longitude = Column(String(20))
    userid = Column(Integer, ForeignKey('user.id'))
    countryid = Column(Integer, ForeignKey('country.id'), nullable=False)
    timecreated = Column(TIMESTAMP, default=datetime.now)
    timeupdated = Column(TIMESTAMP, server_default=func.now(), onupdate=func.current_timestamp())
    isFound = Column(Integer, default=0)

    country = relationship("Country", foreign_keys=[countryid], lazy='subquery')
    user = relationship("User", foreign_keys=[userid], lazy="subquery")

    # http://docs.sqlalchemy.org/en/latest/core/constraints.html#sqlalchemy.schema.UniqueConstraint
    __table_args__ = (UniqueConstraint('userid', 'title', name='msreuq_1'),)

    fields = relationship(
        "MSReportField",
        secondary=MSReportFieldsBridge,
        backref='Report'
    )

    def __init__(self, title, userid):
        self.title = title
        self.userid = userid

    def getRequiredFields(self):
        return [
            "title",
            "userid",
            "countryid",
            "contact"
        ]

    def toJSON(self):
        rec = {
            "id": self.id,
            "title":self.title,
            "description":self.description,
            "additional":self.additional,
            "contact": self.contact,
            "filepath": self.filepath,
            "userid": self.userid,
            "countryid": self.countryid,
            "latitude": self.latitude,
            "longitude": self.longitude,
            "timecreated": str(self.timecreated),
            "timeupdated": str(self.timeupdated),
            "isFound": self.isFound
        }
        if self.country:
            rec['country'] = self.country.name
        if self.user:
            rec["user"]= self.user.username
            rec["fullname"]= "{0} {1}".format(self.user.fname, self.user.lname)

        if self.fields:
            rec['fields']= []

        return rec