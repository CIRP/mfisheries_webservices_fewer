import datetime
import logging

from sqlalchemy import (
	Column,
	Integer,
	String,
	TIMESTAMP,
	ForeignKey,
	UniqueConstraint
)
from sqlalchemy.orm import relationship

from mfisheries.models import Base

log = logging.getLogger(__name__)


# noinspection SpellCheckingInspection,PyPep8Naming
class CAPSource(Base):
    __tablename__ = "capsources"
    id = Column(Integer, primary_key=True)
    countryid = Column(ForeignKey('country.id'))
    name = Column(String(50), nullable=False)
    category = Column(String(30), default="CAP")
    scope = Column(String(45), default="mFisheries")
    url = Column(String(200), nullable=False)
    createdby = Column(Integer, ForeignKey('user.id'))
    created = Column(TIMESTAMP,default=datetime.datetime.utcnow)
    lastAlert = Column(String(200))

    country = relationship("Country", foreign_keys=[countryid], lazy='subquery')
    # http://docs.sqlalchemy.org/en/latest/core/constraints.html#sqlalchemy.schema.UniqueConstraint
    __table_args__ = (UniqueConstraint('name', 'countryid', name='_source_location_uc'),)

    def __init__(self, cid, name, cat, url, cb):
        self.countryid = cid
        self.name = name
        self.category = cat
        self.url = url
        self.createdby = cb

    def toJSON(self):
        return {
            'id': self.id,
            'countryid': self.countryid,
            'country': self.country.name,
            'name': self.name,
            'category': self.category,
            'url': self.url,
            'createdBy': self.createdby,
            'lastAlert': self.lastAlert,
            'scope': self.scope,
            'created': str(self.created)
        }
