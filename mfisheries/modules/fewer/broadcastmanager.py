import time

import requests
# Firebase related imports
from firebase import firebase
from pyfcm import FCMNotification
from pyramid.paster import bootstrap

from mfisheries.core.config import Config
from mfisheries.models import DBSession
from mfisheries.modules.fewer.capsource import CAPSource
from mfisheries.util import display_debug
from mfisheries.events import NotifyError
from pyramid.config import Configurator

# Expected to be executed either from the main or source script
ini_full_path = "./development.ini"


class BroadcastManager(object):
	def __init__(self):
		self.fb = None
		self.api_key = None
		self.enable_debug = True
		self.config = Configurator()

	def getFBApp(self):
		"""
		This method will utilize the system configured firebase connection string to setup the connection
		between the mFisheries system and Firebase
		"""
		if not self.fb:  # If Firebase not previously initialized
			c = DBSession.query(Config).filter(Config.key == "firebase_connection_string").one()
			fb_conn_str = c.value
			self.fb = firebase.FirebaseApplication(fb_conn_str, None)
			DBSession.rollback()
		return self.fb

	def getPushService(self):
		"""
		This method will return an instantiated FCM Notification Object.
		It will fetch the api key from the database and perform the necessary setup for enabling FB push notification services
		"""
		if not self.api_key:
			c = DBSession.query(Config).filter(Config.key == "firebase_api_key").one()
			self.api_key = c.value
			DBSession.rollback()

		return FCMNotification(self.api_key)

	def notify(self, alert):
		push_service = self.getPushService()
		self.debug("Notifying of alert = " + alert["alert_id"])
		data = {
			"title": "FEWER Alert!",
			"message": alert["title"],
			"type": "cap",
			"alertId": alert["alert_id"]
		}
		topic = "cap-" + alert['audience'].lower()
		self.debug("Sending to " + topic)
		result = push_service.notify_topic_subscribers(topic_name=topic, data_message=data)
		self.debug(str(result))
		self.saveStatus(alert["alert_id"], alert["title"])

	def debug(self, message, type="debug"):
		if self.enable_debug:
			display_debug(message, type, module_name=__name__)

	def checkAllSourcesForAlerts(self):
		"""
		Runs through the process for determine if external CAP sources have alerts and send broadcasts to subscribed clients
		Steps:
		1. Extract CAP sources from the database
		2. Check the Feed.xml for each record
		3. Compare the Alert(s) received at the source to determine if a new alert was issued
		4. If new alert issued (by source) the system adds alert info to Firebase and Broadcast alert to country clients
		"""
		# step 1
		sources = self.retrieveSources()
		fb = self.getFBApp()
		# step 2
		for src in sources:
			try:
				self.debug("Retrieving CAP alerts for: {0}".format(src.country.name.lower()))
				fb_alert_path = "/capalerts/last_alerts/{0}/".format(src.country.name.lower())
				fb_alert_name = str(src.id)
				last_alert = fb.get(fb_alert_path, fb_alert_name)
				self.debug("Received {0} from the Firebase path {1}".format(last_alert, fb_alert_path))
				self.debug("Name: {0} with feed from {1}".format(src.name, src.url))
				feed_url = src.url
				self.debug("Retrieving information from: {0}".format(feed_url))
				r = requests.get(feed_url)
				if r.status_code == 200:
					if r.headers['Content-Type'] == "application/json":
						self.debug("CAP for {0} fetched successfully".format(src.name))
						alerts = r.json()
						# step 3
						for alert in alerts:
							self.debug("Current alert = {0}".format(alert['alert_id']))
							if alert['alert_id'] == last_alert:
								self.debug("Already notified")
								break
							else:
								self.notify(alert)  # step 4
						# TODO This implementation needs to be evaluated to determine what limitations exists.
						# TODO These limitations should be clearly documented
						if len(alerts) > 0:
							if alerts[0]['alert_id'] != last_alert:
								self.debug("Updating last alert to " + alerts[0]['alert_id'])
								result = fb.put(fb_alert_path, fb_alert_name, alerts[0]['alert_id'])
								self.debug(str(result))
						else:
							self.debug("No alerts found at: {0}".format(src.name))
					else:
						self.debug("Data was retrieved from the source as {0}".format(r.headers['Content-Type']))
			except Exception, e:
				self.config.registry.notify(NotifyError("BroadcastManager", "Error Occurred when processing alert for {0}".format(src.name)))
				self.debug(e, "error")

	def retrieveSources(self, countryid=0):
		query = DBSession.query(CAPSource).filter(CAPSource.category == "JSON")
		if countryid is not 0:
			query = query.filter(CAPSource.countryid == countryid)
		res = query.all()
		DBSession.rollback()
		return res

	def saveStatus(self, alert_fb_id, message):
		fb = self.getFBApp()
		fb_notify_base = '/capalerts/notif-status/' + alert_fb_id
		self.debug("Saving notification status")
		users = fb.get('/users', None)
		for user in users:
			if 'name' in users[user]:
				result = fb.post(fb_notify_base + '/users/' + users[user]['name'], False)
				self.debug(result)
				millis = int(round(time.time() * 1000))
				fb.post(fb_notify_base + '/sent', millis)
				fb.post(fb_notify_base + '/message', message)
			else:
				self.debug("Unable to retrieve name of account")


def run_bcast_mgmtr_default(path=None, use_bootstrap=False):
	display_debug("Executing Broadcast Manager")
	# Ensures that System Modules are loaded even if executed from the command line or a service
	if use_bootstrap:
		if path:
			bootstrap(path)
		else:
			bootstrap(ini_full_path)
	bc = BroadcastManager()
	bc.checkAllSourcesForAlerts()


if __name__ == "__main__":
	run_bcast_mgmtr_default(use_bootstrap=True)
