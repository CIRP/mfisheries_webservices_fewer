import datetime

from sqlalchemy import (
    Column,
    Integer,
    String,
    TIMESTAMP,
    ForeignKey,
    Boolean
)
from sqlalchemy.orm import relationship

from mfisheries.models import Base


# noinspection SpellCheckingInspection,PyPep8Naming
class Messages(Base):
    """The SQLAlchemy model class for a Messages object"""
    __tablename__ = 'messages'
    id = Column(Integer, primary_key=True)
    userid = Column(Integer, ForeignKey('user.id'))
    groupid = Column(Integer, ForeignKey('usergroups.id'))
    messagecontent = Column(String(100))
    description = Column(String(300), nullable=True)
    severity = Column(String(100), default="Unknown")
    deviceTimestamp = Column(TIMESTAMP)
    latitude = Column(String(20))
    longitude = Column(String(20))
    likes = Column(Integer, default=0)
    dislikes = Column(Integer, default=0)
    timestamp = Column(TIMESTAMP,default=datetime.datetime.utcnow)
    isPublic = Column(Boolean, default=False)
    isVerified = Column(Boolean, default=False)

    group = relationship("UserGroups", foreign_keys=[groupid])
    user = relationship("User", foreign_keys=[userid], lazy='subquery')

    def __init__(self, userid, groupid, messagecontent, deviceTimestamp, latitude, longitude, isPublic):
        self.userid = userid
        self.groupid = groupid
        self.messagecontent = messagecontent
        self.deviceTimestamp = deviceTimestamp
        self.latitude = latitude
        self.longitude = longitude
        self.isPublic = isPublic
        self.likes = 0
        self.dislikes = 0

    def toJSON(self):
        rec = {
            'id': self.id,
            'userid': self.userid,
            'groupid': self.groupid,
            'messagecontent': self.messagecontent,
            'deviceTimestamp': str(self.deviceTimestamp),
            'latitude': self.latitude,
            'longitude': self.longitude,
            'likes': self.likes,
            'dislikes': self.dislikes,
            'timestamp': str(self.timestamp),
            'group': self.group.groupname,
            'country': self.group.country.name,
            'isPublic': self.isPublic,
            'isVerified': self.isVerified,
            'severity': self.severity,
            'messageSeverity': self.severity,
            'messageDescription': self.description
        }

        if self.user:
            rec["username"] = self.user.username
            rec["user"]= self.user.username
            rec["fullname"]= "{0} {1}".format(self.user.fname, self.user.lname)

        return rec
