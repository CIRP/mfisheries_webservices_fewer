from apscheduler.schedulers.background import BackgroundScheduler

from mfisheries.models import DBSession
from mfisheries.modules.alerts import Messages
from mfisheries.modules.alerts import UserGroups
from mfisheries.modules.alerts.alertshandler import AlertHandler
from mfisheries.util import display_debug

FREQUENCY = {
    "Every 6 Hours": 6,
    "Every Hour": 1,
    "Once a day": 24
}

def run_alert_scheduled_task():

    usergroups = DBSession.query(UserGroups).filter(UserGroups.groupname.Like('%Public Advisories%'))
    sched = BackgroundScheduler()

    # Messages message
    # message.userid = 1
    # message.messagecontent = "Monthly alert test"
    # message.description = ""
    # message.severity = "0 - Not Applicable"
    # message.likes = 0
    # message.dislikes = 0
    # message.isPublic = 0
    # message.isVerified = 0


    # for entry in usergroups:
    #     try:
    #         message = AlertHandler._create_empty_model()
    #
    #
    #
    #
    # sched.start()
    # display_debug("Test Alerts Sent Successfully", "info")
