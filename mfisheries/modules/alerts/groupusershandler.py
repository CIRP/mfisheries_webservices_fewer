import logging
log = logging.getLogger(__name__)

from cornice import Service

from mfisheries.models import DBSession
from mfisheries.modules.alerts.usergroups import UserGroups

groupMembers = Service(name='groupMembers', path='api/groups/{groupid}/members', description='add new lek')

@groupMembers.get()
def retrieveMembers(request):
	try:
		groupid = int(request.matchdict['groupid'])
		userGroup = DBSession.query(UserGroups).get(groupid)
		if userGroup:
			subscriber = map(lambda sub: sub.toJSON(), userGroup.subscribers)
			return subscriber
	except Exception, e:
		log.error(e)
	request.response.status = 404
	return False