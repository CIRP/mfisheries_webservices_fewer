import logging
log = logging.getLogger(__name__)

from cornice.resource import resource
from mfisheries.modules.alerts.usergroups import UserGroups
from mfisheries.models import DBSession
from mfisheries.util.secure import validate
from sqlalchemy import text

from mfisheries.core import BaseHandler


# noinspection SpellCheckingInspection,PyMethodMayBeStatic
@resource(collection_path='/api/groups', path='/api/groups/{id}', description='Alert Groups')
class AlertGroupHandler(BaseHandler):
    def _get_order_field(self):
        return UserGroups.groupname

    def __init__(self, request, context=None):
        BaseHandler.__init__(self, request, context)

    def _get_fields(self):
        return ['groupname', 'countryid', 'oneWay']

    def _create_empty_model(self):
        return UserGroups("", "", "")

    def _get_target_class(self):
        return UserGroups

    def _dict_to_model(self, data, rec_id=0):
        # If id is 0, we want to create a new entry
        if rec_id == 0:
            src = self._create_empty_model()
        else:
            src = DBSession.query(self._target_class).get(rec_id)
        # Assign value to the fields of the model
        src.countryid = data['countryid']
        src.groupname = data['groupname']
        src.oneWay = data['oneWay']
        return src

    def _process_results(self, res):
        if len(self.request.GET) > 0 and "expanded" in self.request.GET and self.request.GET['expanded']:
            res = map(lambda m: m.toExpandedJSON(), res)  # convert to expanded JSON format
        else:
            res = map(lambda m: m.toJSON(), res)  # convert each record received to JSON format

        return res


# noinspection SpellCheckingInspection,SqlDialectInspection,SqlNoDataSourceInspection
@resource(path='/api/user/{userid}/groups', description='User groups')
class UserGroupsHandler(object):
    """
    Retrieves the groups associated with a user.
    """
    def __init__(self, request, context=None):
        self.request = request
        self.context = context
        log.debug("Instantiating user group handler")

    def __get_country_groups(self, userid):
        """
        Retrieves the country groups based on the users registration country.
        :param userid: The userid id currently requesting group information on
        :return:
        """
        try:
            stmt = text("SELECT g.id, g.oneWay, g.groupname as groupName, c.name as country, c.id as countryid\
						from usergroups g, country c inner JOIN(select u1.countryid from user u1 where u1.id = :userid) as c1\
						ON c1.countryid = c.id\
						where g.countryid = c.id")  # raw sql statement
            result = DBSession.execute(stmt, {"userid": userid})
            arr = []
            for model in result:
                arr.append(dict(model))
            return arr
        except Exception, e:
            print e.message
            return None

    def __get_user_groups(self, userid):
        """
		Retrieves the user groups that the user is currently subscribed to.
        :param userid:
        :return:
        """
        try:
            stmt = text("SELECT ug.id, ug.groupname from usergroups ug, subscribers s where ug.id = s.usergroupid and s.userid = :userid")
            result = DBSession.execute(stmt, {"userid": userid})
            arr = []
            for model in result:
                arr.append(dict(model))
            return arr
        except Exception, e:
            print e.message
            return None

    def get(self):
        if not validate(self.request):
            return {'status': 401, 'data': []}
        userid = self.request.matchdict['userid']
        groups = self.__get_country_groups(userid)
        ugroups = self.__get_user_groups(userid)

        print ugroups

        temp = []
        for group in groups:
            if len(ugroups) > 0:
                for ug in ugroups:
                    if group['id'] == ug['id']:
                        group['isSubscribed'] = True
                        break
                    else:
                        group['isSubscribed'] = False
            else:
                group['isSubscribed'] = False
            temp.append(group)

        if groups is not None:
            return {'status': 200, "data": temp}
        else:
            return {'status': 500, "data": []}
