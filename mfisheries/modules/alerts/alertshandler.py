from datetime import datetime

from cornice.resource import resource
from sqlalchemy import text

from mfisheries.core import BaseHandler
from mfisheries.models import DBSession
from mfisheries.modules.alerts.messages import Messages
from mfisheries.util import gcmclient, display_debug


# Implemented Based on the BaseHandler and Structure in DamageReportHandler
# noinspection SpellCheckingInspection
@resource(collection_path='api/alerts', path='/api/alerts/{id}', description='Send Alerts')
class AlertHandler(BaseHandler):
    def _get_order_field(self):
        return Messages.timestamp

    def __init__(self, request, context=None):
        BaseHandler.__init__(self, request, context)

    def _get_fields(self):
        return [
            'userid',
            'groupid',
            'messagecontent',
            'deviceTimestamp',
            'latitude',
            'longitude',
            'isPublic'
        ]

    def _create_empty_model(self):
        return Messages("", "", "", "", "", "", "")

    def _get_target_class(self):
        return Messages

    def _handle_conditional_get(self, query):
        query = super(AlertHandler, self)._handle_conditional_get(query)
        if self.request.params.has_key('type'):
            is_public = self.request.params['type']
            if is_public == 'public':
                query = query.filter(Messages.isPublic == True)

        if self.request.params.has_key('group'):
            groupid = self.request.params['group']
            query = query.filter(Messages.groupid == groupid)

        return query

    def _dict_to_model(self, data, rec_id=0):
        # If id is 0, we want to create a new entry
        if rec_id == 0:
            src = self._create_empty_model()
        else:
            src = DBSession.query(self._target_class).get(rec_id)

        src.userid = data['userid']
        src.groupid = data['groupid']
        src.messagecontent = data['messagecontent']
        src.deviceTimestamp = datetime.strptime(data['deviceTimestamp'], '%Y-%m-%d %H:%M:%S')
        src.latitude = data['latitude']
        src.longitude = data['longitude']
        src.isPublic = data['isPublic'] if "isPublic" in data else "0"
        src.likes = data['likes'] if "likes" in data else "0"
        src.dislikes = data['dislikes'] if "dislikes" in data else "0"
        src.description = data['messageDescription'] if "messageDescription" in data else ""
        src.severity = data['messageSeverity'] if "messageSeverity" in data else "Unknown"

        return src

    def __getgcmids(groupid):
        try:
            stmt = text("SELECT u.gcmId from user u, subscribers s where u.id = s.userid and s.usergroupid = :groupid")
            result = DBSession.execute(stmt, {"groupid": groupid})
            arr = []
            for model in result:
                arr.append(dict(model)['gcmId'])
            return arr
        except Exception, e:
            display_debug(e.message, "error")
            return None

    # Overiding the POST to support notification via Cloud Messaging
    def collection_post(self):
        alert = super(AlertHandler, self).collection_post()
        if alert:
            alert['title'] = "New Alert" + "-" + alert['messagecontent']
            alert['message'] = alert['messagecontent']
            alert['type'] = "alert"
            
            if "fullname" not in alert:
                alert['fullname'] = "Admin"
            elif alert['username'] is None:
                alert['fullname'] = "Admin"
            
            # Send using Google Cloud Messaging
            gcmclient.notifyTopic(alert['groupid'], alert)
        # If Not successful, the 400 would have already be set
        return alert
