import datetime

import transaction
from sqlalchemy import (
    Column,
    Integer,
    String,
    Boolean,
    TIMESTAMP,
    ForeignKey,
    UniqueConstraint)
from sqlalchemy import event
from sqlalchemy.orm import relationship

from mfisheries.core.country import Country
from mfisheries.models import Base
from mfisheries.models import DBSession


class UserGroups(Base):
    """The SQLAlchemy model class for a UserGroups object"""
    __tablename__ = 'usergroups'
    id = Column(Integer, primary_key=True)
    groupname = Column(String(100))
    countryid = Column(Integer, ForeignKey('country.id'))
    creationTime = Column(TIMESTAMP, default=datetime.datetime.utcnow)
    messages = relationship('Messages', backref='usergroups', cascade='all')

    subscribers = relationship('Subscribers', backref='usergroups', cascade='all', lazy='dynamic')
    oneWay = Column(Boolean, unique=False, default=False)

    country = relationship(Country, foreign_keys=[countryid])
    __table_args__ = (UniqueConstraint('groupname', 'countryid', name='_unique_country_group'),)


    def __init__(self, groupname, countryid, oneWay):
        self.groupname = groupname
        self.countryid = countryid
        self.oneWay = oneWay

    def toJSON(self):
        return {
            'id': self.id,
            'groupname': self.groupname,
            "countryid": self.countryid,
            'creationTime': str(self.creationTime),
            "country": self.country.name,
            "oneWay": bool(self.oneWay),
            "timecreated": str(self.creationTime)
        }

    def toExpandedJSON(self):
        val = self.toJSON()
        # TODO Use SQLAlchemy to do a count rather than retrieve records
        val['no_message'] = len(self.messages)
        val['no_subscribers'] = len(self.subscribers)
        return val

    def getMessages(self):
        return self.messages

    def getSubscribers(self):
        return self.subscribers


def create_default_country_groups(target, connection_rec, **kw):
    print("Creating Default Country Groups")
    default_group_list = ["Public Advisories", "National Fishers' Group"]
    default_permission = [1, 0]
    try:
        countries = DBSession.query(Country).all()
        for row in countries:
            for oc in range(0, len(default_group_list)):
                group = default_group_list[oc]
                permission = default_permission[oc]
                ug = UserGroups("{0}-{1}".format(group, row.name), row.id, permission)
                DBSession.add(ug)
        DBSession.flush()
    except Exception, e:
        print("Error Occurred on the creating of default groups: {0}".format(e))
        DBSession.rollback()
    transaction.commit()


event.listen(Country.__table__, 'after_create', create_default_country_groups)
