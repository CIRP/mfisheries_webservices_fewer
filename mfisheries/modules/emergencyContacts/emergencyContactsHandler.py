import logging

from cornice.resource import resource

log = logging.getLogger(__name__)

from mfisheries.modules.emergencyContacts.emergencyContacts import EmergencyContact
from mfisheries.models import DBSession
from mfisheries.core import BaseHandler


# http://cornice.readthedocs.io/en/latest/resources.html
@resource(collection_path='/api/emergencycontact', path='/api/emergencycontact/{id}',description="Emergency Contact Info")
class EmergencyContactHandler(BaseHandler):
    def _get_order_field(self):
        return EmergencyContact.name

    def __init__(self, request, context=None):
        BaseHandler.__init__(self, request, context)
    
    def _create_empty_model(self):
        return EmergencyContact("", "", "", "")
    
    def _get_fields(self):
        return self._create_empty_model().getRequiredFields()
    
    def _get_target_class(self):
        return EmergencyContact
    
    def _dict_to_model(self, data, rec_id=0):
        # If id is 0, we want to create a new entry
        if rec_id == 0:
            src = self._create_empty_model()
        else:
            src = DBSession.query(self._target_class).get(rec_id)
        # Assign value to the fields of the model
        src.countryid = data['countryid']
        src.name = data['name']
        src.phone = data['phone']
        src.type = data['type']
        if "image_url" in data:
            src.image_url = data['image_url']
        if "email" in data:
            src.email = data['email']
        if "createdby" in data:
            src.createdby = data["createdby"]
        if "additional" in data:
            src.additional = data['additional']
        return src