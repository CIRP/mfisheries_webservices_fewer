from sqlalchemy import (
    Column,
    Integer,
    String,
    TIMESTAMP,
    ForeignKey
)

from mfisheries.models import Base
import datetime

class ResolvedSos(Base):
    """The SQLAlchemy model class for a ResolvedSos object"""
    __tablename__ = 'resolvesos'
    id = Column(Integer, primary_key=True)
    userid = Column(Integer, ForeignKey('user.id'))
    caserefno = Column(String(100))
    details = Column(String(100))
    timestamp = Column(TIMESTAMP, default=datetime.datetime.utcnow)
    trackid = Column(Integer, ForeignKey('trackpoints.id'), nullable=False)
    coastguardnumber = (String(100))

    def __init__(self, caserefno, details, timestamp, trackid, coastguardnumber):
        self.caserefno = caserefno
        self.details = details
        self.timestamp = timestamp
        self.trackid = trackid
        self.coastguardnumber = coastguardnumber

    def toJSON(self):
        json = {}
        json['id'] = self.id
        json['userid'] = self.userid
        json['caserefno'] = self.caserefno
        json['details'] = self.details
        json['timestamp'] = str(self.timestamp)
        json['trackid'] = self.trackid
        json['coastguardnumber'] = self.coastguardnumber

        return json
