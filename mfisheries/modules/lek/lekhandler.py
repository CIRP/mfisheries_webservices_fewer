import json
import logging
from datetime import datetime

import transaction  # transaction manager
from cornice.resource import resource
from sqlalchemy import desc, or_
# from cornice import Service
from sqlalchemy import text

from mfisheries.core import BaseHandler
from mfisheries.models import DBSession
from mfisheries.modules.lek.lek import LEK
from mfisheries.modules.lek.lekcategories import LEKCategory
from mfisheries.util.fileutil import store_media
from mfisheries.util.secure import validate
from mfisheries.util import display_debug

log = logging.getLogger(__name__)


# TODO Put a restriction to return public records for general get using _handle_conditional_get
# http://cornice.readthedocs.io/en/latest/resources.html
@resource(collection_path='/api/lek', path='/api/lek/{id}', description="LEK")
class LEKHandler(BaseHandler):
	def __init__(self, request, context=None):
		BaseHandler.__init__(self, request, context)
		self.order_func = desc

	def _get_order_field(self):
		return LEK.timestamp

	def _create_empty_model(self):
		return LEK("", "", "", "", "", "", "", "", "", "")
	
	def _get_fields(self):
		return self._create_empty_model().getRequiredFields()
	
	def _get_target_class(self):
		return LEK

	def _dict_to_model(self, data, rec_id=0):
		# If id is 0, we want to create a new entry
		if rec_id == 0:
			src = self._create_empty_model()
		else:
			src = DBSession.query(self._target_class).get(rec_id)

		display_debug("Within LEK Handler")
		display_debug(str(data))
		
		src.userid = data["userid"]
		src.countryid = data["countryid"]
		src.text = data["text"]
		src.filepath = data["filepath"]  # resource/file location
		src.filetype = data["filetype"]
		src.category = data["category"]
		src.latitude = data["latitude"]
		src.longitude = data["longitude"]

		try:
			src.aDate = datetime.fromtimestamp(int(data["aDate"]))
		except Exception, e1:
			log.error("First Invalid date format " + str(e1))
			try:
				print("Attempting alternative format for " + str(data['aDate']))
				src.aDate = datetime.fromtimestamp(int(data["aDate"])/1000)
			except Exception, e:
				log.error("Second Invalid date format " + str(e))
				print("Failed to convert date from:" + str(data['aDate']))
		
		# Optional Fields
		if "isSpecific" in data:
			src.isSpecific = data["isSpecific"]
			display_debug("Display debug was set")

		# Ensure we can accommodate for both numbers and words else default to private
		if "isPublic" in data:
			try:
				src.isPublic = int(data['isPublic'])
			except:
				try:
					if data['isPublic'].lower() is "public":
						src.isPublic = 1
					elif data['isPublic'].lower() is "private":
						src.isPublic = 0
				except Exception:
					src.isPublic = 0
		else:
			display_debug("Audience not set, making public")
			src.isPublic = 1 # public by default

		display_debug("Completed the conversion of the LEK model")

		return src

	# def _handle_conditional_get(self, query):
	# 	query = super(LEKHandler, self)._handle_conditional_get(query)
	#
	# 	# Handle individual user reports
	# 	if 'userid' in self.request.params:
	# 		userid = self.request.params['userid']
	# 		query = query.filter(LEK.userid == userid)
	#
	# 	# Handle visibility
	# 	if self.request.params.has_key('type'):
	# 		is_public = self.request.params['type']
	# 		if is_public == 'public':
	# 			query = query.filter(LEK.isPublic == 1)
	# 		elif is_public == 'all':
	# 			query = query.filter(or_(LEK.isPublic == 1, LEK.isPublic == 0))
	# 		elif is_public == 'private':
	# 			query = query.filter(LEK.isPublic == 0)
	# 	else:
	# 		query = query.filter(LEK.isPublic == 1)
	#
	#
	# 	if self.request.params.has_key("category"):
	# 		category = self.request.params['category']
	# 		query = query.filter(LEK.category  == category)
	#
	# 	return query

	def collection_post(self):
		res = super(LEKHandler, self).collection_post()
		log.debug(res)
		if res:
			if "file" in self.request.POST:
				file_data = {
					"filename": self.request.POST['file'].filename,
					"file": self.request.POST['file'].value,
					"type": self.request.POST['filetype'],
					"userid": self.request.POST['userid']
				}
				location = store_media(file_data)
				# If file successfully
				if location is not "":
					location_str = "static" + location.split('static')[1]
					res['filepath'] = location_str
					src = self._save(res, res['id'])
					if src:
						return src.toJSON()
				self.request.response.status = 400
				res['message'] = "Unable to save file resource"
				# TODO Should the record be rollback or deleted at this point
		return res

@resource(collection_path='/api/lek/user/', path='/api/lek/user/{id}', description="User specific LEK posts")
class LEKUserHandler(LEKHandler):
	def __init__(self, request):
		LEKHandler.__init__(self, request)
	
	def get(self):
		user_id = int(self.request.matchdict['id'])
		query = DBSession.query(self._target_class)
		query = self._handle_conditional_get(query)
		query = query.filter(LEK.userid == user_id)
		res = query.all()
		res = self._process_results(res)
		return res
	
	def collection_get(self):
		return []
	
	def collection_post(self):
		self.request.response.status = 405
		return False
	
	def put(self):
		self.request.response.status = 405
		return False
	
	def delete(self):
		self.request.response.status = 405
		return False


# http://cornice.readthedocs.io/en/latest/resources.html
@resource(collection_path='/api/lek/categories', path='/api/lek/categories/{id}', description="LEK Categories")
class LEKCategoryHandler(BaseHandler):
	def _get_order_field(self):
		return LEKCategory.name
	
	def __init__(self, request, context=None):
		BaseHandler.__init__(self, request, context)
	
	def _create_empty_model(self):
		return LEKCategory(0, "")
	
	def _get_fields(self):
		return self._create_empty_model().getRequiredFields()
	
	def _get_target_class(self):
		return LEKCategory
	
	def _dict_to_model(self, data, rec_id=0):
		# If id is 0, we want to create a new entry
		if rec_id == 0:
			src = self._create_empty_model()
		else:
			src = DBSession.query(self._target_class).get(rec_id)
		# Assign value to the fields of the model
		src.countryid = data["countryid"]
		src.name = data['name']
		if "createdby" in data:
			src.createdby = data['createdby']
		
		return src


# lekservices = Service(name='lekservices', path='api/add/lek', description='add new lek')
# getLek = Service(name='getLek', path='/api/get/lek/{userid}/{type}/{offset}', description='get lek')

# lekService = Service(name='lekService', path='api/lek', description='lek service')

# lekCategoryService = Service(name='lekCategoryService', path='api/lek/categories', description='lek category service')
# lekCategoryIdService = Service(name='lekCategoryIdService', path='api/lek/categories/{id}',description='get lek category service')

# getLekService = Service(name='getLekService', path='api/lek/{userid}/{type}/{category}',
#                         description='get lek near service')
# getLekDateService = Service(name='getLekDateService', path='api/lek/{userid}/{type}/{category}/{day}/{month}/{year}',
#                             description='get lek date service')
# getLekRangeService = Service(name='getLekRangeService',
#                              path='api/lek/{userid}/{type}/{category}/{countryid}/{day1}/{month1}/{year1}/{day2}/{month2}/{year2}',
#                              description='get lek range service')

# @getLek.get()
def get_lek(request):
	if not validate(request):
		return {'status': 401, 'data': []}
	userid = int(request.matchdict['userid'])
	type = request.matchdict['type']
	offset = int(request.matchdict['offset'])
	
	result = getlekOffset(userid, type, offset)
	if result is None:
		return {'status': 204, 'data': []}
	else:
		return {'status': 200, 'data': result}


# @getLekService.get()
def get_lek2(request):
	if not validate(request):
		return {'status': 401, 'data': []}
	userid = int(request.matchdict['userid'])
	type = request.matchdict['type']
	category = request.matchdict['category']
	
	result = getlek(userid, type, category)
	if result is None:
		return {'status': 204, 'data': []}
	else:
		return {'status': 200, 'data': result}


# @getLekDateService.get()
def get_lek_date(request):
	if not validate(request):
		return {'status': 401, 'data': []}
	userid = int(request.matchdict['userid'])
	type = request.matchdict['type']
	category = request.matchdict['category']
	day = request.matchdict['day']
	month = request.matchdict['month']
	year = request.matchdict['year']
	
	result = getlekByDate(userid, type, category, day, month, year)
	if result is None:
		return {'status': 204, 'data': []}
	else:
		return {'status': 200, 'data': result}


# @getLekRangeService.get()
def get_lek_range(request):
	if not validate(request):
		return {'status': 401, 'data': []}
	userid = int(request.matchdict['userid'])
	type = request.matchdict['type']
	category = request.matchdict['category']
	countryid = request.matchdict['countryid']
	
	day1 = request.matchdict['day1']
	month1 = request.matchdict['month1']
	year1 = request.matchdict['year1']
	
	day2 = request.matchdict['day2']
	month2 = request.matchdict['month2']
	year2 = request.matchdict['year2']
	
	date1 = year1 + "-" + month1 + "-" + day1
	date2 = year2 + "-" + month2 + "-" + day2
	
	result = getlekByRange(userid, countryid, type, category, date1, date2)
	if result is None:
		return {'status': 204, 'data': []}
	else:
		return {'status': 200, 'data': result}


# @lekCategoryIdService.get()
def get_lek_categories(request):
	if not validate(request):
		return {'status': 401, 'data': []}
	userid = request.matchdict['id']
	
	result = getlekCategories(userid)
	if result is None:
		return {'status': 204, 'data': []}
	else:
		return {'status': 200, 'data': result}


# @lekCategoryService.post()
def add_lek_category(request):
	if not validate(request):
		return {'status': 401, 'data': []}
	
	category = json.loads(request.body)
	
	new_category = LEKCategory(category['countryid'], category['name'])
	if insertCategory(new_category):
		return {'status': 201}
	else:
		return request.errors.add('url', 'create', 'user not registered')


# @lekCategoryIdService.delete()
def delete_lek_category(request):
	if not validate(request):
		return {'status': 401, 'data': []}
	
	try:
		id = request.matchdict['id']
		DBSession.query(LEKCategory).filter(LEKCategory.id == id).delete()
		transaction.commit()
		return {'status': 200}
	except Exception, e:
		log.error("Error Occurred while deleting lek category: " + e.message)
		return {'status': 500}


# @lekservices.post()
# @lekService.post()
def add_lek(request):
	if not validate(request):
		return {'status': 401, 'data': []}
	
	location = ""
	filetype = request.POST['filetype']
	print "FILE TYPE " + filetype
	
	if "file" in request.POST:
		lek_data = {
			"filename": request.POST['file'].filename,
			"file": request.POST['file'].value,
			"type": filetype,
			"userid": request.POST['userid']
			
		}
		location = store_media(lek_data)
		location = "static/" + location.split('static')[1]
	
	new_lek = LEK(
		request.POST['userid'],
		location,
		filetype,
		request.POST['text'],
		request.POST['latitude'],
		request.POST['longitude'],
		request.POST['isSpecific'],
		request.POST['aDate'],
		request.POST['category'],
		request.POST['countryid']
	)
	value = save_lek(new_lek)
	if value:
		print 'saved'
		return {"status": 201}
	else:
		print 'error'
		return request.errors.add('url', 'create', 'error saving to database')


def save_lek(lek):
	try:
		DBSession.add(lek)
		DBSession.flush()
		transaction.commit()
		return True
	except Exception, e:
		reason = e.message
		print reason
		return False


def insertCategory(category):
	try:
		DBSession.add(category)
		DBSession.flush()
		transaction.commit()
		return True
	except Exception, e:
		reason = e.message
		print reason
		if "Duplicate entry" in reason:
			print "%s already in table." % e.params[1]
			DBSession.rollback()
		return False


def getlekOffset(userid, type, offset):
	filetype = "%" if type == 'all' else type + "%"
	
	stmt = text("SELECT l.id, c.username, l.userid, l.location, l.filetype, l.text,l.latitude, l.longitude, DATE_FORMAT( l.timestamp, '%b %d %Y %h:%i %p') as timestamp \
				from lek l, user u	inner join(select u.countryid, u.username from user u where u.id = :userid) as c \
				on u.countryid = c.countryid where l.filetype like :filetype\
				and u.id = l.userid order by l.timestamp desc limit 10 offset :offset")
	try:
		result = DBSession.execute(stmt, {"userid": userid, "filetype": filetype, "offset": offset})
		arr = []
		for model in result:
			print dict(model)
			arr.append(dict(model))
		return arr
	except Exception, e:
		print e.message
		return None


def getlek(userid, type, category):
	filetype = "%" if type == 'all' else type + "%"
	
	query = "SELECT l.id, c.username, l.userid, l.location, l.filetype, l.text, l.category, l.latitude, l.longitude, DATE_FORMAT( l.timestamp, '%b %d %Y %h:%i %p') as timestamp \
					from lek l, user u	inner join(select u.countryid, u.username from user u where u.id = :userid) as c \
					on u.countryid = c.countryid where l.filetype like :filetype\
					and u.id = l.userid "
	
	if category != "all":
		query += "and l.category = :category "
	
	query += "order by l.timestamp desc"
	
	stmt = text(query)
	
	try:
		result = DBSession.execute(stmt, {"userid": userid, "category": category, "filetype": filetype})
		arr = []
		for model in result:
			print dict(model)
			arr.append(dict(model))
		return arr
	except Exception, e:
		print e.message
		return None


# TODO - Translate to Alchemy syntax - current implementation is mysql specific
def getlekByDate(userid, type, category, day, month, year):
	filetype = "%" if type == 'all' else type + "%"
	
	query = "SELECT l.id, c.username, l.userid, l.location, l.filetype, l.text, l.category, l.latitude, l.longitude, DATE_FORMAT( l.timestamp, '%b %d %Y %h:%i %p') as timestamp \
					from lek l, user u inner join(select u.countryid, u.username from user u where u.id = :userid) as c \
					on u.countryid = c.countryid where l.filetype like :filetype \
					and DAY(l.timestamp) = :day and MONTH(l.timestamp) = :month and YEAR(l.timestamp) = :year "
	
	if category != "all":
		query += "and l.category = :category "
	
	query += "order by l.timestamp desc"
	
	stmt = text(query)
	
	try:
		result = DBSession.execute(stmt, {"userid": userid, "category": category, "filetype": filetype, "day": day,
		                                  "month": month, "year": year})
		arr = []
		for model in result:
			print dict(model)
			arr.append(dict(model))
		return arr
	except Exception, e:
		print e.message
		return None


def getlekByRange(userid, countryid, type, category, date1, date2):
	filetype = "%" if type == 'all' else type + "%"
	
	query = "SELECT l.id, c.username, l.userid, l.location, l.filetype, l.text, l.category, l.latitude, l.longitude, DATE_FORMAT( l.timestamp, '%b %d %Y %h:%i %p') as timestamp \
					from lek l, user u inner join(select u.countryid, u.username from user u where u.id = :userid) as c \
					on u.countryid = c.countryid where l.filetype like :filetype and l.timestamp between :date1 and :date2"
	
	if category != "all":
		query += " and l.category = :category"
	
	if countryid != "default":
		query += " and l.countryid = :countryid"
	
	query += " order by l.timestamp desc"
	
	stmt = text(query)
	
	try:
		result = DBSession.execute(stmt, {"userid": userid, "countryid": countryid, "category": category,
		                                  "filetype": filetype, "date1": date1, "date2": date2})
		arr = []
		for model in result:
			print dict(model)
			arr.append(dict(model))
		return arr
	except Exception, e:
		print e.message
		return None


def getlekCategories(userid):
	query = "SELECT l.id, l.countryid, c.name as 'countryname', l.name from lekcategories l inner join country as c on l.countryid = c.id"
	
	if userid != "all":
		query += " WHERE l.countryid IN (select u.countryid from user u where u.id = :userid)"
	
	stmt = text(query)
	
	try:
		result = DBSession.execute(stmt, {"userid": userid})
		arr = []
		for model in result:
			print dict(model)
			arr.append(dict(model))
		return arr
	except Exception, e:
		print e.message
		return None
