from sqlalchemy import (
    Column,
    Integer,
    String,
    ForeignKey,
    DateTime, func,
    UniqueConstraint,
    event
)
from sqlalchemy.orm import relationship
from mfisheries.models import Base, DBSession
import transaction
from mfisheries.core import Country


class LEKCategory(Base):
    """The SQLAlchemy declarative model class for a Module object"""
    __tablename__ = 'lekcategories'
    id = Column(Integer, primary_key=True)
    countryid = Column(Integer, ForeignKey('country.id'), nullable=False)
    name = Column(String(100))
    timecreated = Column(DateTime, nullable=False, server_default=func.now())
    timemodified = Column(DateTime, server_default=func.now(), onupdate=func.now())
    createdby = Column(Integer)

    country = relationship("Country", foreign_keys=[countryid])
    

    # http://docs.sqlalchemy.org/en/latest/core/constraints.html#sqlalchemy.schema.UniqueConstraint
    __table_args__ = (UniqueConstraint('countryid', 'name', name='uixcm_1'),)

    def __init__(self, countryid, name):
        self.countryid = countryid
        self.name = name

    def getRequiredFields(self):
        return ['countryid', 'name']


    def toJSON(self):
        return {
            'id': self.id,
            'countryid': self.countryid,
            'name': self.name,
            "country": self.country.name,
            "timecreated": str(self.timecreated),
            "timemodified": str(self.timemodified),
            "createdby" : str(self.createdby)
        }


def create_default_categories(target, connection_rec, **kw):
    print("Creating Default LEK Categories")

    categories = [
        "Land",
        "Coast line"
        "Open Seas",
        "River",
        "Weather",
        "Other"
    ]
    try:
        countries = DBSession.query(Country).all()
        for country in countries:
            for cat_str in categories:
                category = LEKCategory(country.id, cat_str)
                DBSession.add(category)
        DBSession.flush()
        transaction.commit()
    except Exception as e:
        print("Error: Inserting LEK Categories Failed: %s" % e.message)
        DBSession.rollback()


event.listen(Country.__table__, 'after_create', create_default_categories)