import json
import logging

import transaction
from cornice import Service
from sqlalchemy import text

log = logging.getLogger(__name__)

from mfisheries.models import DBSession
from mfisheries.modules.sos import ResolvedSos
from mfisheries.modules.tracks import TrackPoints
from mfisheries.core.user import User
from mfisheries.util.secure import validate
from mfisheries.modules.sos.sosclient import send_sos_notification
from mfisheries.util.fileutil import (
    add_data,
    read,
)

addTracks = Service(name='addTracks', path='api/add/user/tracks', description='store user track information')
getTrackDates = Service(name="getTrackDates", path="api/get/track/dates/{cgid}",
                        description="get all unique dates from track table")
getTracksByDay = Service(name="getTracksByDay", path='api/get/track/by/day/{day}/{month}/{year}/{cgid}',
                         description='gets tracks for a given day')
getUserTracks = Service(name="getUserTracks",
                        path="api/get/user/tracks/{day}/{month}/{year}/{userid}/{starttrackid}/{cgid}",
                        description="returns all tracks for a user")
resolveSOS = Service(name="resolveSOS", path="/api/update/sos/status",
                     description="updates the status of sos to resolved")

trackService = Service(name='trackService', path='api/tracks', description='track service')
trackDateService = Service(name="trackDateService", path="api/tracks/dates/{cgid}",
                           description="get all unique dates from track table")
tracksByDayService = Service(name="tracksByDayService", path='api/tracks/day/{day}/{month}/{year}/{cgid}',
                             description='gets tracks for a given day')
userTrackService = Service(name="userTrackService",
                           path="api/tracks/user/{day}/{month}/{year}/{userid}/{starttrackid}/{cgid}",
                           description="returns all tracks for a user")
SOSService = Service(name="SOSService", path="/api/sos",
                     description="updates the status of sos to resolved")


@addTracks.post()
@trackService.post()
def add_user_tracks(request):
    if not validate(request):
        return {'status': 401, 'data': []}
    tracks = json.loads(request.body)
    tracks_models = []
    if tracks is not None:
        # Convert from webservice (JSON) to system model format
        for track in tracks:
            trackpoint = TrackPoints(
                track['uId'],
                # track['starttrackid'],
                track['lat'],
                track['lng'],
                track['rssi'],
                track['acc'],
                track['brg'],
                track['spd'],
                track['prov'],
                track['time'],
                track['type'].lower()
            )
            # Add Each track to a list of tracks
            tracks_models.append(trackpoint)
        # Store the
        value = store_tracks(tracks_models)
        if value:
            log.info("Successfully processed {0} tracks from the server".format(len(tracks_models)))
            return {'status': 201}
        else:
            log.error("Unable to save  records to the system")
            request.errors.add('url', 'create', 'tracks not saved')
    else:
        log.error("Bad request received, no records saved")
        request.errors.add('url', 'create', 'received empty list')


@getTracksByDay.get()
@tracksByDayService.get()
def get_tracks_by_day(request):
    if not validate(request):
        return {'status': 401, 'data': []}
    req = {
        "day": request.matchdict['day'],
        "month": request.matchdict['month'],
        "year": request.matchdict['year'],
        "cgid": request.matchdict['cgid']
    }

    result = gettrackbyday(req)
    if not result:
        return {'status': 204, 'data': []}
    else:
        return {'status': 200, 'data': result}


@getUserTracks.get()
@userTrackService.get()
def get_user_tracks(request):
    if not validate(request):
        return {'status': 401, 'data': []}
    req = {
        "day": request.matchdict['day'],
        "month": request.matchdict['month'],
        "year": request.matchdict['year'],
        "userid": request.matchdict['userid'],
        "starttrackid": request.matchdict['starttrackid'],
        "cgid": request.matchdict['cgid']
    }

    result = getusertracks(req)
    if not result:
        return {'status': 204, 'data': []}
    else:
        return {'status': 200, 'data': result}


@getTrackDates.get()
@trackDateService.get()
def get_track_dates(request):
    if not validate(request):
        return {'status': 401, 'data': []}
    dates = get_dates(request.matchdict['cgid'])
    if not dates:
        return {'status': 204, 'data': []}
    else:
        return {'status': 200, 'data': dates}


@resolveSOS.post()
@SOSService.put()
def resolve_sos(request):
    if not validate(request):
        return {'status': 401, 'data': []}
    data = json.loads(request.body)
    resolve = ResolvedSos(data['caseno'],
                          data['details'],
                          None,
                          data['trackid'],
                          data['badgeno'])
    if resolvesos(resolve):
        return {'status': 200}
    else:
        return request.errors.add('url', 'update', 'unable to resolve sos')


# TODO - Translate to Alchemy syntax - current implementation is mysql specific
# api/get/user/tracks/27/10/2015/42/254
def getusertracks(data):
    stmt = text("SELECT t1.id,t1.userid, t1.latitude, t1.longitude, DAY( t1.deviceTimestamp ) AS day, t1.starttrackid, \
					MONTH( t1.deviceTimestamp ) AS month, \
					t1.type, t1.bearing, u.fname, u.lname, t1.speed, DATE_FORMAT( t1.deviceTimestamp, '%Y-%m-%d %H:%i:%s') as time \
					FROM trackpoints t1, user u \
					inner JOIN(select u1.countryid from user u1 where u1.id = :cgid) as c\
					ON c.countryid = u.countryid\
					WHERE u.id = t1.userid \
					AND t1.userid = :userid \
					AND DAY( t1.deviceTimestamp ) <= :day \
					AND MONTH(t1.deviceTimestamp) <= :month \
					AND YEAR(t1.deviceTimestamp) <= :year \
					AND (t1.starttrackid = :starttrackid \
					OR t1.id = :starttrackid) \
					order by time asc")
    try:
        result = DBSession.execute(stmt, data)
        arr = []
        for model in result:
            arr.append(dict(model))
        return arr
    except Exception, e:
        print e.message
        return None


def gettrackbyday(data):
    stmt = text("SELECT distinct t1.userid, t1.latitude, t1.longitude, DAY( t1.deviceTimestamp ) AS day, \
					t1.starttrackid, MONTH( t1.deviceTimestamp ) AS month, YEAR( t1.deviceTimestamp ) AS year,\
					t1.type, t1.bearing, u.fname, u.lname, u.mobileNum, u.homeNum, t1.speed, DATE_FORMAT( t1.deviceTimestamp, '%Y-%m-%d %H:%i:%s') as time \
					FROM trackpoints t1 \
					inner JOIN user u \
					ON u.id = t1.userid \
					inner JOIN (SELECT max(t2.deviceTimestamp) max_time, t2.starttrackid \
					      FROM trackpoints t2 \
					      where day(t2.deviceTimeStamp) = :day \
						  and month(t2.deviceTimeStamp) = :month \
					      group by t2.starttrackid) AS tr \
					ON tr.starttrackid = t1.starttrackid \
					AND tr.max_time = t1.deviceTimeStamp \
					inner JOIN(select u1.countryid from user u1 where u1.id = :cgid) as c\
					ON c.countryid = u.countryid")
    try:
        result = DBSession.execute(stmt, data)
        arr = []
        for model in result:
            arr.append(dict(model))
        # print arr
        sos_tracks = getsostracks(data['cgid'])
        if sos_tracks is not None:
            arr += sos_tracks  # Add Both list into one list stored in the variable arr
        return arr
    except Exception, e:
        print e.message
        return None


def getsostracks(cgid):
    stmt = text("SELECT distinct t1.id, t1.userid, t1.latitude, t1.longitude, DAY( t1.deviceTimestamp ) AS day, t1.starttrackid, \
					MONTH( t1.deviceTimestamp ) AS month, YEAR( t1.deviceTimestamp ) AS year, \
					t1.type, t1.bearing, u.fname, u.lname, u.mobileNum, u.homeNum,  t1.speed, DATE_FORMAT( t1.deviceTimestamp, '%Y-%m-%d %H:%i:%s') as time \
					FROM trackpoints t1, user u\
					inner JOIN(select u1.countryid from user u1 where u1.id = :cgid) as c\
					ON c.countryid = u.countryid\
					WHERE u.id = t1.userid \
					AND t1.type = 'sos'")
    try:
        result = DBSession.execute(stmt, {"cgid": cgid})
        arr = []
        for model in result:
            print dict(model)
            arr.append(dict(model))
        return arr
    except Exception, e:
        print e.message
        return None


def updateSos(trackid):
    try:
        track = DBSession.query(TrackPoints).filter(TrackPoints.id == trackid).one()
        track.type = 'resolved'
        return True
    except Exception, e:
        print e.message
        return False


def resolvesos(data):
    try:
        if updateSos(data.trackid):
            DBSession.add(data)
            return True
        else:
            return False
    except Exception, e:
        print e.message
        return False


def get_dates(cgid):
    # Check user id to determine the type of user
    # if coast guard (i.e. type == 1) then provide all users
    # otherwise provide tracks for only specific user
    try:
        usr = DBSession.query(User).filter(
            User.id == cgid).one()  # http://docs.sqlalchemy.org/en/latest/orm/query.html#sqlalchemy.orm.query.Query.one
        if usr._class == 1:
            log.info("Received request from the coast guard with id " + cgid)
            stmt = text("SELECT distinct DATE_FORMAT(deviceTimestamp, '%Y-%m-%d') as start\
 				FROM trackpoints, user u\
                inner join(select u.countryid from user u where u.id = :cgid)\
			 	as c on u.countryid = c.countryid where u.id = trackpoints.userid")
        else:
            log.info("retrieve request from general user with id " + cgid)
            stmt = text("SELECT distinct DATE_FORMAT(deviceTimestamp, '%Y-%m-%d') as start\
			FROM trackpoints t, user u\
			WHERE u.id = t.userid\
			AND u.id = :cgid")

        result = DBSession.execute(stmt, {"cgid": cgid})
        arr = []
        for model in result:
            obj = dict(model)
            obj['title'] = "Activity"
            arr.append(obj)
        log.info("Found {0} tracks for the user {1}".format(len(arr), cgid))
        return arr
    except Exception, e:
        print e.message
        return None


def store_track(data):
    try:
        log.info("Received id {0} of {1} track for user {2} with value of ({3},{4})".format(data.id, data.type, data.userid, data.latitude, data.longitude))
        # Attempt to save track to the database
        DBSession.add(data)
        DBSession.flush()  # flushing required to commit pending transactions
        if data.type == 'sos':
            send_sos_notification(data.id, data.userid)
        if data.type == 'start':
            data.starttrackid = data.id
        else:
            data.starttrackid = get_last_start_track_id(data.userid)

        log.info("Received {0} as the starting track id".format(data.starttrackid))
        transaction.commit()
        return True
    except Exception, e:
        try:
            DBSession.rollback()
            transaction.abort()
            reason = str(e)
            log.error("Failed to save {0} track because of {1}".format(data.type, reason))
            if "Duplicate entry" in reason:
                print "%s already in table." % e.params[0]
                # Write the record to the duplicates.json file
                add_data({
                    "userid": data.userid,
                    "latitude": round(float(data.latitude), 6),
                    "longitude": round(float(data.longitude), 6),
                    "deviceTimestamp": data.deviceTimestamp
                })
        except Exception, e2:
            log.error("Error Occurred when handling track storage error: " + str(e2))
        return False


def store_tracks(data):
    try:
        log.info("Retrieved {0} tracks".format(len(data)))
        for track in data:
            reviewed = store_track(track)
            if not reviewed:
                log.error("Did not save track for " + str(track.toJSON()))
                return False
        else:
            return True

    except Exception, e:
        log.error("Unable to store tracks: " + str(e))
        return False


def update_duplicate_tracks():
    try:
        data = read()
        for rec in data:
            r = DBSession.query(TrackPoints).filter(TrackPoints.userid == rec['userid'],
                                                    TrackPoints.latitude == rec['latitude'],
                                                    TrackPoints.longitude == rec['longitude'],
                                                    TrackPoints.deviceTimestamp == rec['deviceTimestamp']).one()
            r.duplicated = "yes"
            r.duplicatecount = r.duplicatecount + 1
        return True
    except Exception, e:
        print e.message
        return False


def get_last_start_track_id(userid):
    stmt = text("SELECT t.starttrackid from trackpoints t inner join \
			(select max(t1.deviceTimestamp) as m,t1.starttrackid, t1.userid \
			from trackpoints t1 where t1.userid = :userid and t1.starttrackid is not null and t1.type = 'start') as tem on \
			t.userid = tem.userid and tem.m = t.deviceTimestamp")
    try:
        result = DBSession.execute(stmt, {"userid": userid})
        return dict(result.fetchone())['starttrackid']
    except Exception, e:
        print e.message
        return None
