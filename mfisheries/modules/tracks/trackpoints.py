from sqlalchemy import (
    Column,
    Integer,
    String,
    Float,
    TIMESTAMP,
    ForeignKey,
    UniqueConstraint
)
import datetime
from mfisheries.models import Base


class TrackPoints(Base):
    """The SQLAlchemy declarative model class for a TrackPoints object"""
    __tablename__ = 'trackpoints'
    id = Column(Integer, primary_key=True)
    userid = Column(Integer, ForeignKey('user.id'), nullable=False)
    starttrackid = Column(Integer)
    latitude = Column(String(20))
    longitude = Column(String(20))
    rssi = Column(String(100))
    altitude = Column(Float)
    accuracy = Column(Float)
    bearing = Column(Float)
    speed = Column(Float)
    mockprovider = Column(Float)
    deviceTimestamp = Column(TIMESTAMP)
    timestamp = Column(TIMESTAMP, default=datetime.datetime.utcnow)
    type = Column(String(20))
    duplicated = Column(String(3))
    duplicatecount = Column(Integer)
    __table_args__ = (UniqueConstraint('userid', 'latitude', 'longitude', 'deviceTimestamp', name='user_location'),)

    def __init__(self, userid, latitude, longitude, rssi, accuracy, bearing, speed, mockprovider, deviceTimestamp,
                 type):
        self.userid = userid
        self.starttrackid = 0
        self.latitude = latitude
        self.longitude = longitude
        self.rssi = rssi
        self.accuracy = accuracy
        self.bearing = bearing
        self.speed = speed
        self.mockprovider = mockprovider
        self.deviceTimestamp = deviceTimestamp
        self.type = type
        self.duplicated = "no"
        self.duplicatecount = 0

    def toJSON(self):
        json = {}
        json['id'] = self.id
        json['userid'] = self.userid
        json['starttrackid'] = self.starttrackid
        json['latitude'] = self.latitude
        json['longitude'] = self.longitude
        json['rssi'] = self.rssi
        json['altitude'] = self.altitude
        json['accuracy'] = self.accuracy
        json['bearing'] = self.bearing
        json['speed'] = self.speed
        json['mockprovider'] = self.mockprovider
        json['deviceTimestamp'] = str(self.deviceTimestamp)
        json['timestamp'] = str(self.timestamp)
        json['type'] = self.type

        return json
