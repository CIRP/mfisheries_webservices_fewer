# This file is used for testing purposes and is ran directly using python -m mfisheries.tests

# class TestMyViewSuccessCondition(unittest.TestCase):
#     def setUp(self):
#         self.config = testing.setUp()
#         from sqlalchemy import create_engine
#         engine = create_engine('sqlite://')
#         from mfisheries.models import (
#             Base
#             )
#         DBSession.configure(bind=engine)
#         Base.metadata.create_all(engine)
#
#     def tearDown(self):
#         DBSession.remove()
#         testing.tearDown()
#
#     def test_passing_view(self):
#         from mfisheries.views import my_view
#         request = testing.DummyRequest()
#         info = my_view(request)
#         self.assertEqual(info['one'].name, 'one')
#         self.assertEqual(info['project'], 'mfisheries')
#
#
# class TestMyViewFailureCondition(unittest.TestCase):
#     def setUp(self):
#         self.config = testing.setUp()
#         from sqlalchemy import create_engine
#         engine = create_engine('sqlite://')
#         DBSession.configure(bind=engine)
#
#     def tearDown(self):
#         DBSession.remove()
#         testing.tearDown()
#
#     def test_failing_view(self):
#         from mfisheries.views import my_view
#         request = testing.DummyRequest()
#         info = my_view(request)
#         self.assertEqual(info.status_int, 500)
#
# def test_kml(filename = None):
#     # country = DBSession.query(Country).get(1)
#     if filename is None:
#         filename = "TobagoVillages.kml"
#     file_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), filename)
#     parts = filename.split(".")
#     print parts[-1]
#     if parts[-1] == "csv":
#         with open(file_path, 'r') as csvfile:
#             reader = csv.DictReader(csvfile,delimiter=',')
#             if 'latitude' in reader.fieldnames and 'longitude' in reader.fieldnames and 'village' in reader.fieldnames:
#                 for row in reader:
#                     DBSession.add(Village(row['village'], row['latitude'], row['longitude'], 1))
#     elif parts[-1] == "kml":
#         print "KML File detected"
#         with open(file_path, 'r') as kmlfile:
#             dataStr = kmlfile.read()
#             k = kml.KML()
#             k.from_string(dataStr)
#             document = list(k.features())
#             print document[0].name
#             placemarks = list(document[0].features())
#             print "Document contains {} features".format(len(placemarks))
#             for placemark in placemarks:
#                 print "Location called {} is located at ({}, {}) ".format(placemark.name, placemark.geometry.x, placemark.geometry.y)
#                 DBSession.add(placemark.name, placemark.geometry.y,placemark.geometry.x, 1 )
#     else:
#         print "Unsupported extension"
#
#
# if __name__ == '__main__':
#     test_kml()
