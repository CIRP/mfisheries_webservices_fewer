import json
import unittest

import transaction
from pyramid.paster import get_appsettings
from webtest import TestApp
from zope.sqlalchemy import register

from mfisheries import main, APP_ROOT
from ..models import DBSession
from mfisheries.modules.weather import get_all_package_names


class ExtractorTests(unittest.TestCase):
	def setUp(self):
		settings = get_appsettings('mfisheries/tests/test.ini', name='main')
		self.testapp = TestApp(main(global_config=None, **settings))
		
		fp = open('{0}/API_KEY.json'.format(APP_ROOT))
		data = json.load(fp)
		self.header = {"apikey": str(data[0])}
		self.countries = ['Trinidad', 'Tobago']
		from mfisheries.scripts.initializedb import install_database_tables
		# install_database_tables(settings={'sqlalchemy.url': 'sqlite:///test.db'})
		# register(DBSession, transaction_manager=transaction.manager)
		
	def test_get_extractor_files(self):
		packages = get_all_package_names("extract_test")
		self.assertGreater(len(packages), 0)
		return packages

	def _getExtractors(self):
		packages = self.test_get_extractor_files()
		extractors = []
		for p in packages:
			if p != "__init__" and len(p) > 3 and p != "TemplateWeather" and p != "TemplateTides":
				extractors.append(p)
		return extractors

	def test_all_file_extractors(self):
		from mfisheries.modules.weather.parsers.WeatherSourceExtractor import WeatherSourceExtractor
		from mfisheries.modules.weather.parsers.extract_test import *

		extractors = self._getExtractors()
		for p in extractors:
			if "Tides" not in p:
				call = eval(p+".Extractor()")
				self.assertTrue( isinstance(call, WeatherSourceExtractor), "Type {0} is not a weather extractor".format(p))
				reading_types = call.get_reading_types()
				self.assertGreater(len(reading_types.keys()), 0, "Extractor for {0} did not provide readings".format(p))
