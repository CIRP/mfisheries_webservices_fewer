import json
import os
import unittest

import transaction
from pyramid.paster import get_appsettings
from webtest import TestApp
from zope.sqlalchemy import register

from mfisheries import APP_ROOT
from mfisheries import main
from ..models import DBSession

dir_path = os.path.dirname(os.path.realpath(__file__))


class APIResources(unittest.TestCase):
	def setUp(self):
		self.test_db_name = "test.db"
		# self.delete_database()
		settings = get_appsettings('mfisheries/tests/test.ini', name='main')
		self.testapp = TestApp(main(global_config=None, **settings))
		
		fp = open('{0}/API_KEY.json'.format(APP_ROOT))
		data = json.load(fp)
		self.header = {
			"apikey": str(data[0])
		}
		self.countries = ['Trinidad', 'Tobago']
		from mfisheries.scripts.initializedb import install_database_tables
		engine, res, e = install_database_tables(settings={
			'sqlalchemy.url': 'sqlite:///test.db'
		})
		register(DBSession, transaction_manager=transaction.manager)
		self.session = DBSession
	
	def delete_database(self):
		# Delete Existing
		if os.path.exists("./{0}".format(self.test_db_name)):
			print("Found database attempting to delete")
			os.remove(self.test_db_name)
		else:
			print("database was not found.. skipping")
	
	def tearDown(self):
		self.delete_database()
		transaction.abort()
		self.session = None
	
	def __check_create(self, url, data):
		data_json = json.dumps(data)
		print("Attempting to send data: {0} to {1}".format(data_json, url) )
		res = self.testapp.post(url, params=data_json, headers=self.header)
		self.assertEqual(res.status_int, 201, "Status was found to be: {0}".format(res.status_int))
		rec = json.loads(res.body)
		self.assertGreater(rec['id'], 0, "Found id to be {0}".format(rec['id']))
		return rec
	
	def __check_getAll(self, url):
		res = self.testapp.get(url, headers=self.header)
		print(url)
		self.assertEqual(res.status_int, 200, "Status was found to be: {0}".format(res.status_int))
		rec = json.loads(res.body)
		print(rec)
		self.assertGreater(len(rec), 0, "Found number of records to be {0}".format(len(rec)))
	
	def __check_get(self, url, rec_id, data, key):
		rec_url = url + "/" + str(rec_id)
		res = self.testapp.get(rec_url, headers=self.header)
		self.assertEqual(res.status_int, 200, "Status was found to be: {0}".format(res.status_int))
		rec = json.loads(res.body)
		self.assertEqual(rec[key], data[key], "{0} found {1}".format(data[key], rec[key]))
	
	def __check_country(self, base_url, data):
		self.assertTrue("countryid" in data, "Country ID was not inserted so test invalid")
		countryid = data['countryid']
		# Check if Find the record at the expected country
		rec_url = "{0}?countryid={1}".format(base_url, countryid)
		res = self.testapp.get(rec_url, headers=self.header)
		self.assertEqual(res.status_int, 200, "Status was found to be: {0}".format(res.status_int))
		rec = json.loads(res.body)
		self.assertGreaterEqual(len(rec), 1, "Did not retrieve record just inserted")
		# Ensure that record is not at another country
		rec_url = "{0}?countryid={1}".format(base_url, -1)
		res = self.testapp.get(rec_url, headers=self.header)
		self.assertEqual(res.status_int, 200, "Status was found to be: {0}".format(res.status_int))
		rec = json.loads(res.body)
		self.assertEqual(len(rec), 0, "Received {0} when expected 0".format(len(rec)))
	
	def __run_update(self, url, rec_id, data, key):
		rec_url = url + "/" + str(rec_id)
		data_json = json.dumps(data)
		res = self.testapp.put(rec_url, params=data_json, headers=self.header)
		self.assertEqual(res.status_int, 200, "Status was found to be: {0}".format(res.status_int))
		rec = json.loads(res.body)
		self.assertEqual(rec[key], data[key], "{0} found {1}".format(data[key], rec[key]))
		self.assertEqual(rec['id'], rec_id,"{0} found {1}".format(rec['id'], rec_id) )
	
	def __check_get_and_save(self, base_url, data_file, additional=None, test_country=False, data_type="path"):
		res = self.testapp.get(base_url, status=200)
		self.assertEqual(res.status_int, 200)
		data = {}
		if data_type is "path":
			with open(dir_path + data_file) as fp:
				data = json.load(fp)
				fp.close()
		elif data_type is "data":
			data = data_file
		# Adding or over-ridding additional values
		if additional:
			for k in additional:
				data[k] = additional[k]
		# Test saving a new record
		stored_rec = self.__check_create(base_url, data)
		# Testing retrieving records
		self.__check_getAll(base_url)
		if test_country:
			self.__check_country(base_url, data)
		return stored_rec, data
		
	def __check_update(self, base_url, stored_rec, data, key):
		data['id'] = stored_rec['id']
		self.__run_update(base_url, stored_rec['id'], data, key)
		self.__check_get(base_url, stored_rec['id'], data, key)
		return stored_rec['id']
		
	def __check_delete(self, base_url, rec_id):
		self.testapp.delete(base_url+"/"+str(rec_id), status=200)
		self.testapp.get(base_url+"/"+str(rec_id), status=404)
		
	# Testing Methods
	def test_cap_sources(self):
		base_url = "/api/capsources"
		stored_rec, data = self.__check_get_and_save(base_url, "/data/cap_source.json")
		# Testing Update
		self.assertGreater(stored_rec['id'], 0, "Did not received save CAP Source")
		data['name'] = "Test_updated"
		rec_id = self.__check_update(base_url, stored_rec, data, "name")
		self.__check_delete(base_url, rec_id)

	def test_damage_report(self):
		base_url = "/api/damagereport"
		stored_rec, data = self.__check_get_and_save(base_url, "/data/report/damage_report_data.json")
		self.assertGreater(stored_rec['id'], 0, "Did not received saved Damage Report")
		# Testing Update
		data['name'] = "Test_updated"
		rec_id = self.__check_update(base_url, stored_rec, data, "name")
		self.__check_delete(base_url, rec_id)

	def test_damage_report_category(self):
		base_url = "/api/damagereport/category"
		options = {"countryid": 1}
		stored_rec, data = self.__check_get_and_save(
			base_url,
			"/data/report/damage_report_categories.json",
			options,
			True )
		self.assertGreater(stored_rec['id'], 0, "Did not received saved Damage Report Category")
		# Testing Update
		data['name'] = "Test_updated"
		rec_id = self.__check_update(base_url, stored_rec, data, "name")
		self.__check_delete(base_url, rec_id)

	def test_groups(self, to_delete=True):
		base_url = "/api/groups"
		stored_rec, data = self.__check_get_and_save(base_url, "/data/alerts/group.json")
		# Testing Update
		if stored_rec and stored_rec['id'] > 0:
			data['groupname'] = "Test_updated"
			rec_id = self.__check_update(base_url, stored_rec, data, "groupname")
			if to_delete:
				self.__check_delete(base_url, rec_id)

		return stored_rec

	def test_alert(self):
		base_url = "/api/alerts"
		self.test_groups()
		stored_rec, data = self.__check_get_and_save(base_url, "/data/alerts/alert.json")
		# Testing Update
		if stored_rec and stored_rec['id'] > 0:
			data['messagecontent'] = "Test_updated"
			rec_id = self.__check_update(base_url, stored_rec, data, "messagecontent")
			self.__check_delete(base_url, rec_id)

	def test_config(self):
		base_url = "/api/config"
		stored_rec, data = self.__check_get_and_save(base_url, "/data/core/config.json")
		# Testing Update
		if stored_rec and stored_rec['id'] > 0:
			data['value'] = "Test_updated"
			rec_id = self.__check_update(base_url, stored_rec, data, "value")
			self.__check_delete(base_url, rec_id)
			
	def test_occupation(self):
		base_url = "/api/occupations"
		stored_rec, data = self.__check_get_and_save(base_url, "/data/core/occupation.json", test_country=True)
		# Testing Update
		if stored_rec and stored_rec['id'] > 0:
			data['type'] = "Test_updated"
			rec_id = self.__check_update(base_url, stored_rec, data, "type")
			self.__check_delete(base_url, rec_id)
	
	def test_country_loc(self):
		base_url="/api/countrylocs"
		# Attempting to add a new country to get a 7th country
		from mfisheries.core.country import Country
		import transaction
		c = Country("", "TestCountry")
		self.session.add(c)
		transaction.commit()
		options = {"countryid": c.id}
		stored_rec, data = self.__check_get_and_save(base_url, "/data/core/test_country_loc.json", options, True)
		# Testing Update
		self.assertGreater(stored_rec['id'], 0, "Did not received saved Country Location")
		data['latitude'] = "126790"
		rec_id = self.__check_update(base_url, stored_rec, data, "latitude")
		self.__check_delete(base_url, rec_id)
	
	def test_emergency_contact(self):
		base_url = "/api/emergencycontact"
		stored_rec, data = self.__check_get_and_save(base_url, "/data/emergency_contact_data.json")
		# Testing Update
		self.assertGreater(stored_rec['id'], 0, "Did not received saved Emergency Contacts")
		data['type'] = "individual"
		rec_id = self.__check_update(base_url, stored_rec, data, "type")
		self.assertGreater(len(data['additional']), 0, "Did not save additional details as expected")
		self.__check_delete(base_url, rec_id)

	def test_lek(self):
		base_url = "/api/lek"
		stored_rec, data = self.__check_get_and_save(base_url, "/data/lek/lek.json")
		self.assertGreater(stored_rec['id'], 0, "Did not received saved LEK")
		# Testing Update
		data['text'] = "Test_updated"
		rec_id = self.__check_update(base_url, stored_rec, data, "text")
		self.__check_delete(base_url, rec_id)
		
		# TODO Add additional test operations required by LEK module
		from mfisheries.core.country import Country
		import transaction
		c = Country("", "TestCountry")
		self.session.add(c)
		transaction.commit()
		self.assertGreater(c.id, 0, "Unable to save new Country")
		options = {"countryid": c.id}
		stored_rec, data = self.__check_get_and_save(base_url, "/data/lek/lek.json", options, True)
		self.assertGreater(stored_rec['id'], 0, "Did not received saved LEK")
		
	def test_countries(self):
		# Base Tests
		base_url="/api/countries"
		stored_rec, data = self.__check_get_and_save(base_url, "/data/core/test_country.json")
		# Testing Update
		self.assertGreater(stored_rec['id'], 0, "Did not received saved Module")
		data['name'] = "test description updated"
		rec_id = self.__check_update(base_url, stored_rec, data, "name")
		# Testing delete
		self.__check_delete(base_url, rec_id)
		
		# Test uses by the application
	
	def test_module(self, to_delete=True):
		base_url="/api/modules"
		stored_rec, data = self.__check_get_and_save(base_url, "/data/module/module.json")
		# Testing Update
		self.assertGreater(stored_rec['id'], 0, "Did not received saved Module")
		data['description'] = "test description updated"
		rec_id = self.__check_update(base_url, stored_rec, data, "description")
		if to_delete:
			self.__check_delete(base_url, rec_id)
			return None
		else:
			return stored_rec
		
	# TODO Basic works, but need to consider the specific uses of the country module and test for these
	def test_country_module(self):
		base_url = "/api/country/modules"
		from mfisheries.core.country import Country
		file_path = "{0}{1}".format(dir_path,"/data/module/countrymodule.json")
		with open(file_path) as fp:
			data = json.load(fp)
			fp.close()
		countries = self.session.query(Country).all()
		self.assertIsNotNone(countries, "Did not receive countries")
		self.assertGreater(len(countries), 0, "Did not receive Countries")
		t_module = self.test_module(False)
		self.assertIsNotNone(t_module, "Did not receive module")
		for country in countries:
			data['moduleid'] = t_module['id']
			data['countryid'] = country.id
			stored_rec, raw_data = self.__check_get_and_save(base_url, data,test_country=True, data_type="data")
			self.assertGreater(stored_rec['id'], 0, "Did not received saved Country Module")
			raw_data['path'] = "/mfisheries/test"
			rec_id = self.__check_update(base_url, stored_rec, raw_data, "path")
			self.__check_delete(base_url, rec_id)

	def test_ms_reports(self):
		base_url = "/api/missingpersons"
		stored_rec, data_rec = self.__check_get_and_save(base_url, "/data/fewer/missing.json")
		# # Testing Update
		self.assertGreater(stored_rec['id'], 0, "Did not received saved Missing Persons")
		data_rec['description'] = "test description updated"
		rec_id = self.__check_update(base_url, stored_rec, data_rec, "description")

		field_url = base_url +"/fields"
		fields = ["Gender", "Height", "Ethnicity", "Skin Colour", "Area"]
		fieldRecs = []
		for fld in fields:
			rec = {"name" : fld}
			fld_stored_rec, fld_data = self.__check_get_and_save(field_url,rec, data_type="data")
			self.assertGreater(fld_stored_rec['id'], 0, "Did not received saved field")
			fieldRecs.append(fld_stored_rec)

		from mfisheries.core.user import User
		allUsers = self.session.query(User).all()
		with open(dir_path + "/data/fewer/missing.json") as fp:
			data = json.load(fp)
			fp.close()

		msReportRecs = []
		for user in allUsers:
			in_rec = data
			in_rec['title'] = user.username
			in_rec['userid'] = user.id
			print(in_rec)
			msr_stored_rec, msr_data = self.__check_get_and_save(base_url,in_rec, data_type="data")
			self.assertGreater(stored_rec['id'], 0, "Did not received saved Missing Persons")
			msReportRecs.append(msr_stored_rec)

		self.__check_delete(base_url, rec_id)

