import os
import unittest

import transaction
from pyramid import testing
from pyramid.paster import get_appsettings
from zope.sqlalchemy import register

from ..models import DBSession


# Based on the guide from: http://docs.pylonsproject.org/projects/pyramid/en/latest/tutorials/wiki2/tests.html


class ModelsTests(unittest.TestCase):
    def delete_database(self):
        # Delete Existing
        if os.path.exists("./{0}".format(self.test_db_name)):
            print("Found database attempting to delete")
            os.remove(self.test_db_name)
        else:
            print("database was not found.. skipping")

    def setUp(self):
        self.test_db_name = "test.db"

        # Listing of All expected tables
        self.tables = [
            # Meta related tables
            'configs',
            # core mfisheries
            'alternativecontact',
            'catchdetails',
            'country',
            'countrymodule',
            'countrylocations',
            'hulltype',
            'lek',
            'lekcategories',
            'messages',
            'module',
            'occupation',
            'occupations',
            'phonedetails',
            'resolvesos',
            'sale',
            'subscribers',
            'trackpoints',
            'tripheaders',
            'trippoints',
            'user',
            'usergroups',
            'userimages',
            'usermodule',
            'vessels',
            'village',
            
            # mFisheries extension for CC4Fish
            'weathersources',
            'emergencycontacts',
            'weatherdetails',
            'weatheralerts',
            'weatheremail',
            'weatherentry',
            'weatherreadings',
            'weatherthresholds',
            # FEWER related tables
            'capsources',
            'damagereports',
            'damagecategories',
            'msreportfields_bridge',
            'msreportfields',
            'msreports'
        ]

        # Configuration of the database connection and install Database tables
        from mfisheries.scripts.initializedb import install_database_tables
        settings = get_appsettings('mfisheries/tests/test.ini', name='main')
        engine, res, e = install_database_tables(settings=settings)

        self.assertTrue(res, "Unable to connect to database: {0}".format(e))
        register(DBSession, transaction_manager=transaction.manager)

        self.engine = engine
        self.session = DBSession

    def tearDown(self):
        testing.tearDown()
        self.delete_database()
        transaction.abort()
        self.session = None

    def getTableNames(self):
        from sqlalchemy.engine.reflection import Inspector
        inspector = Inspector.from_engine(self.engine)
        names = inspector.get_table_names()
        return names

    def test_if_table_created(self):
        names = self.getTableNames()
        self.assertEqual(len(names), len(self.tables),
                         "Installed:{0} Expected:{1}".format(len(names), len(self.tables)))

    def test_tables(self):
        print("Checking here for table names")
        names = self.getTableNames()
        for x in range(0, len(names)):
            self.assertTrue(names[x] in self.tables, "Found {0} table".format(names[x]))

    def test_get_countries(self):
        from mfisheries.core.country import Country
        print("Retrieving Countries")  # Retrieve first to check for default values
        countries = self.session.query(Country).all()
        self.assertTrue(len(countries) > 1, "Found {0} countries".format(len(countries)))

    def test_save_country(self):
        from mfisheries.core.country import Country
        print("Creating a new country record")
        c = Country("../", "Test Country")
        self.session.add(c)
        transaction.commit()
        self.assertTrue(c.id > 0, "Unable to create a new country")

    def test_get_modules(self):
        from mfisheries.core import Module
        print("Retrieving modules")  # Retrieve first to check for default values
        modules = self.session.query(Module).all()
        self.assertTrue(len(modules) >= 8, "Found {0} modules".format(len(modules)))

        # from mfisheries.core.country.modulehandler import getmodules
        # modules = getmodules()
        # self.assertTrue(len(modules) >= 8, "Found {0} modules".format(len(modules)))

    def test_get_country_modules(self):
        from mfisheries.core.country import CountryModule
        cmodules = self.session.query(CountryModule).all()
        self.assertTrue(len(cmodules) >= 0, "Found {0} modules".format(len(cmodules)))

        # from mfisheries.core.country.modulehandler import getcountrymodules
        # cmodules = getcountrymodules()
        # self.assertTrue(len(cmodules) >= 0, "Found {0} modules".format(len(cmodules)))

        # from mfisheries.core.country.modulehandler import getmodulebycountry
        # cmodules = getmodulebycountry(1)
        # self.assertTrue(len(cmodules) >= 0, "Found {0} modules".format(len(cmodules)))

    # TODO Check expected format for the module to ensure it produces what the web and android client expects

    def test_get_occupations(self):
        from mfisheries.core import Occupation
        print("Retrieving occupations")  # Retrieve first to check for default values
        occupations = self.session.query(Occupation).all()
        self.assertTrue(len(occupations) >= 8, "Found {0} occupations".format(len(occupations)))

    def test_get_users(self):
        from mfisheries.core.user.userhandler import get_users
        print("Retrieving Users")
        users = get_users()
        self.assertIsNotNone(users, "Users found to be none")
        self.assertTrue(len(users) > 0, "Found {0} users".format(len(users)))

    # Helper method used is the test checking user and test_get_user_id
    def save_user(self):
        from mfisheries.core.user import User
        import hashlib
        print("Saving Users")
        password = hashlib.sha256("simplePass").hexdigest()
        u = User("test_user", password, "Test", "User", "test@user.com")
        self.session.add(u)
        transaction.commit()
        self.assertTrue(u.id > 0, "ID was found to be {0}".format(u.id))

    def test_checking_user(self):
        from mfisheries.core.user.registrationhandler import check_user
        import hashlib
        self.save_user()
        password = hashlib.sha256("simplePass").hexdigest()
        self.assertIsNotNone(check_user("test_user", password), "User was not correctly")

    def test_get_user_id(self):
        from mfisheries.core.user.userhandler import get_user_by_id
        u = get_user_by_id(1)
        self.assertIsNotNone(u, "Unable to find user")
        self.assertEqual(u.id, 1, "user id was {0} expected {1}".format(u.id, 1))

    def test_save_user_submit(self):
        import os
        import json
        from mfisheries.core.user.userhandler import add_user

        dir_path = os.path.dirname(os.path.realpath(__file__))
        with open(dir_path + "/data/user_data.json") as fp:
            data = json.load(fp)
            fp.close()

        u = add_user(data)
        self.assertIsNotNone(u, "Unable to find user")
        self.assertTrue(u.id > 0, "user id was {0}".format(u.id))

    # TODO Add additional information to user created

    def test_cap_sources(self, name="Test CAP"):
        from mfisheries.modules.fewer.capsource import CAPSource
        c = CAPSource(1, name, 'CAP', "", 1)
        self.session.add(c)
        transaction.commit()
        self.assertTrue(c.id > 0, "ID was found to be {0}".format(c.id))
        cursor = self.session.query(CAPSource).all()
        self.assertIsNotNone(cursor, "CAP result found to be none")
        self.assertTrue(len(cursor) > 0, "Found {0} users".format(len(cursor)))
        get_rec = self.session.query(CAPSource).get(c.id)
        self.assertTrue(get_rec.name == c.name, "Successfully received record")
        return c
    
    def _base_resource_tester(self, base_class, class_handler, data_path, additional=None):
        dir_path = os.path.dirname(os.path.realpath(__file__))
        import json
        with open(dir_path + data_path) as fp:
            data = json.load(fp)
            fp.close()
        # Adding or over-ridding additional values
        if additional:
            for k in additional:
                data[k] = additional[k]
        handler = class_handler(None)
        model = handler._dict_to_model(data)
        
        self.assertIsNotNone(model, "Unable to retrieve model from data")
        self.assertTrue(isinstance(model, base_class), "Invalid data type returned")
        self.session.add(model)
        transaction.commit()
        self.assertTrue(model.id > 0, "ID was found to be {0}".format(model.id))
        return model
        
    # def test_damage_report(self):
    #     from mfisheries.modules.fewer.damagereport import DamageReport, DamageReportHandler
    #     self._base_resource_tester(DamageReport, DamageReportHandler, "/data/fewer/damage_report_data.json")

    def test_country_loc(self):
        from mfisheries.core.country import CountryLocation, CountryLocationHandler, Country
        c = Country("", "TestCountry")
        self.session.add(c)
        transaction.commit()
        options = {"countryid": c.id}
        self._base_resource_tester(CountryLocation, CountryLocationHandler, "/data/core/test_country_loc.json", options)
