https://avolkov.github.io/pyramid-web-app-setup-with-with-postgres-sqlalchemy-and-alembic.html

1. Created Migration Using Alembic
```python
alembic init mfisheries/alembic
```

2. Created migrations
```python
alembic revision --autogenerate -m "Initial Commit"
```
