# Conversion to Python 3 migration
## Introduction
As far as possible we would like to ensure that the code is compatible with both python 2 and python 3.
Python language provides a tool called 2 to 3 that will provide a way to both test and convert existing code. Therefore we should attempt to ensure to run this code for every file that exists and is subsequently converted.

### Command for testing
```bash
2to3 -w <filename>.py

```

## Tested Modules
The following section highlights modules within the application that were tested and converted to be compatible with Python 3.

* mfisheries.core.config
