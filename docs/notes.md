mFisheries Web and API Services Platform
==================
##Notes
This read me will help to keep track of notest made during discovery of the system structure

1. The init python file within the mfisheries package folder will be a useful first place to look at as its the starting/entry point for the application
2. The function load_local_modules function if the init file has all of the models created within the system



### Running Development on the Server
1. Use git instead of https [Add rsa public key to account](https://help.github.com/articles/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent/)

2. Clone Repository
```bash
git@bitbucket.org:mfisheries/mfisheries_webservices.git
cd mfisheries_webservices
```

3. Update development.ini settings
```bash
git update-index --assume-unchanged development.ini
cp ../development.ini ./ #Backup kept in the dev folder
```
Re-enable changes to development.ini file
```bash
git update-index --no-assume-unchanged development.ini
```


4. Create Virtual environment and install pyramid
```bash
virtualenv venv
source venv/bin/activate
pip install pyramid
python setup.py develop
```
5. Install database and bower
```bash
initialize_mfisheries_db development.ini
```

6. Install Front-end libraries.

First I attempted to use:
```bash
bower install mfisheries/static/bower.json 
```
However for some unknown reason it failed

To get it to work I:
```bash
rm .bowerrc # One time step
cd mfisheries/static
bower install bower.json
```

http://bootstrap-table.wenzhixin.net.cn/getting-started/#usage


## Migration from Bower to NPM
https://medium.com/netscape/bye-bye-bower-or-how-to-migrate-from-bower-to-npm-and-webpack-4eb2e1121a50
Completed:
* NPM installed (using npm 5.5.1)
```bash
npm install --save-dev 
```
