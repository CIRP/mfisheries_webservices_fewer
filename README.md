mFisheries Web and API Services Platform
==================
## Getting Started

### 1. Install software dependencies

For Ubuntu (debian systems)
```bash
sudo apt-get install libmysqlclient-dev python-dev
sudo apt-get install python-pip python-virtualenv
```


For Centos (Fedora)
```bash
sudo yum install mysql-devel mysql-lib python-devel
sudo yum install python-pip
pip install virtualenv
```
(NB for Centos you may have to enable additional repositories for these packages to be available)

### 2. Clone repository
Clone the repository into the workspace or desired:
```bash
git clone https://<username>@bitbucket.org/CIRP/mfisheries_webservices.git
```
- or
```bash
git clone git@bitbucket.org:CIRP/mfisheries_webservices.git
```
However this requires the ssh key of the machine to be configured in bitbucket

**Before configuration for installation**

Run the git command "git update-index --assume-unchanged" command on both the development.ini file and production.ini file.
```bash
git update-index --assume-unchanged development.ini
git update-index --assume-unchanged production.ini
```


This ensures that the values from each independent installation do not conflict with the development of each other

### 3. Create Virtual environment
- Ensure that you have pip and virtualenv installed for your environment
```bash
cd [directory containing this project]
virtualenv venv
source venv/bin/activate
```
- Ensure you are using the python from the local venv directory
```bash
which python
```
### 4. Install pyramid
- Install the pyramid system first
```bash
pip install pyramid
```
- Execute the installation procedure for pyramid
```bash
pip install pyramid
python setup.py develop
```
### 5. Configure and install database
- setup the database configuration
- Add the database username and password to the development and production .ini files
```bash
initialize_mfisheries_db development.ini
```
### 6. Install Front-end Libraries
```bash
cd mfisheries/static
bower install bower.json
```
### 7. Install default modules
```bash
python init_modules.py
```
### 8. Add Email Username and App Password
- Within the development and production .ini files add the details for the 'email_user' and 'email_app_password'
### 9 Run the system
```bash
pserve development.ini
```
- Add the reload if you want the system to relaunch with the updates when changes are made

Commands 5 to 9 may be done repeatedly when developing, as such we have created a simple script called "configure.sh" which performs these commands through and interactive prompt.

## JavaScript/Front-end Development
The following section highlights the use of the gulp system to test and build the front-end JavaScript code.
The gulp system takes the code written for the mFisheries angular web app within the js folders and subfolders (controller, services, etc).

The "gulp watch" command will look for changes in the JS and CSS files. When a change (upon saving) is detected, the system will lint the JS code and compile the JS code to a single app.js file.

### 10 Node Modules Development installation
- Install gulp cli globally
```bash
npm install -g gulp gulp-cli
```

- Install node packages used within the system
```bash
npm install
```
### 11 Watch for the changes within the Web application
```bash
gulp watch
```
### 11a) Specific Gulp Commands



### Extra Commands
To get undo/show dir's/files that are set to assume-unchanged
```bash
git update-index --no-assume-unchanged development.ini
```

To get a list of dir's/files that are assume-unchanged
```bash
git ls-files -v|grep '^h'
```

## Installation to Heroku For Testing
1. Use of multipacks
2. Added nodejs multipack
3. Procfile initiates run.sh to bootstrap app

## Notes
Required to remove gcm related packages
```bash
pip uninstall gcm-client
```