import os

from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(here, 'README.md')) as f:
    README = f.read()
with open(os.path.join("{0}/docs".format(here), 'CHANGES.txt')) as f:
    CHANGES = f.read()


requires = [
    'alembic',
    'apscheduler',
    'bs4',
    'cornice',
    'fastkml',
    'mysql-python',
    'plaster_pastedeploy',
    'pygeoif',
    'pyfcm',
    'pyramid',
    'pyramid_chameleon',
    'pyramid_debugtoolbar',
    'pyramid_mako',
    'pyramid_tm',
    'python-crontab',
    'python-dateutil',
    'python-firebase',
    'requests',
    'simplejson',
    'SQLAlchemy',
    'transaction',
    'zope.interface',
    'zope.sqlalchemy',
    'waitress',
    'beautifulsoup4'
]

test_requires = [
    'WebTest >= 1.3.1', 
    'pytest == 2.9.2',
    'pytest-cov',
]


setup(name='mfisheries',
      version='3.2',
      description='mfisheries',
      long_description=README + '\n\n' + CHANGES,
      classifiers=[
        "mFisheries", "CIRP", "UWI", "STA"
        ],
      author='Caribbean ICT Research Programme',
      author_email='mfisheries2011@gmail.com',
      url='http://www.cirp.org.tt',
      keywords='mfisheries',
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False,
      test_suite='mfisheries',
      # tests_require=test_requires,
      extras_require={
          'testing': test_requires,
      },
      install_requires=requires,
      entry_points="""\
      [paste.app_factory]
      main = mfisheries:main
      [console_scripts]
      initialize_mfisheries_db = mfisheries.scripts.initializedb:main
      run_alert_broadcast = mfisheries.scripts.broadcast:broadcast
      run_event_test = mfisheries.scripts.broadcast:trigger_notification
      show_settings = mfisheries.scripts.manage_settings:show_settings
      """,
      )
